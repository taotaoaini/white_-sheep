// 01 最长公共后缀查询 https://leetcode.cn/problems/longest-common-suffix-queries/description/
#include<bits/stdc++.h>

using namespace std;

class Solution {
public:
    vector<int> stringIndices(vector<string> &a, vector<string> &b) {
        vector<int> ans;
        int ns = 0;
        int mi[2]{0, 0x3f3f3f3f};
        for (int i = 0; i < (int) a.size(); i++) {
            if ((int) a[i].length() < mi[1]) {
                mi[1] = a[i].length();
                mi[0] = i;
            }
        }
        for (auto &s: a) ns += s.length();
        vector<array<int, 26>> ch(ns * 2 + 10);
        vector<int> t(ns * 2 + 10, -1);
        int idx = 0;

        auto insert = [&](string s, int id) { //插入
            int p = 0;
            int n = s.length();
            for (int i = 0; i < n; i++) {
                int j = s[i] - 'a';
                if (!ch[p][j]) ch[p][j] = ++idx;
                p = ch[p][j];
                if (t[p] == -1 || a[t[p]].length() > n) t[p] = id;
            }
        };
        auto query = [&](string s) -> int {  //查询
            int p = 0;
            int n = s.length();
            int res = -1;
            for (int i = 0; i < n; i++) {
                int j = s[i] - 'a';
                if (!ch[p][j]) return res;
                p = ch[p][j];
                res = t[p];
            }
            return t[p];
        };

        for (int i = 0; i < (int) a.size(); i++) {
            reverse(a[i].begin(), a[i].end());
            insert(a[i], i);
        }
        for (auto &s: b) {
            reverse(s.begin(), s.end());
            int res = query(s);
            if (res == -1) ans.push_back(mi[0]);
            else
                ans.push_back(res);
        }
        return ans;
    }
};