// https://codeforces.com/problemset/problem/1923/E
// 任意两点之间的信息处理，计数两端颜色相同，中间简单路径上的颜色都与两端的点对数
// 思路：
// 考虑以u为根节点的情况
// 1）以u为一个端点的情况，计数各个子树中与u颜色相同的所有符合条件的端点数
// 2）以u为中间节点，计数两个端点（简单路径经过u）符合条件的点对数
// 3）关键点：如何维护所有颜色的端点到达根路径上不会遇到相同颜色的端点
// 4）在（1）处理的同时，把子节点中与u节点颜色相同的删掉，可以保证（3）的情况
// todo https://atcoder.jp/contests/abc163/tasks/abc163_f https://www.luogu.com.cn/problem/AT_abc163_f
// https://www.cnblogs.com/HaHeHyt/p/18049302
// https://www.luogu.com.cn/problem/CF891C
//https://www.cnblogs.com/zac2010/p/17985517
#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
using i64 = long long;
const int N = 200010;
int n;
int c[N];
vector<int> e[N];
i64 res;
map<int, i64> dp[N];

void init() {
    res = 0;
    for (int i = 1; i <= n; i++) {
        c[i] = 0;
        e[i].clear();
        dp[i].clear();
    }
}

void dfs(int u, int fa) {
    dp[u][c[u]] = 1;
    for (auto v : e[u]) {
        if (v == fa) continue;
        dfs(v, u);
        if (dp[v].count(c[u])) { //统计以u为端点的情况
            res += dp[v][c[u]];
            dp[v].erase(c[u]);
        }
        // 启发式合并
        // 每个点最多被计算log次
        if (dp[u].size() < dp[v].size()) swap(dp[u], dp[v]); //v合并到u上
        for (auto t : dp[v]) { // 统计两端点经过lca=u的情况
            int cc = t.fi;
            if (dp[u].count(cc))
                res += dp[u][cc] * dp[v][cc];
            dp[u][cc] += dp[v][cc];
        }
    }
}

void solve() {
    cin >> n;
    for (int i = 1; i <= n; i++) cin >> c[i];
    for (int i = 1; i <= n - 1; i++) {
        int a, b;
        cin >> a >> b;
        e[a].push_back(b);
        e[b].push_back(a);
    }
    dfs(1, 0);
    cout << res << endl;
    init();
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    int t = 1;
    cin >> t;
    while (t--) {
        solve();
    }
    return 0;
}