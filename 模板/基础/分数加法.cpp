// N个数求和 https://pintia.cn/problem-sets/994805046380707840/exam/problems/994805133597065216?type=7&page=0
#include<bits/stdc++.h>
using namespace std;
int n,a,b,up,down=1;
char c;
int gcd(int a,int b){
    return b==0?a:gcd(b,a%b);
}
int main()
{
    cin>>n;
    while(n--)
    {
        cin>>a>>c>>b;
        up=up*b+a*down;                     /*通分*/
        down*=b;
        int k=abs(gcd(up,down));
        up/=k;                              /*约分*/
        down/=k;
    }
    if(up%down==0)cout<<up/down<<endl;      /*结果为整数*/
    else
    {
        if(up/down)cout<<up/down<<' ';      /*结果大于1*/
        cout<<up-up/down*down<<'/'<<down<<endl;
    }
    return 0;
}

