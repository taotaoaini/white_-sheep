#include<iostream>
#include <cmath>

using namespace std;
const int mod = 1e9 + 7;
using i64 = long long;
using f64 = double;

//整型快速幂
//--start--
int qmi(int a, int b, int p) {//int_pow
    int res = 1;
    a%=p;
    for (; b; b /= 2, a = 1LL * a * a % p) {
        if (b % 2) {
            res = 1LL * res * a % p;
        }
    }
    return res;
}


//长整形快速幂
//--start--
i64 qmi(i64 a, i64 b, i64 p) {//i64_pow
    i64 res = 1;
    a%=p;
    for (; b; b /= 2, a = a * a % p) {
        if (b % 2) {
            res = res * a % p;
        }
    }
    return res;
}

//浮点数快速幂
//--start--
f64 qmi(f64 a, i64 b) {//f64_pow
    if (fabs(a - 0.0) < 1e-6) return 0;
    f64 res = 1.0;
    while (b) {
        if (b & 1) res = res * a;
        a = a * a;
        b >>= 1;
    }
    return res;
}

int main() {
    cout << qmi(2, 4, mod) << endl;
    return 0;
}
