#include<bits/stdc++.h>
using namespace std;
void swap(vector<int>& a, int i, int j) {
    int temp = a[i];
    a[i] = a[j];
    a[j] = temp;
}
// 全排列的递归函数
void permute(vector<int>& nums, int start, vector<vector<int>>& res) {
    if (start == nums.size()) {
        // 当到达数组末尾时，将当前排列加入结果集中
        res.push_back(nums);
        return;
    }

    for (int i = start; i < nums.size(); ++i) {
        // 交换元素以创建新的排列
        swap(nums, start, i);
        // 递归生成剩余元素的全排列
        permute(nums, start + 1, res);
        // 回溯，恢复原数组状态
        swap(nums, start, i);
    }
}
int main() {
    int n = 5; // 示例：生成0到2的全排列
    vector<int> nums(n);
    for (int i = 0; i < n; ++i) {
        nums[i] = i;
    }
    vector<vector<int>> results;

    permute(nums, 0, results);

    // 打印所有全排列
    for (const auto& vec : results) {
        for (int num : vec) {
            cout << num << " ";
        }
        cout << endl;
    }

    return 0;
}