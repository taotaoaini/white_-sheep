#include <bits/stdc++.h>
using  namespace std;

// https://ac.nowcoder.com/acm/contest/76790/B
// https://acm.hdu.edu.cn/showproblem.php?pid=1205

// （1） 若有n+1只鸽子飞回n个鸽巢，则至少有一个鸽巢里有不少于两只鸽子。
