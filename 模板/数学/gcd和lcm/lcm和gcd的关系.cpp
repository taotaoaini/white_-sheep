// 指导：https://blog.csdn.net/baiyifeifei/article/details/87927862
// lcm(a,b,c)=a*b*c*gcd(a,b,c)/(gcd(a,b)*gcd(b,c)*gcd(a,c))
// 组合数定理
// max(u,v)=u+v-min(u,v)
// max(u,v,w)=u+v+w-min(u,v)-min(v,w)-min(u,w)+min(u,v,w);
// 先考虑a,b,c均为素数的简单情况
// a=p^u,b=p^v,c=p^w
// lcm(a,b,c)=p^max(u,v,w)
// lcm(a,b,c)=a*b*c*gcd(a,b,c)/(gcd(a,b)*gcd(b,c)*gcd(a,c))

