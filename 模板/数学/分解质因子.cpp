#include<bits/stdc++.h>
//https://leetcode.cn/problems/maximum-element-sum-of-a-complete-subset-of-indices/description/
using namespace std;

// 1.整数n的质因子和个数
// 时间复杂度：O(sqrt(n))
// 时间优化: 预处理出<=sqrt(n)的所有质数，for循环减少，时间缩短为10分之1
struct Solution1 {
    vector<int> a;//质数
    vector<int> b;//个数
    void init(int n) {
        for (int i = 2; i <= n / i; i++) {
            if (n % i == 0) {
                a.push_back(i);
                b.push_back(0);
                while (n % i == 0) {
                    b.back() += 1;
                    n/=i;
                }
            }
        }
        if(n>1) a.push_back(n),b.push_back(1);
    }
};

// 2.求[1,n]的质因子
struct Solution2{
    vector<vector<int>> init(int n){// n<=1e7 最多的一个数也就8个质因子
        vector<vector<int>> ans(n+10);
        for(int i=2;i<=n;i++){
            if(ans[i].empty()){
                for(int j=i;j<=n;j+=i){
                    ans[j].push_back(i);
                }
            }
        }
        return ans;
    }
};

// 3.筛选[1,n]的质因子，统计子集中每对数都是完全平方数
struct Solution3{

};

