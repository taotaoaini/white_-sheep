// 逆序对期望 https://dashoj.com/d/lqbproblem/p/220
#include<bits/stdc++.h>

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

i64 cnt = 0;
int n = 51;
i64 res = 0;
int a[60];
int tr[60];
int lowbit(int x) {
    return x & -x;
}
int query(int x) {
    int res = 0;
    for (int i = x; i >= 1; i -= lowbit(i)) {
        res += tr[i];
    }
    return res;
}
void add(int x, int val) {
    for (int i = x; i <= n; i += lowbit(i)) tr[i] += val;
}
int get() {
    int res = 0;
    for (int i = 1; i <= n; i++) tr[i] = 0;
    for (int i = n; i >= 1; i--) {
        res += query(a[i]);
        add(a[i], 1);
    }
    return res;
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (i == j) continue;
            for (int k = 1; k <= n; k++) {
                for (int z = 1; z <= n; z++) {
                    if (k == z) continue;
                    cnt++;
                    for (int t = 1; t <= n; t++) a[t] = t;
                    swap(a[i], a[j]);
                    swap(a[k], a[z]);
                    res += get();
                }
            }
        }
    }
    cout << (f128)res / cnt << endl; // 65.33
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}