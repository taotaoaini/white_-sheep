#include <bits/stdc++.h>
using namespace std;
//预头--
using i64= long long;
const int mod=1e9+7;
const int N=2010;

// 1.递推法 1<=m,n<=1e3，如果没有mod，只能n<=60
struct Solution1{
    vector<vector<int>> c;
    void init(int n){
        c.assign(n+10,vector<int>(n+10));
        for(int i=1;i<=n;i++){
            for(int j=0;j<=i;j++){
                if(!j) c[i][j]=1;
                else c[i][j]=(c[i-1][j]+c[i-1][j-1])%mod;
            }
        }
    }
};

// 2.组合数模板
//--start--
const int MOD = 1e9 + 7;
const int MX = 100000;

long long q_pow(long long x, int n) {//快速幂
    long long res = 1;
    for (; n > 0; n /= 2) {
        if (n % 2) {
            res = res * x % MOD;
        }
        x = x * x % MOD;
    }
    return res;
}

long long fac[MX], inv_fac[MX];//阶乘数组和乘法逆元
auto init = [] {
    fac[0] = 1;
    for (int i = 1; i < MX; i++) {
        fac[i] = fac[i - 1] * i % MOD;
    }
    inv_fac[MX - 1] = q_pow(fac[MX - 1], MOD - 2);
    for (int i = MX - 1; i > 0; i--) {
        inv_fac[i - 1] = inv_fac[i] * i % MOD;
    }
    return 0;
}();

long long comb(int n, int k) {//C(n,k)
    return fac[n] * inv_fac[k] % MOD * inv_fac[n - k] % MOD;
}
