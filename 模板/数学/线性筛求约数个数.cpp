// https://www.cnblogs.com/littlehb/p/16650121.html
#include <bits/stdc++.h>
using namespace std;

// 数据存储：d[i]:记录i的约数个数
// num[i]: 记录i的最小质因子个数

// 分情况讨论
//1) i:质数   d[i]=2,num[i]=1
//2) i取模枚举的第j个质数为0，primes[j]整除i
// d[i]=>d[i*primes[j]]的值
// primes[j]从小到大枚举，所以一定是i的最小的质因子，所以p^(r+1)，按唯一分解定理展开
// d[i*primes[j]]=(1+r1+1)*...*(1+rk)
// d[i*primes[j]]=d[i]/(num[i]+1)*(num[i]+2)
// num[i*primes[j]]=num[i]+1
//3) 当前数取模枚举的第j个质数不为0，即primes[j]不能整除i
// d[i*primes[j]]=d[i]*(1+1);
// 此时对于最小质因子，j是从小到大枚举的，，i*primes[j]这个数的最小质因子就是primes[j]
// num[i*primes[j]]=1

const int N = 1010;
int primes[N], cnt; // primes[]存储所有素数
bool st[N];         // st[x]存储x是否被筛掉
int d[N];           // d[x]表示x的约数个数
int num[N];         // num[x]表示x的最小质因数的个数
int n;

//欧拉筛法+求约数个数
void init(int n) { // 时间复杂度：O(n)
    d[1] = 1; // 1的约数只有1个,这个比较特殊

    for (int i = 2; i <= n; i++) {
        if (!st[i]) {
            primes[cnt++] = i;
            // i是质数
            d[i] = 2;   //约数个数是2个，一个是1，另一个是i
            num[i] = 1; //最小质因子个数是1，最小质因子就是自己i
        }

        for (int j = 0; j < cnt && i * primes[j] <= n; j++) {
            st[i * primes[j]] = true;
            if (i % primes[j] == 0) {
                d[i * primes[j]] = d[i] / (num[i] + 1) * (num[i] + 2);
                num[i * primes[j]] = num[i] + 1;
                break;
            } else {
                // d[i * primes[j]] = d[i] * d[primes[j]]; 等价于下面的代码　
                d[i * primes[j]] = d[i] * 2;
                num[i * primes[j]] = 1;
            }
        }
    }
}

int main() {

}