#include <bits/stdc++.h>

using namespace std;
using i64 = long long;
const int inf = 0x3f3f3f3f;
const int mod = 1e9 + 7;

// 线性基是一个数的集合，并且每个序列都拥有至少一个线性基，取线性基中若干个数异或起来可以得到原序列中的任何一个数。
// 性质：（1）原序列的任意一个数都可以通过线性基里面的一些数异或得到
//       (2)线性基里面的任意一些数异或起来不能得到0
//       (3)线性基里面的数是唯一，并且再保持性质的前提下，数的个数也是最少的
//        (4)线性基里面的任意数异或出来的数也是随机的
// 线性无关组
//快速查询一个数是否可以被一堆数异或出来
//快速查询一堆数可以异或出来的最大/最小值
//快速查询一堆数可以异或出来的第k大值

//【问题】
// （1）.给定一组数A={a1,a2,…,an}判断通过异或操作可以得到多少不同的数。
//用这组数构建线性基，记r为线性基的大小，每个数都可以表示为线性基中若干个数的异或和，因此结果为2^r
//不会出现重复的
// （2）.给定一组数A={a1,a2,…,an}判断其中有多少个子集，其异或和为0。
// 所有子集中异或和为0的子集共有2^(n−r)个
// （3）.给定一组数A={a1,a2,…,an}，判断其中有多少个子集，其异或和为x
// ?
// （4）.给定一组数A={a1,a2,…,an}，问可以切分为最多多少个连续的子序列，要求切分后任意子序列的异或和都不为0。
// 解决

// (5)给定一颗有n个顶点的树，每个顶点上都写了一个数字。对于每个顶点，回答在以该顶点为根的子树中，任意选取顶点上的数字，有多少种不同的亦或和
// ???

//【模板】线性基 https://www.luogu.com.cn/problem/P3812
const int BITN = 60;

struct Solution1 {
    i64 d[BITN + 10];
    int cnt;//插入多少个数
    bool zero = false; //是否出现过异或为0的
    void add(i64 x) { //返回线性基数组
        for (int i = BITN; i >= 0; i--) {
            if (x & (1LL << i)) {
                if (d[i]) x ^= d[i];
                else {
                    d[i] = x;
                    cnt++;
                    break;
                }
            }
        }
        if (!x) zero = true;
    }

    bool ask(i64 x) { //查询一个元素是否可以被异或出来
        for (int i = BITN; i >= 0; i--) {
            if (x & (1LL << i)) x ^= d[i];
        }
        return x == 0;
    }

    i64 askmx() { //查询异或最大值
        i64 res = 0;
        for (int i = BITN; i >= 0; i--) {
            if ((res ^ d[i]) > res) res ^= d[i];
        }
    }

    i64 askmi() { //查询异或最小值
        if (zero) return 0;
        for (int i = 0; i <= BITN; i++) {
            if (d[i]) return d[i];
        }
    }

    // 查询第k大/小问题???
    i64 p[BITN + 10];
    int cntp = 0;

    void rebuild() { //重构成优秀性质的线性基
        for (int i = BITN; i >= 0; i--) {
            for (int j = i - 1; j >= 0; j--) {
                if (d[i] & (1LL << j)) d[i] ^= d[j];
                //显然是把一个高位的线性基的其它位变得更小
//                一个数二进制拆分后，每一位的线性基异或后所得到的值严格大于一个比它小的数所得的值。
            }
        }
        for (int i = 0; i <= BITN; i++) {
            if (d[i]) p[cntp++] = d[i];
        }
    }

    i64 kthmx(int k) { //第k大
        if (k >= (1LL << cnt)) return -1; //不存在
        i64 res = 0;
        for (int i = BITN; i >= 0; i--) {
            if ((k >> i) & 1) res ^= p[i];
        }
        return res;
    }
};

// C. Square Subsets https://codeforces.com/problemset/problem/895/C
// 题意：给定n(1<=n<=1e5)个数，ai<=70,问有多少个集合，满足集合中所有数相乘是一个完全平方数(空集除外)
// 完全平方数，根据唯一分解定理，唯一分解后质因子出现的个数为偶数
// <=70:19个质数
// 将读入的每一个数转化为一个19位的二进制数，每一位上为1表示该因子出现奇数次，否则为偶数
// -->给你n个数，求满足异或和为0的集合个数??
// 求n个数组成集合的线性基，答案为2^(n-线性基大小)-1
// 构造线性基，因为线性基里面的任意一些数异或起来不为0，所以没插入到线性基里面的数肯定可以通过异或最后变成0
struct Solution2 {
    i64 qmi(i64 a, i64 b) {
        i64 res = 1;
        while (b) {
            if (b & 1) res = (res * a) % mod;
            b >>= 1;
            a = (a * a) % mod;
        }
        return res;
    }

    void solve2() {
        int n;
        cin >> n;
        auto gets = [&](int n) -> vector<int> {
            vector<int> pr;
            for (int i = 2; i <= n; i++) {
                bool flag = true;
                for (int j = 2; j <= i / j; j++) {
                    if (i % j != 0) {
                        flag = false;
                        break;
                    }
                }
                if (flag) pr.push_back(i);
            }
            return pr;
        };
        vector<int> a(n + 10, 0);
        vector<int> pr = gets(70);
        vector<int> ans(pr.size() + 10, 0);
        for (int i = 1, x; i <= n; i++) {
            cin >> x;
            for (int j = 0; j <= pr.size(); j++) {
                int t = 0;
                while (x % pr[j] == 0) {
                    x /= pr[j];
                    t ^= 1;
                }
                a[i] |= t * (1LL << j);
            }
        }
        for (int i = 1; i <= n; i++) {
            for (int j = pr.size() - 1; j >= 0; j--) {
                if (a[i] & (1LL << j)) {
                    if (ans[j] == 0) {
                        ans[j] = a[i];
                        break;
                    } else a[i] ^= ans[j];
                }
            }
        }
        i64 res = n;
        for (int i = 0; i < pr.size(); i++) {
            if (ans[i]) res -= 1;
        }
        cout << qmi(2LL, res) - 1 << endl;
    }
};

//（4）给定一组数A={a1,a2,…,an}，问可以切分为最多多少个连续的子序列，要求切分后任意子序列的异或和都不为0。
//先处理出前缀异或，这样选择更多的区间其实就相当于选择更多的前缀异或，并且这些前缀异或不能异或出0，这就变成了线性基的基础题了。
// 贪心的放，能放就放。不能放就意味着线性基的add函数里面的val最后变成了0，也就是当前已经插入的线性基已经可以异或出正在插入的数了，所以不能放。
struct Solution4 {
    void solve4() {
        int n;
        cin >> n;
        vector<int> a(n + 1);
        vector<int> p(40);
        for (int i = 1; i <= n; i++) cin >> a[i], a[i] ^= a[i - 1]; //亦或前綴和
        if (a[n] == 0) {
            cout << -1 << endl;
            return;
        }
        int res = 0;
        for (int i = n; i >= 1; i--) { //分段，要將亦或為0的放在一起 a[i]^a[j]=0->[i+1:j]==0
            for (int j = 30; j >= 0; j--) {
                if (a[i] & (1 << j)) {
                    if (!p[j]) {
                        p[j] = a[i];
                        res += 1;
                        break;
                    } else {
                        a[i] ^= p[j];
                    }
                }
            }
        }
        cout << res << endl;
    }
};