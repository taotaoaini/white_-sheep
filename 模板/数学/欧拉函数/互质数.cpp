// 互质数
// 欧拉函数小于等于或等于n的正整数中与n互质的数的数目(1需要特判)
// f(x)=x*(p1-1)/p1*(p2-1)/p2*.....*(pn-1)/pn
// 欧拉函数性质和简单证明 https://www.cnblogs.com/Tshaxz/p/15176782.html

// （1）如果n是一个质数的莫一个次方，即n=p^k（p为质数，k为大于等于1的整数）
// f(p^k)=p^k-p^{k-1};
// f(8)= 1 3 5 7
// 只有当一个数不包含质数p，才可能与n互质。
// 1*p,2*p,3*p,....,p^{k-1}*p p^{k-1}个

// （2）n可以分解为两个互质的整数之积，n=p1*p2,则f(n)=f(p1)*f(p2)

#include<bits/stdc++.h>

using namespace std;
using i64 = long long;
// 从1~n求与n互质的数的个数，即求一个phi(n)，欧拉函数phi(1) = 1，但如果不包含右区间，1需要特判
i64 get(int n) { // 试除法
    i64 res = n;
    for (int i = 2; i <= n / i; i++) {
        if (n % i == 0) {
            res = res / i * (i - 1); // 先除再乘，防止爆
            while (n % i == 0) n /= i;
        }
    }
    if (n > 1) res = res / n * (n - 1);
    return res;
}

void solve() {
//        cout<<get(8)<<endl;

}
int main(){
    solve();
    return 0;
}