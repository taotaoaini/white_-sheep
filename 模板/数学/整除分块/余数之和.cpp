// 余数之和 https://www.acwing.com/problem/content/201/

// k = n/i
// i'=max i,ik<=n
// i'= n/k=n/(n/i)
// k mod [1,n] 求和   n<=1e9,k<=1e9
#include <bits/stdc++.h>
using namespace std;
using i64 = long long;
void solve(){
    // k mod i = k- k/i * i
    //  发现这个方程可以进行整除分块优化
    int n,k;
    cin>>n>>k;
    i64 res=1LL*n*k;
    for(int l=1,r;l<=n;l=r+1){
        int x=k/l;
        if(x==0) break;
        r=min(n,k/x);
        res-=1LL*x*(r-l+1)*(l+r)/2;
    }
    cout<<res<<endl;
}
int main(){
    solve();
    return 0;
}