//  两个闹钟 https://dashoj.com/d/problemDay/p/51

// n+ax=m+by [x>=0&&y>=0] 最优解
// ax-by=m-n
// 写成ax+by=m-n也是可以的，y可以是负数，先把初始解求出来，再处理
#include<bits/stdc++.h>

using namespace std;
#define endl '\n'
using i64 = long long;

i64 exgcd(i64 a, i64 b, i64 &x, i64 &y) {
    if (b == 0) {
        x = 1, y = 0;
        return a;
    }
    i64 res = exgcd(b, a % b, y, x);
    y -= a / b * x;
    return res;
}

void solve() {
    i64 n, m, a, b;
    cin >> n >> m >> a >> b;
    i64 x,y,g,c=m-n;
    g=exgcd(a,b,x,y);
    if(c%g==0){
        i64 x0=x*(c/g); // 初始解
        i64 y0=y*(c/g);
        i64 t=b/g;// x的步长
        // x'=x+tk , y' = y - a/g * k
        x0=(x0%t+t)%t; //x0的最小非负整数解
        y0=(n+a*x0-m)/b; // 利用原式求解
        while(y0<0){ // 找到第一个 y 的非负整数解即可
            x0+=t; // x 累加 t，恒大于等于 0
            y0=(n+a*x0-m)/b;
        }
        cout<<n+a*x0<<endl;
    }else{
        cout<<"Impossible"<<endl;
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    solve();
    return 0;
}