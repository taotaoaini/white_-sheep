#include<bits/stdc++.h>

using namespace std;
const int N = 1e3;

//单个质数判断
// 1. 6倍高效判断素数法
struct Solution1 {
    //从5开始,如果能整除6n+-1,肯定是合数
    bool isprime(int num) {
        if (num == 2 || num == 3) { return true; }
        if (num % 6 != 1 && num % 6 != 5) { return false; }
        int len = (int) sqrt(num);
        for (int i = 5; i <= len; i += 6) {
            if (num % i == 0 || num % (i + 2) == 0)
                return false;
        }
        return true;
    }
};

// [1,n]质数判断
// 1.线性筛
// 1e8 : 5.7e6
// 1e7 : 6.6e5
// 1e6 : 7.8e4
// 1e5 : 9592
// 1e4 : 1229
// sqrt(1e9) : 3401
vector<bool> vis; //[1,n]判断数组vis
vector<int> pr; //质数数组
int init = [](int n) {
    int res=0;
    vis.resize(n + 10, 0);
    for (int i = 2; i <= n; i++) {
        if (!vis[i]) pr.push_back(i),res++;
        for (int j = 0; j < pr.size() && (1ll) * pr[j] * i <= n; j++) {
            vis[pr[j] * i] = true;
            if (i % pr[j] == 0) break; //确保每个数被自己的最小质数筛掉
        }
    }
    return res;
}((int) (sqrt(1e9)));

struct Solution2 {
    //全局变量写法
    int pr[N], cnt;
    bool vis[N];

    int init(int n) {// 返回质数个数
        int res = 0;
        for (int i = 2; i <= n; i++) {
            if (!vis[i]) pr[cnt++] = i, res++;
            for (int j = 0; j < cnt && (1ll) * pr[j] * i <= n; j++) {
                vis[pr[j] * i] = true;
                if (i % pr[j] == 0) break;//确保每个数被自己的最小质数筛掉
            }
        }
        return res;
    }
};

// 3.埃式筛
struct Solution3 {
    // 埃式筛
    vector<int> init(int n) {// 返回素数判断数组
        vector<int> res(n + 1, 1);
        res[0] = res[1] = 0;
        int len = sqrt(n);
        for (int i = 2; i <= len; i++) {
            if (res[i]) {
                for (int j = i * i; j <= n; j += i) {
                    res[j] = 0;
                }
            }
            return res;
        }
    }
};



