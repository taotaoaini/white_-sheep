#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
typedef pair<int, int> P;
typedef long long ll;
const int N=2e7+10;
vector<int> pr;
int mip[N];
int m = 2e7;
void init() {
    for(int i = 2; i <= m; i++) {
        if(!mip[i]) {
            mip[i] = i;
            pr.push_back(i);
        }
        for(auto & p : pr) {
            i64 n = i * p;
            if(n > m) break;
            mip[n] = p;
            if(mip[i] == p) {
                break;
            }
        }
    }
}

void solve () {
    int n;
    cin >> n;
    i64 ans = 0;
    for(auto & p : pr) {
        if(p >= n) break;
        int now = n - p;
        if(mip[now] == now) continue;
        unordered_map<int, int> pp;
        while(now > 1) {
            pp[mip[now]] ++ ;
            now /= mip[now];
        }
        i64 sum = 1;
        for(auto& t : pp) {
            sum *= (t.se + 1);
        }
        ans += sum;
    }
    cout << ans << endl;
}


int main () {
    ios::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    init();
    solve ();
    return 0;
}