// 回文质数 Prime Palindromes  https://www.luogu.com.cn/problem/P1217

#include<bits/stdc++.h>

using namespace std;
using i64 = long long;

int len(int n) {
    int res = 0;
    while (n) {
        res += 1;
        n /= 10;
    }
    return res;
}

int is_prime(i64 x) { // 判断质数
    if (x == 2) return 1;
    for (int i = 2; 1LL*i * i <= x; i++) {
        if (x % i == 0) return 0;
    }
    return 1;
}

i64 get_odd(int x) { // 构造奇数回文串
    i64 res = x;
    x /= 10;
    while (x > 0) {
        res *= 10;
        res = res + x % 10;
        x /= 10;
    }
    return res;
}

i64 get_even(int x) { // 构造偶数回文串
    i64 res = x;
    while (x > 0) {
        res *= 10;
        res = res + x % 10;
        x /= 10;
    }
    return res;
}

void solve() { //sqrt(n)*log(n)*sqrt(n) 1e5*1e3*1e3/10
    int a, b;
    cin >> a >> b;
    for (int i = 5; i <= 100000; i++) { // 只要枚举b的数位的一半
        // 1....1  中间有偶数个0，包括0，则一定是11的倍数
        // 所有偶数位的回文数都是11的倍数,除了11，除非你认为它是一倍
        if (i == 9 && a <= 11 && b >= 11) cout << "11" << endl;
        // abba : a*1001+b*110
        i64 odd = get_odd(i);
        if (odd > b) break;
        if (odd >= a && is_prime(odd)) cout << odd << endl;
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    solve();
    return 0;
}