

//  给你一个整数数组 coins 表示不同面额的硬币，另给你一个整数 k 。你有无限量的每种面额的硬币。但是，你不能组合使用不同面额的硬币。
// 返回使用这些硬币能制造的 第 kth小金额。
// 1 <= coins.length <= 15
// 1 <= coins[i] <= 25 coins 包含两两不同的整数
// 1 <= k <= 2 * 10^9
// 特殊到一般
// x和y，组合出 m/x+m/y-m/(lcm(x,y))个不同的不超过m的值
// 对于更一般的容斥原理，需要枚举coins的所有非空子集，设子集的最小公倍数lcm，那么子集对个数的贡献为(-1)^{k-1}*m/lcm
#include <bits/stdc++.h>

using namespace std;
using i64 = long long;

long long findKthSmallest(vector<int> &coins, int k) {
    auto check = [&](long long m) -> bool {
        long long cnt = 0;
        for (int i = 1; i < (1 << coins.size()); i++) { // 枚举所有非空子集
            i64 tmp = 1;// 计算子集 LCM
            for (int j = 0; j < coins.size(); j++) {
                if (i >> j & 1) {
                    tmp = tmp / gcd(tmp, coins[j]) * coins[j];
                    if (tmp > m) { // 太大了
                        break;
                    }
                }
            }
            cnt += __builtin_popcount(i) % 2 ? m / tmp : -m / tmp;
        }
        return cnt >= k;
    };

    long long left = 0, right = 1e12;
    while (left + 1 < right) {
        long long mid = (left + right) / 2;
        (check(mid) ? right : left) = mid;
    }
    return right;
}
