#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using f64 = long double;
bool check(int n){
    for(int i=2;i<=n/i;i++){
        if(n%i==0) return false;
    }
    return true;
}
void solve(){
    int n;
    cin>>n;
    if(check(n)) {cout<<1<<endl;cout<<n<<endl;return;}
    int L=0,R=-1;
    for(int i=2;i<=n/i;i++){  //O(sqrt(n))
        if(n%i==0){
            int t=n;
            int j=i;
            while(t%j==0){ // log的 n/i/j/k
                t/=j;
                j++;
            }
            if(j-i>R-L+1){
                L=i;
                R=j-1;
            }
        }
    }
    cout<<R-L+1<<endl;
    for(int i=L;i<=R;i++){
        if(i==L) cout<<i;
        else cout<<"*"<<i;
    }
    cout<<endl;
}
int main(){
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    int t=1;
    // cin>>t;
    while(t--){
        solve();
    }
}
