#include<bits/stdc++.h>
using namespace std;

// (1)静态区间第k小
// (2)动态区间第k小，可以修改点值


// 1.静态区间第k小 https://www.luogu.com.cn/problem/P3834
// 给定 n 个整数构成的序列a，将对于指定的闭区间[l, r]查询其区间内的第k小值。m次询问
// 1<n,m<=1e5 ,|ai|<=1e9 , 1<=l,r<=n, 1<=k<=r-l+1
// 主席树==n颗线段树的叠合
// 空间复杂度分析：
// 每次插入，只更改logn+1个节点，故只增加更改的节点。(二叉树的总节点=叶子节点*2-1)
// 总节点数=2^{log_2(n)+1}-1 【n:最后一个节点】
// 动态开点，初始建树开2n-1个节点,有n次插入,每次插入最多增加logn+1个节点
// 总节点数：2n+n(logn+1)=n(logn+3)
// 时间复杂度分析：
// O(nlog^2{n})

//预头
#define lc(x) tr[x].l
#define rc(x) tr[x].r
const int N=200010;
struct node{
    // 节点值域中有多少个数
    int l,r,s;
};
struct Solution1 {
    vector<node> tr;
    int idx = 0;
    void init(int n) { //设置空间大小
        idx = 0;
        tr.assign(n*25,node());
    }
    void build(int &x, int L, int R) {
        x = ++idx;
        if (L == R) return;
        int mid = (L + R) / 2;
        build(lc(x), L, mid);
        build(rc(x), mid + 1, R);
    }
    void add(int x, int &y, int L, int R, int k) {
        y = ++idx;
        tr[y] = tr[x]; //先复制一遍
        tr[y].s += 1;
        if (L == R) return;
        int mid = (L + R) / 2;
        if (k <= mid) add(lc(x), lc(y), L, mid, k);
        else add(rc(x), rc(y), mid + 1, R, k);
    }
    int query(int x, int y, int L, int R, int k) { //前缀和思想
        if (L == R) return L;
        int mid = (L + R) / 2;
        int s = tr[lc(y)].s - tr[lc(x)].s;
        if (k <= s) return query(lc(x), lc(y), L, mid, k);
        else return query(rc(x), rc(y), mid + 1, R, k - s);
    }
};
void solve1() {
    int n, m;
    cin >> n >> m;
    vector<int> a(n + 1, 0);
    vector<int> b; //离散化数组
    vector<int> root(n + 1, 0);
    for (int i = 1; i <= n; i++) cin >> a[i];
    b = a;
    sort(b.begin()+1,b.end());
    b.erase(unique(b.begin() + 1, b.end()), b.end());
    int bn = b.size() - 1;
    Solution1 *tr = new Solution1();
    tr->init(n);
    tr->build(root[0], 1, bn); //创建空壳
    for (int i = 1; i <= n; i++) {
        int id = lower_bound(b.begin() + 1, b.end(), a[i]) - b.begin();
        tr->add(root[i - 1], root[i], 1, bn, id);
    }
    for (int i = 1, l, r, k; i <= m; i++) {
        cin >> l >> r >> k;
        int id = tr->query(root[l - 1], root[r], 1, bn, k);
        cout << b[id] << endl;
    }
}