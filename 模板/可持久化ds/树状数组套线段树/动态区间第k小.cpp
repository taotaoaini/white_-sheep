//  动态区间第k小 (1).https://www.luogu.com.cn/problem/P2617
// 树状数组 套 权值线段树树 O(nlognlogn)

// 文章：
// https://www.cnblogs.com/zxytxdy/p/12375599.html

// 思想：
// 主席树维护静态区间kth：建立可持久化线段树后，利用前缀和的思想查询区间的kth。
// 想对区间kth带修改操作，前缀和是关键
// 维护普通前缀和，支持查询和修改操作时：树状数组/线段树。
// 如何套：外层维护一颗树状数组，树状数组的每个节点（内层）维护权值线段树（的根节点）
// 时间复杂度分析：
// 修改操作：
//          修改一个位置的数字ai，则在外层树状数组上，修改logn个节点，同时对于每个节点（代表一颗权值线段树）
//          分别有logn个节点受影响，修改复杂度：O((logn)^2)
// 查询操作：
//          对于每个[L,R]区间的查询，先提取这个区间的logn个根节点，然后将问题转换为静态主席树求区间第k小
//          时间复杂度：O((logn)^2)
// 总时间复杂度：O(n(logn)^2)
// 空间复杂度分析
// 线段树的空间复杂度为O(4*n),树状数组的复杂度为O(n)
// 为什么线段树是4*n,因为线段树存的是区间，会比普通存值的二叉树多出一层，2^((log_2 n)+1)*2-1
// 线段树可以动态开点，O((logn)^2)

#include<bits/stdc++.h>
using namespace std;
// 空间计算
// 只需要计算更新时候的节点变化所需空间
// 每次修改需要改变 log n*log n
// m*log (n+m) * log (n*m)
const int N = 1e5 + 10;
int n, m;
int a[N], b[N * 2], len;
struct Query {
    int op;
    int l, r, val;
} q[N];

#define mid ((l+r)/2)
#define sum(p) tr[p].sum
#define ls(p) tr[p].left
#define rs(p) tr[p].right

struct node {
    int sum;
    int left, right;
} tr[N * 20*20];
int root[N],tot;

int lowbit(int x) {
    return x & -x;
}

void pull(int p) {
    sum(p) = sum(ls(p)) + sum(rs(p));
}

void update_st(int &p, int l, int r, int x, int val) {
    if (!p) p = ++tot;
    if (l == r) {
        sum(p) += val;
        return;
    }
    if (x <= mid) update_st(ls(p), l, mid, x, val);
    else update_st(rs(p), mid + 1, r, x, val);
    pull(p);
}

void update_bt(int pos, int x, int val) {
    for (int i = pos; i <= n; i += lowbit(i))
        update_st(root[i], 1, len, x, val);
}

// 提取区间线段树的根节点
int rt1[N], rt2[N], cnt1, cnt2;

void locate(int l, int r) {
    cnt1 = cnt2 = 0;
    for (int i = l - 1; i; i -= lowbit(i))
        rt1[++cnt1] = root[i];
    for (int i = r; i; i -= lowbit(i))
        rt2[++cnt2] = root[i];
}

int ask(int l, int r, int k) { // O(nlogn)
    if (l == r) return l;
    int sumL = 0;
    for (int i = 1; i <= cnt1; i++) // O(n)
        sumL -= sum(ls(rt1[i]));
    for (int i = 1; i <= cnt2; i++)
        sumL += sum(ls(rt2[i]));
    if (sumL >= k) {
        for (int i = 1; i <= cnt1; i++)
            rt1[i] = ls(rt1[i]);
        for (int i = 1; i <= cnt2; i++)
            rt2[i] = ls(rt2[i]);
        return ask(l, mid, k);
    } else {
        for (int i = 1; i <= cnt1; i++)
            rt1[i] = rs(rt1[i]);
        for (int i = 1; i <= cnt2; i++)
            rt2[i] = rs(rt2[i]);
        return ask(mid + 1, r, k - sumL);
    }
}

void solve() {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
        b[++len] = a[i];
    }
    for (int i = 1; i <= m; i++) {
        char op;
        cin >> op;
        if (op == 'Q') {
            q[i].op = 0;
            cin >> q[i].l >> q[i].r >> q[i].val;
        } else {
            q[i].op = 1;
            cin >> q[i].l >> q[i].val;
            b[++len] = q[i].val;
        }
    }
    //数值离散化
    sort(b + 1, b + len + 1);
    len = unique(b + 1, b + len + 1) - b - 1;
    for (int i = 1; i <= n; i++) a[i] = lower_bound(b + 1, b + len + 1, a[i]) - b;
    for (int i = 1; i <= m; i++)
        if (q[i].op) q[i].val = lower_bound(b + 1, b + len + 1, q[i].val) - b;
    // 建树（动态开点形式）
    for (int i = 1; i <= n; i++) {
        update_bt(i, a[i], 1);
    }
    for (int i = 1; i <= m; i++) {
        if (q[i].op) {
            update_bt(q[i].l, a[q[i].l], -1);
            a[q[i].l] = q[i].val;
            update_bt(q[i].l, q[i].val, 1);
        } else {
            locate(q[i].l, q[i].r);
            int res = b[ask(1, len, q[i].val)];
            cout << res << endl;
        }
    }
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}