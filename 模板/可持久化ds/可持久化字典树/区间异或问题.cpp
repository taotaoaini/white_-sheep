// 最大异或和 https://www.acwing.com/problem/content/258/

// 区间和通常转化为前缀和
// s[i]=(a[1]^a[2]^...^a[i])
// 找到位置p，s[p]^s[p+1]^...^s[n]^x==s[p-1]^s[n]^x
// s[n]^x定值，v=s[n]^x
// 现在就是在区间[l-1,r-1]中找到与v异或的最大值

// 字典树的空间如何取： N:数组大小*最多子节点个数*log(数组大小)
#include<bits/stdc++.h>
using namespace std;
const int N=6e5+10,len=25;
int ch[N*25][2],ver[N*25]; // ver版本号可以确定此节点是否存在ver[i]=0,表示此节点非法
int root[N],idx;
int a[N];
void insert(int x,int pre,int i){
    ver[x]=i;
    for(int k=len;k>=0;k--){
        int c=(a[i]>>k)&1;
        ch[x][!c]=ch[pre][!c]; // 将旧节点指向上一个版本
        ch[x][c]=++idx;
        x=ch[x][c],pre=ch[pre][c]; //向下传递
        ver[x]=i;
    }
}
int query(int x,int L,int v){
    int res=0;
    for(int k=len;k>=0;k--){
        int c=(v>>k)&1;
        if(ver[ch[x][!c]]>=L)
            x=ch[x][!c],res+=1<<k;
        else x=ch[x][c];
    }
    return res;
}
void solve(){
         int n,m;
         cin>>n>>m;
         ver[0]=-1; // 如果到达版本0：就是完全不存在的节点
         root[0]=++idx; // 创建初始版本，对应边界情况
         insert(root[0],0,0); // 构造初始节点
         for(int i=1,x;i<=n;i++){
             cin>>x;
             root[i]=++idx;
             a[i]=a[i-1]^x; // 插入的是前缀异或和的节点
            insert(root[i],root[i-1],i); // 根据上个版本进行插入新版本
         }
         for(int i=1;i<=m;i++){
             char op;
             int l,r,x;
             cin>>op;
             if(op=='A'){
                 cin>>x;
                 root[++n]=++idx;
                 a[n]=a[n-1]^x;
                 insert(root[n],root[n-1],n);
             }else{
                 cin>>l>>r>>x;
                 int res=query(root[r-1],l-1,a[n]^x); // 因为求的是p-1的位置，所以有-1的偏移量
                 cout<<res<<endl;
             }
         }
}
int main(){
    solve();
    return 0;
}