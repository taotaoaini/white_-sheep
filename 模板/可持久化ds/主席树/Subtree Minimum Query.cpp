// F. Subtree Minimum Query https://codeforces.com/problemset/problem/893/F

// （1）给你一颗有根树，点有权值，m 次询问，每次问你某个点的子树中距离其不超过 k 的点的权值的最小值。（边权均为1,点权有可能重复，k 值每次询问有可能不同）

//  扩展问题： 如果考虑是所有与x顶点连接距离其不超过k的点的权值的最小值,  （？？？？）

// 强制在线写法
// https://www.luogu.com.cn/article/uonv2s60
// https://blog.csdn.net/qq_45863710/article/details/120181267

#include<bits/stdc++.h>

using namespace std;
const int inf = 0x3f3f3f3f;
const int N = 1e5 + 10;
#define mid ((l+r)/2)
#define ls(p) tr[p].l
#define rs(p) tr[p].r
#define val(p) tr[p].val

#define ls(t) tr[t].l
#define rs(t) tr[t].r
#define val(t) tr[t].val

int n, m;
int rt;
int a[N];
int dfn[N];
int sz[N];
int dep[N]; // dep[rt]=0;
int f[N];
int tot=0;
int cnt=0;
vector<int> e[N];
int root[N];
int mxdep=0;
struct node{
    int val=inf;
    int l,r;
}tr[N*20];
void pull(int p){
    val(p)=min(val(ls(p)),val(rs(p)));
}
void change(int t,int& p,int l,int r,int x,int val){
    p=++cnt;
    tr[p]=tr[t];
    if(l==x&&r==x) {
        val(p) = val;
        return;
    }
    if(x<=mid) change(ls(t),ls(p),l,mid,x,val);
    else change(rs(t),rs(p),mid+1,r,x,val);
    pull(p);
}
int query(int p,int l,int r,int ql,int qr){
    if(ql<=l&&qr>=r) return val(p);
    int res=inf;
    if(ql<=mid) res=min(res, query(ls(p),l,mid,ql,qr));
    if(qr>mid) res=min(res, query(rs(p),mid+1,r,ql,qr));
    return res;
}
void dfs(int u, int fa) { //处理dfn序,子树大小,节点深度
    sz[u]=1;
    f[u]=fa;
    dfn[u]=++tot;
    for(int v:e[u]){
        if(v==fa) continue;
        dep[v]=dep[u]+1;
        mxdep=max(mxdep,dep[v]);
        dfs(v,u);
        sz[u]+=sz[v];
    }
}
void bfs(){
    int last=0;
    queue<int> q;
    q.push(rt);
    while(!q.empty()){
        int u=q.front();
        q.pop();
        change(root[dep[last]],root[dep[u]],1,n,dfn[u],a[u]);
        last=u;
        for(int v:e[u]){
            if(v==f[u]) continue;
            q.push(v);
        }
    }
}

void solve() {
    cin >> n >> rt;
    for (int i = 1; i <= n; i++) cin >> a[i];
    for (int i = 1; i <= n - 1; i++) {
        int x, y;
        cin >> x >> y;
        e[x].push_back(y);
        e[y].push_back(x);
    }
    dfs(rt, 0);
    bfs();
    cin>>m;
    int last=0;
    for(int i=1;i<=m;i++){
        int x,k;
        cin>>x>>k;
        x=(x+last)%n+1;
        k=(k+last)%n;
        last=query(root[min(dep[x]+k,mxdep)],1,n,dfn[x],dfn[x]+sz[x]-1); //最低层可能到不了dep[x]+k;
        cout<<last<<endl;
    }
}

int main() {
    solve();
    return 0;
}