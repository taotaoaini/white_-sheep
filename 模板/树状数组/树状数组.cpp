#include <algorithm>
#include <iostream>

using namespace std;

//树状数组+权值树状数组
// i的二进制表示末尾有k个连续的0，c[i]存储的区间长度为2^k,c[i]=a[i-2^k+1]+......+a[i];
// i=20(10100) [10100,10011,10010,10001]+c[10000]
//--start--
template<typename T>
struct Fenwick {
    int n;
    std::vector<T> a;

    Fenwick(int n = 0) {
        init(n);
    }

    void init(int n) {//初始化空间大小
        this->n = n;
        a.assign(n, T());
    }

    void add(int x, T v) {//允许从0开始加
        for (int i = x + 1; i <= n; i += i & -i) {
            a[i - 1] += v;
        }
    }

    T sum(int x) {//算的是下标小于x的sum,统计[0,x)的个数
        auto ans = T();
        for (int i = x; i > 0; i -= i & -i) {
            ans += a[i - 1];
        }
        return ans;
    }

    T rangeSum(int l, int r) {//[l,r)
        return sum(r) - sum(l);
    }

    //树状数组二分
    int kth(T k) {// 返回第k小 权值树状数组的时候使用，从0下标开始，0下标最小的数，如果没有kth小的数存在返回Fenwick_MaxLength
        int x = 0;
        for (int i = 1 << std::__lg(n); i; i /= 2) {
            if (x + i <= n && k >= a[x + i - 1]) {
                x += i;
                k -= a[x - 1];
            }
        }
        return x;
    }
    int qeury(T s){//查询最大T，满足sum(a1..ak)<=s 的k,从最大位开始贪心
        T sum=0;
        int pos=0;
        for(int j=18;j>=0;j--){
            if(pos+(1<<j)<=n&&sum+a[pos+(1<<j)]<=s){
                pos+=(1<<j);
                sum+=a[pos];
            }
        }
        return pos;
    }
};
//--end--

int main() {
//    Fenwick<int> fk = Fenwick<int>(1000);
//    fk.add(1, 1);
//    fk.add(3, 1);
//    fk.add(0, 1);
//    cout << fk.rangeSum(1, 4) << endl;
//    cout << fk.sum(4) << endl;
//    cout << fk.kth(2) << endl;
}