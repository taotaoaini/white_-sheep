#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using pii =pair<int,int>;
#define fi first
#define se second

const int mod=1e9+7;
// 交换相邻的数，达到有序的最少操作次数，冒泡排序，逆序对
//(1) 火柴排队 https://www.acwing.com/problem/content/description/507/
// 先排序，确定最小的距离，在考虑最少的交换次数
// 这题保证了每一列的数不同，如果有相同的数，问题就会变复杂，因为相同的数，在考虑交换次数上需要讨论
//  这题有离散,映射的思想
struct Solution1{
    void solve() {
        int n;
        cin>>n;
        vector<pii> a(n+1);
        vector<pii> b(n+1);
        vector<int> c(n+1);
        vector<i64> tr(n+10);
        auto query=[&](int x)->i64{
            i64 res=0;
            for(int i=x;i>=1;i=i&(i-1)){
                res+=tr[i];
            }
            return res;
        };
        auto change=[&](int x,int val){
            for(int i=x;i<=n;i+=i&-i){
                tr[i]+=val;
            }
        };
        for(int i=1;i<=n;i++) cin>>a[i].fi,a[i].se=i;
        for(int i=1;i<=n;i++) cin>>b[i].fi,b[i].se=i;
        sort(a.begin()+1,a.end());
        sort(b.begin()+1,b.end());
        for(int i=1;i<=n;i++){
            c[a[i].se]=b[i].se;
        }
        i64 res=0;
        for(int i=n;i>=1;i--){
            res=(res+query(c[i]))%mod;
            change(c[i],1);
        }
        cout<<res<<endl;

    }
};
int main(){

}