#include <bits/stdc++.h>
using namespace std;
// 预头
const int mod=1e9+7;

// (1)子数组的最小值之和 https://leetcode.cn/problems/sum-of-subarray-minimums/
// 找到数组中所有的连续子数组的最小值，并求和，1<=n<=3e4
// 贡献法考虑问题，找到左右端点，单调栈解决问题
// right[i]: 右边小于等于a[i]的第一个位置
// left[i]:左边严格小于a[i]的第一个位置
// 制定规则，将左边可以越过相同的数据，向右不能越过相同的数据
struct Solution1 {
    int sumSubarrayMins(vector<int>& arr) {
        int n=arr.size();
        stack<int> st;
        vector<int> left(n+1,-1);
        vector<int> right(n+1,n);
        for(int i=0;i<n;i++){
            while(!st.empty()&&arr[st.top()]>=arr[i]){
                right[st.top()]=i;
                st.pop();
            }
            if(!st.empty()){
                left[i]=st.top();
            }
            st.push(i);
        }
        int res=0;
        for(int i=0;i<n;i++){
            int L=left[i]+1,R=right[i]-1;
            res=(res+1LL*(i-L+1)*(R-i+1)%mod*arr[i]%mod)%mod;
        }
        return res;
    }
};