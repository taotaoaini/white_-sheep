#include<bits/stdc++.h>
using namespace std;
//  好子数组的最大分数 https://leetcode.cn/problems/maximum-score-of-a-good-subarray/?envType=daily-question&envId=2024-03-19
// （1）第i个元素之后第一个大于ai的元素的下标
void solve1(){ //维护一个单调递减的栈
    int n;
    cin>>n;
    vector<int> a(n+1);
    for(int i=1;i<=n;i++) cin>>a[i];
    deque<int> q;
    vector<int> ans(n+1); //若不存在，ans(i)=0
    for(int i=n;i>=1;i--){ //从后往前考虑
        while(q.size()&&a[q.back()]<=a[i]) q.pop_back();
        if(q.size()) ans[i]=q.back();
        q.push_back(i);
    }
    for(int i=1;i<=n;i++) cout<<ans[i]<<" ";
    cout<<endl;
}