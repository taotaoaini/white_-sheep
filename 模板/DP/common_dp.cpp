#include<bits/stdc++.h>
using namespace std;
using i64=long long;
using f64=double;

// 线性DP：https://www.lanqiao.cn/problems/2110/learning/
// 状态定义：f[i]表示i列的方案总数
// 堆积木分情况讨论：
// 1. 第i-1列铺满，+f[i-1]
// 2. 第i-1列空着，+f[i-2]
// 3. 第i-1列只排了一半，+2*f[i-3]
//    x x  or  x
//    x        x x
// 4. +2*f[i-3]
//    y y y y or y x x y
//    y x x y    y y y y
// 5. +2*f[i-4]
//    y y x x y or y x x y y
//    y x x y y    y y x x y
//......
//f[1]=1,f[2]=2,f[3]=5;
//得出公式:
// f[i]=f[i-1]+f[i-2]+2*f[i-3]+2*f[i-4]+...+2*f[1]
// f[i-1]=f[i-2]+f[i-3]+2*f[i-4]+...+2*f[1]
// f[i]-f[i-1]=f[i-1]+f[i-3]=>f[i]=2*f[i-1]+f[i-3]
