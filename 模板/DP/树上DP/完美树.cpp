
//  L3-035 完美树 https://pintia.cn/problem-sets/994805046380707840/exam/problems/1649748772845703169?type=7&page=1
//题解：https://blog.csdn.net/lamujiaolian/article/details/130354554
// 本题结论
// （1）如果树的大小为偶数，则白=黑，为奇数，则abs(白-黑）=1
// f[i][0]: 黑比白多一 f[i][1]:相等 f[i][2]:黑比白少一
// 对于整棵树来说，设它的根为root，进行后序遍历来优先处理所有子树并计算节点的个数。
// 如果以t为根的子树，它的节点个数为偶数，则必须用f[t][1]向上传递
// 如果以t为根的子树，它的节点个数为奇数，则可以用f[t][2]，f[t][0]向上传递，现在就需要考虑如何组合子树f[t][2]，f[t][0]值，以达到最小代价条件

//Case 1：如果所有子树加在一起，总共白色结点比黑色结点多2个，此时根必须要是黑的，而且只能转移到 f[root][2];
//Case 2: 如果所有子树加在一起，总共白色结点比黑色结点多1个，此时根也必须要是黑的，而且只能转移到f[root][1];
//Case 3: 如果所有子树加在一起，白色结点和黑色结点的数量相等，此时根的颜色任意，可以转移到f[root][2];
//Case 4: 如果所有子树加在一起，总共黑色结点比白色结点多1个，此时根必须要是白的，而且只能转移到f[root][1];
//Case 5: 如果所有子树加在一起，总共黑色结点比白色结点多2个，此时根也必须要是白的，而且只能转移到f[root][0]。
//其他情况不能成立，状态转移是十分清晰的。如果所需根的颜色和目前根的颜色不同，则需要额外支付给根染色的代价。

// 现需要考虑哪些子树取f[t][0] 哪些子树取f[t][2]
// 这里有一个贪心的方法
// 这个做法很秒，就是将个数为奇数的子树全部先变成f[i][2]，将f[i][2]-f[i][0]存入大顶堆中，以此取出大顶堆中的最大值，依次处理，到达状态{2,1,0,-1,-2}时更新状态

// 转义状态
// 1.当前树大小为偶数// 因每棵子树都需要满足要求，在转义时只考虑子树的奇偶性，在转义到父节点，不考虑奇偶性，统一处理
#include <bits/stdc++.h>
using namespace std;
vector<int> v[100050];
int color[100050];
int number[100050];
int cost[100050];
int f[100050][3]; // 0-黑比白多1 1-两者相等 2-白比黑多1
void traversal(int root)
{
    for(auto k:v[root]) traversal(k);
    priority_queue<int> pq;
    int sum = 0, cnt = 0;
    for(auto k:v[root]){
        number[root] += number[k];
        //先默认所有奇数个结点的子树都取白的比黑的多1个
        if(number[k]%2==1){ //只能由1或者3转移过来
            pq.push(f[k][2]-f[k][0]); //把替换代价最小的拿过来
            sum += f[k][2];
            cnt++;
        } else sum += f[k][1]; //否则只能由2转移过来
    }
    number[root]++;
    //此时分别计算f[root][i]
    while(cnt>=-2){
        if(cnt==2) { //白的比黑的多2个，根必须是黑的
            if(color[root] == 1) f[root][2] = min(f[root][2],sum);
            else f[root][2] = min(f[root][2], sum + cost[root]);
        }
        if(cnt==1) { //白的比黑的多1个，根必须是黑的
            if(color[root] == 1) f[root][1] = min(f[root][1],sum);
            else f[root][1] = min(f[root][1], sum + cost[root]);
        }
        if(cnt==0) { //白的和黑的相等，这是任意的
            if(color[root] == 1){
                f[root][0] = min(f[root][0], sum);
                f[root][2] = min(f[root][2], sum + cost[root]);
            } else {
                f[root][0] = min(f[root][0], sum + cost[root]);
                f[root][2] = min(f[root][2], sum);
            }
        }
        if(cnt==-1) { //白的比黑的少1个，根必须是白的
            if(color[root] == 1) f[root][1] = min(f[root][1],sum + cost[root]);
            else f[root][1] = min(f[root][1], sum);
        }
        if(cnt==-2) { //白的比黑的少2个，根必须是白的
            if(color[root] == 1) f[root][0] = min(f[root][0],sum + cost[root]);
            else f[root][0] = min(f[root][0], sum);
        }
        if(!pq.size()) break;
        sum -= pq.top();
        pq.pop();
        cnt -= 2;
    }
}
map<int,int> m;
int main()
{
    int n;
    cin >> n;
    memset(f,0x3f,sizeof f);
    for(int i=1;i<=n;i++){
        int k;
        scanf("%d %d %d",&color[i],&cost[i],&k);
        for(int j=0;j<k;j++) {
            int x;
            scanf("%d",&x);
            v[i].push_back(x);
            m[x] = i;
        }
    }
    int root;
    for(int i=1;i<=n;i++) {
        if(m[i] == 0){
            root = i;
            break;
        }
    }
    traversal(root);
    cout << min(f[root][1],min(f[root][0],f[root][2]));
}
