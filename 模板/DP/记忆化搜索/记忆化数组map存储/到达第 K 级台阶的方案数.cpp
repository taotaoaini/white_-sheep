#include<bits/stdc++.h>
using  namespace std;
// 到达第 K 级台阶的方案数  https://leetcode.cn/problems/find-number-of-ways-to-reach-the-k-th-stair/description/
// map记忆化搜索，状态即存即用
class Solution {
    unordered_map<long long, int> dp;

    int dfs(int i, int j, bool preDown, int k) {
        if (i > k + 1) {
            return 0;
        }
        long long p = (long long) i << 32 | j << 1 | preDown; // 用一个 long long 表示状态
        if (dp.count(p)) { // 之前算过了
            return dp[p];
        }
        int res = i == k;
        res += dfs(i + (1 << j), j + 1, false, k); // 操作二
        if (i && !preDown) {
            res += dfs(i - 1, j, true, k); // 操作一
        }
        return dp[p] = res; // 记忆化
    };

public:
    int waysToReachStair(int k) {
        return dfs(1, 0, false, k);
    }
};