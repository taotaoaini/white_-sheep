// 吃掉 N 个橘子的最少天数 https://leetcode.cn/problems/minimum-number-of-days-to-eat-n-oranges/description/

// 因为n=2*10^9,静态数组会很大，但简单分析一下，-1不可能比除优，我们需要尽可能使用/2，/3操作，所以只考虑除操作
// 不是每个n都会遍历到，所以开map可以解决问题

#include<bits/stdc++.h>

using namespace std;

class Solution {
public:
    unordered_map<int, int> mp{{1, 1},
                               {2, 2}}; // 记忆化数组初始化
    int minDays(int n) {
        if (mp.count(n)) return mp[n];
        int i2 = n % 2, i3 = n % 3; // 将n变为2的倍数，3的倍数的代价
        return mp[n] = min(1 + i2 + minDays(n / 2), 1 + i3 + minDays(n / 3));
    }
};