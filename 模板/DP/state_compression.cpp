#include<bits/stdc++.h>

using namespace std;


//--子集动态规划--

//n=常数，压缩暴力转移状态 https://leetcode.cn/problems/minimum-number-of-work-sessions-to-finish-the-tasks/description/
//预处理所有子集和，时间复杂度分析：预处理子集和耗时O(2^n)
//状态转移次数为 s的每个子集的子集个数之和，由于元素个数为 k 的集合有 C(n,k)个，其子集有2^k个
//二项式定理，sum(C(n,k)*2^k)=(2+1)^n=3^n
int minSessions(vector<int> &tasks, int sessionTime) {
    int n = tasks.size();
    int m = 1 << n;
    vector<int> sum(m);
    for (int i = 0; i < n; i++) {//预处理所有子集和
        for (int j = 0, k = 1 << i; j < k; j++) {
            sum[j | k] = sum[j] + tasks[i];
        }
    }
    vector<int> f(m, n);
    f[0] = 0;
    for (int s = 0; s < m; s++) {
        for (int sub = s; sub; sub = (sub - 1) & s) {//子集枚举
            if (sum[sub] <= sessionTime) {
                f[s] = min(f[s], f[s ^ sub] + 1);
            }
        }
    }
    return f[m - 1];
}
//-双倍经验
// 1.https://leetcode.cn/problems/parallel-courses-ii/description/
// 2.https://leetcode.cn/problems/distribute-repeating-integers/description/ （这题突破点在m的值很小，只有10）



