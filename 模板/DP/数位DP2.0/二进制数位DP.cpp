// 数位翻转 http://www.mokit.top/problem/program/6169/4707

// 对整数x的一次操作是翻转x的任意一个二进制位 （将0改为1或是将1改为0）,少要对x做多少次操作才能使得x ∈ [l, r]成立
// 多组测试，1 ≤ T ≤ 30000，0<=x,l,r<=2^63-1

// 数位DP2.0
#include<bits/stdc++.h>
using namespace std;
using i64 = signed long long;
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
i64 n;
i64 L, R;
int dp[70];
int dfs(int x, bool limit_low, bool limit_high) {
    if (x < 0) return 0;
    if (!limit_low && !limit_high && dp[x] != -1) {
        return dp[x];
    }
    int res = inf;
    int lo = limit_low ? ((1LL << x)&L) != 0 : 0;
    int hi = limit_high ? ((1LL << x)&R) != 0 : 1;
    for (i64 d = lo; d <= hi; d++) {
        int w = 0;
        if ((d << x) ^ ((1LL << x)&n))  w = 1;
        res = min(res, dfs(x - 1, limit_low && d == lo, limit_high && d == hi) + w);
    }
    if (!limit_low && !limit_high) dp[x] = res;
    return res;
}

void solve() {
    cin >> n >> L >> R;
    memset(dp, -1, sizeof(dp));
    cout << dfs(62, true, true) << endl;
}

int main() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    cin >> t;

    while (t--) {
        solve();
        //init();
    }
    return 0;
}