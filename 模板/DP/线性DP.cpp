#include <bits/stdc++.h>
using namespace std;
//(1)三个字符串的最长公共子序列
//找出最长的公共子序列，输出最长公共子序列本身
//
struct Solution1{
    void solve1(){
        string s1,s2,s3;
        cin>>s1>>s2>>s3;
        int n1=s1.length();
        int n2=s2.length();
        int n3=s3.length();
        s1=" "+s1;
        s2=" "+s2;
        s3=" "+s3;
        vector<string> ans;
        ans[0]="1";
        vector<vector<vector<string>>> dp(n1 + 1, vector<vector<string>>(n2 + 1, vector<string>(n3 + 1, "")));
        for(int i=1;i<=n1;i++){
            for(int j=1;j<=n2;j++){
                for(int k=1;k<=n3;k++){
                    if(s1[i]==s2[j]&&s1[i]==s3[k]){
                        dp[i][j][k]=dp[i-1][j-1][k-1]+s1[i];
                    }else{
                        if(dp[i-1][j][k].size()>dp[i][j-1][k].size()&&dp[i-1][j][k].size()>dp[i][j][k-1].size()){
                            dp[i][j][k]=dp[i-1][j][k];
                        }else if(dp[i][j - 1][k].size() > dp[i - 1][j][k].size() && dp[i][j - 1][k].size() > dp[i][j][k - 1].size()){
                            dp[i][j][k]=dp[i][j-1][k];
                        }else{
                            dp[i][j][k] = dp[i][j][k - 1];
                        }
                    }
/*                    if(ans[0].size()<dp[i][j][k].size()){
                        ans.clear();
                        ans.push_back(dp[i][j][k]);
                    }else if(ans[0].size()==dp[i][j][k].size()){
                        ans.push_back(dp[i][j][k]);
                    }*/
                    cout<<dp[i][j][k]<<endl;
                }
            }
        }
        cout<<dp[n1][n2][n3]<<endl;
//        for(auto& ss:ans) cout<<ss<<endl;
    }
};
int main(){
    Solution1 *tr=new Solution1();
    tr->solve1();
    return 0;
}