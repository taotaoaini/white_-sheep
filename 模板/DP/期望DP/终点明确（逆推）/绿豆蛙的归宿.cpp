// 绿豆蛙的归宿 https://www.acwing.com/problem/content/description/219/
// 题解：
// https://www.cnblogs.com/wzxbeliever/p/11679674.html
// https://www.cnblogs.com/RioTian/p/15053904.html
#include<bits/stdc++.h>
using namespace std;
using f64 = double;
using pii = pair<int,int>;
#define fi first
#define se second
const int N=100010;
f64 f[N];
vector<pii> e[N];
int n,m;
f64 dfs(int u){
    if(u==n) return 0;
    if(f[u]>=0) return f[u];
    f[u]=0;
    int sz=e[u].size();
    for(auto t:e[u]){
        int v=t.fi,w=t.se;
        f[u]+=(dfs(v)+w)*1.0/sz;
    }
    return f[u];
}
void solve(){
    cin>>n>>m;
    for(int i=1;i<=n;i++) f[i]= -1;
    for(int i=1;i<=m;i++){
        int x,y,w;
        cin>>x>>y>>w;
        e[x].push_back({y,w});
    }
    cout<<dfs(1)<<endl;
}
int main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    cout<<fixed<<setprecision(2);
    solve();
    return 0;
}