// Critical Hit https://atcoder.jp/contests/abc280/tasks/abc280_e
// 期望DP入门题：通过 E=p1*x1+p2*x2+....+pn*xn
#include<bits/stdc++.h>
using namespace std;
using f64 = double;
using i64 =long long;
const  i64 mod =998244353;
i64 qmi(i64 a,i64 b){
    i64 res=1;
    while(b){
        if(b&1) res=res*a%mod;
        b>>=1;
        a=a*a%mod;
    }
    return res;
}
void solve(){
    int n,p;
    cin>>n>>p;
    vector<i64> f(n+1);
    i64 A=p*qmi(100,mod-2)%mod;
    i64 B=(1-A+mod)%mod;
    f[1]=1; //初始化
    for(int i=1;i<=n;i++){
//        f[i]=(f[i-1]*B%mod+f[i-2]*A%mod+1)%mod;
        f[i]=((f[i-1]+1)*B%mod+(f[i-2]+1)*A%mod)%mod;
    }
    cout<<f[n]<<endl;
}
int main(){
    ios::sync_with_stdio(false);
    solve();
    return 0;
}