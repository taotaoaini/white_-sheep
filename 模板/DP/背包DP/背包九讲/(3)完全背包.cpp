#include <bits/stdc++.h>
using namespace std;
// 01 完全背包最大型背包 https://www.acwing.com/problem/content/3/
// 02 完全恰好型背包求方案数 https://www.acwing.com/problem/content/281/

//  很简单，无脑推就行了
struct Solution1{
    void solve() {
        int n,m;
        cin>>n>>m;
        vector<int> v(n+10),w(n+10);
        for(int i=1; i<=n; i++) {
            cin>>v[i]>>w[i];
        }
        vector<int> f(m+10);
        for(int i=1; i<=n; i++) {
            for(int j=v[i]; j<=m; j++) { //顺推
                f[j]=max(f[j],f[j-v[i]]+w[i]);
            }
        }
        cout<<f[m]<<endl;
    }
};