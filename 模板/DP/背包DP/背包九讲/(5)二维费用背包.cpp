#include <bits/stdc++.h>
using namespace std;
// 05 二维费用背包 https://www.acwing.com/problem/content/8/
struct Solution5{
    void solve() {
        int n,v,m;
        cin>>n>>v>>m;
        vector<vector<int>> f(v+10,vector<int>(m+10)); //  朴素写法 f[i,j]:前i组物品，能放入容量为j的背包的最大值
        vector<int> vv(n+1),mm(n+1),ww(n+1); //vv: 体积，mm：重量，ww：价值
        for(int i=1;i<=n;i++) cin>>vv[i]>>mm[i]>>ww[i];
        f[v][m]=0;
        for(int i=1;i<=n;i++) {
            for(int j=v;j>=vv[i];j--){ //逆序
                for(int k=m;k>=mm[i];k--){ //逆序
                    f[j][k]=max(f[j][k],f[j-vv[i]][k-mm[i]]+ww[i]);
                }
            }
        }
        cout<<f[v][m]<<endl;
    }
};