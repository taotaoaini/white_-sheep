#include<bits/stdc++.h>
using namespace std;
//背包九讲：v:体积，w：价值
// 01 背包问题求具体方案，求一个最小序方案 https://www.acwing.com/problem/content/12/
// （1）通过二维DP(此时不能不能状态压缩)先求结果，再反推方案
struct Solution1{
    void solve() {
        int n,m;
        cin>>n>>m;
        vector<int> v(n+10),w(n+10);
        for(int i=1; i<=n; i++) {
            cin>>v[i]>>w[i];
        }
        vector<vector<int>> f(n+10,vector<int>(m+10));
        for(int i=n; i>=1; i--) { //逆推
            for(int j=0; j<=m; j++) {
                f[i][j]=f[i+1][j];
                if(j>=v[i]) f[i][j]=max(f[i][j],f[i+1][j-v[i]]+w[i]);
            }
        }
        int vol=m;
        //最大值放在了f[1][m]，最小的先选
        for(int i=1;i<=n;i++){ //贪心选择
            if(vol>=v[i]&&f[i][vol]==f[i+1][vol-v[i]]+w[i]){ //前面的数，能选则选
                cout<<i<<" ";
                vol-=v[i];
            }
        }
        cout<<endl;
    }
};

// 02 求背包问题的最优选法的方案数 https://www.acwing.com/problem/content/description/11/
struct Solution2{
    const int mod=1e9+7;
    const int inf=0x3f3f3f3f;
    void solve() {
        int n,m;
        cin>>n>>m;
        vector<int> v(n+10),w(n+10);
        for(int i=1; i<=n; i++) {
            cin>>v[i]>>w[i];
        }
        vector<int> f(m+10,-inf),g(m+10);//恰好型背包,因为最多型背包不好计数
        f[0]=0;
        g[0]=1;
        for(int i=1; i<=n; i++) {
            for(int j=m; j>=v[i]; j--) {
                int t=max(f[j],f[j-v[i]]+w[i]);
                int s=0;
                if(t==f[j]) s=(s+g[j])%mod;
                if(t==f[j-v[i]]+w[i]) s=(s+g[j-v[i]])%mod;
                g[j]=s;
                f[j]=t;
            }
        }
        int mx=0;
        for(int i=0;i<=m;i++) mx=max(mx,f[i]);
        int res=0;
        for(int i=0;i<=m;i++){
            if(f[i]==mx){
                res=(res+g[i])%mod;
            }
        }
        cout<<res<<endl;
    }
};

// 03 完全背包最大型背包 https://www.acwing.com/problem/content/3/
//  很简单，无脑推就行了
struct Solution3{
    void solve() {
        int n,m;
        cin>>n>>m;
        vector<int> v(n+10),w(n+10);
        for(int i=1; i<=n; i++) {
            cin>>v[i]>>w[i];
        }
        vector<int> f(m+10);
        for(int i=1; i<=n; i++) {
            for(int j=v[i]; j<=m; j++) { //顺推
                f[j]=max(f[j],f[j-v[i]]+w[i]);
            }
        }
        cout<<f[m]<<endl;
    }
};

// 04 分组背包问题 https://www.acwing.com/problem/content/9/
struct Solution4{
    void solve1() { //二维数组
        int n,m;
        cin>>n>>m;
        vector<vector<int>> f(n+10,vector<int>(m+10)); //  朴素写法 f[i,j]:前i组物品，能放入容量为j的背包的最大值
        for(int i=1,s; i<=n; i++) {
            cin>>s;
            vector<int> v(s+10),w(s+10);
            for(int j=1; j<=s; j++) cin>>v[j]>>w[j];
            for(int j=0; j<=m; j++) {
                f[i][j]=f[i-1][j];
                for(int k=1; k<=s; k++) { //s个中只能选择一个，所以需要放在内层循环里
                    if(j>=v[k]) f[i][j]=max(f[i][j],f[i-1][j-v[k]]+w[k]);
                }
            }
        }
        cout<<f[n][m]<<endl;
    }
    void solve2() { //滚动数组
        int n,m;
        cin>>n>>m;
        vector<int> f(m+10);
        for(int k=1,s; k<=n; k++) {
            cin>>s;
            vector<int> v(s+10),w(s+10);
            for(int i=1; i<=s; i++) cin>>v[i]>>w[i];
            for(int i=m; i>=1; i--) { //逆序枚举体积，即当前i体积下选择s中的其中一个
                for(int j=1; j<=s; j++) { //s个中只能选择一个，所以需要放在内层循环里
                    if(i>=v[j])
                        f[i]=max(f[i],f[i-v[j]]+w[j]);
                }
            }
        }
        cout<<f[m]<<endl;
    }
};

// 05 二维费用背包 https://www.acwing.com/problem/content/8/
struct Solution5{
    void solve() {
        int n,v,m;
        cin>>n>>v>>m;
        vector<vector<int>> f(v+10,vector<int>(m+10)); //  朴素写法 f[i,j]:前i组物品，能放入容量为j的背包的最大值
        vector<int> vv(n+1),mm(n+1),ww(n+1); //vv: 体积，mm：重量，ww：价值
        for(int i=1;i<=n;i++) cin>>vv[i]>>mm[i]>>ww[i];
        f[v][m]=0;
        for(int i=1;i<=n;i++) {
            for(int j=v;j>=vv[i];j--){ //逆序
                for(int k=m;k>=mm[i];k--){ //逆序
                    f[j][k]=max(f[j][k],f[j-vv[i]][k-mm[i]]+ww[i]);
                }
            }
        }
        cout<<f[v][m]<<endl;
    }
};

// 06 混合背包问题 https://www.acwing.com/problem/content/7/
// n:1000,v:1000
struct Solution6{
    void solve1() {
        int n,m; //n:物品数量，m：体积
        cin>>n>>m;
        vector<array<int,3>> a;
        for(int i=1;i<=n;i++){
            int v,w,s;
            cin>>v>>w>>s;
            if(s==0){ //完全背包s设为0
                a.push_back({v,w,0});
            }else{
                if(s==-1){
                    a.push_back({v,w,1});
                }else{ // 多重背包重量以二进制形式进行拆分
                    int k=1;
                    while(s>=k){
                        a.push_back({k*v,k*w,1});
                        s-=k;
                        k<<=1;
                    }
                    if(s){
                        a.push_back({s*v,s*w,1});
                    }
                }
            }
        }
        vector<int> f(m+10);
        for(int i=0;i<a.size();i++){
            int v=a[i][0],w=a[i][1],c=a[i][2];
            if(c==1){ //01背包，逆序遍历
                for(int j=m;j>=v;j--){
                    f[j]=max(f[j],f[j-v]+w);
                }
            }else{ //完全背包，顺序遍历
                for(int j=v;j<=m;j++){
                    f[j]=max(f[j],f[j-v]+w);
                }
            }
        }
        cout<<f[m]<<endl;
    }
};
// 06 有依赖的背包问题 https://www.acwing.com/problem/content/10/
// https://www.luogu.com.cn/problem/P1064
// https://www.luogu.com.cn/problem/P2014
// 选择了这个节点，则从这个节点到根节点路径上的点都要选
// 转化思想：考虑以u为根节点的子树的所有的物品
// f[u][j]:表示选择以u为子树的物品，在体积和不超过容量j时所活获得的最大价值
// 当前节点u有i个子节点，每个节点可选可不选,把u的i个节点看作i组物品，每组物品按单位体积拆分进行分组背包,0,1...j-v[u]种决策，相对于组装的思想
//按单位体积拆分是因为子孙中可能出现体积为1的物品，拆分到j-v[u]是因为预留v[u]的空间装入节点u
const int N=110;
int n,m; // n：物品数量，m：体积
vector<int> e[N];
int w[N],v[N];
int f[N][N];
int root;
void dfs(int u,int fa) {
    //只选u节点
    for(int i=v[u]; i<=m; i++) {
        f[u][i]=w[u];
    }
    for(auto& x:e[u]) {
        if(x==fa) continue;
        dfs(x,u);
        // 筛选v节点
        for(int i=m; i>=v[u]; i--) { //预留v[u]的空间放v节点
            for(int j=0; j<=i-v[u]; j++) { //决策只能选择其中的一个，内循环
                f[u][i]=max(f[u][i],f[u][i-j]+f[x][j]);
            }
        }
    }
}
void solve1() {
    cin>>n>>m;
    for(int i=1,x; i<=n; i++) {
        cin>>v[i]>>w[i]>>x;
        if(x==-1) {
            root=i;
            continue;
        }
        e[x].push_back(i);
        e[i].push_back(x);
    }
// 	cout<<root<<endl;
    dfs(root,0);
    cout<<f[root][m]<<endl;
}

// 07 多重背包I  https://www.acwing.com/problem/content/4/
// n=100,v=100
struct Solution7{
    void solve1() { //O(nvv)
        int n,m; // n：物品数量，m：体积
        cin>>n>>m;
        vector<int> v(n+10),w(n+10),s(n+10);
        vector<int> f(m+10);
        for(int i=1; i<=n; i++) cin>>v[i]>>w[i]>>s[i];
        for(int i=1; i<=n; i++) {
            for(int j=m; j>=v[i]; j--) { // 体积
                for(int k=0; k<=s[i]&&k*v[i]<=j; k++) {
                    f[j]=max(f[j],f[j-k*v[i]]+k*w[i]);
                    //决策，内循环，每个i只能选择其中的一个决策
                }
            }
        }
        cout<<f[m]<<endl;
    }
};

// 08 多重背包问题 II https://www.acwing.com/problem/content/5/
// n=1000,v=2000
struct Solution8{
    void solve() {
        int n,m; // n：物品数量，m：体积
        cin>>n>>m;
        vector<int> v(n+10),w(n+10),s(n+10); // v:体积，w：价值,s[i]:物品i的个数
        vector<int> f(m+10);
        for(int i=1; i<=n; i++) cin>>v[i]>>w[i]>>s[i];
        auto get=[&](int x,int idx)->vector<array<int,2>>{ // s[i]二进制拆分
            vector<array<int,2>> ans;
            int k=1;
            while(x>=k){
                ans.push_back({k*v[idx],k*w[idx]});
                x-=k;
                k<<=1;
            }
            if(x){
                ans.push_back({x*v[idx],x*w[idx]});
            }
            return ans;
        };
        for(int i=1; i<=n; i++) {
            vector<array<int,2>> ans=get(s[i],i);
            for(int j=0;j<ans.size();j++){
                for(int k=m;k>=ans[j][0];k--){
                    f[k]=max(f[k],f[k-ans[j][0]]+ans[j][1]);
                }
            }
        }
        cout<<f[m]<<endl;
    }
};

// 09 多重背包问题 III https://www.acwing.com/problem/content/6/ :卡常
// 宝物筛选 https://www.luogu.com.cn/problem/P1776
// n=1000,v=20000,vi<=20000
struct Solution9{
    void solve() { //O(n*m)
        int n,m; // n：物品数量，m：体积
        cin>>n>>m;
        vector<int> f(m+10);
        for(int i=1;i<=n;i++){
            int v,w,s;
            cin>>v>>w>>s; // v:体积， w：价值, s:物品i的个数
            vector<int> g=f;
            for(int j=0;j<v;j++){// 分拆成w[i]个类,
                deque<int> q;	//对每个类使用单调队列　
                for(int k=j;k<=m;k+=v){ // 滑动窗口[k-s[i]*v[i],k-v[i]]
                    while(q.size()&&g[k]>=g[q.back()]+(k-q.back())/v*w) q.pop_back();
                    q.push_back(k);
                    while(q.size()&&q.front()<k-s*v) q.pop_front();
                    if(q.size()) f[k]=max(g[k],g[q.front()]+(k-q.front())/v*w);
                }
            }
        }
        cout<<f[m]<<endl;
    }
};