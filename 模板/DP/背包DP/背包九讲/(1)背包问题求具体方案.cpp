#include<bits/stdc++.h>
using namespace std;
//背包九讲：v:体积，w：价值
// 01 背包问题求具体方案，求一个最小序方案 https://www.acwing.com/problem/content/12/
// 02 拼凑出m的最小序方案 https://pintia.cn/problem-sets/994805046380707840/exam/problems/994805054207279104?type=7&page=1
// （1）通过二维DP(此时不能状态压缩)先求结果，再反推方案
// 01
struct Solution1{
    void solve() {
        int n,m;
        cin>>n>>m;
        vector<int> v(n+10),w(n+10);
        for(int i=1; i<=n; i++) {
            cin>>v[i]>>w[i];
        }
        vector<vector<int>> f(n+10,vector<int>(m+10));
        for(int i=n; i>=1; i--) { //逆推
            for(int j=0; j<=m; j++) {
                f[i][j]=f[i+1][j];
                if(j>=v[i]) f[i][j]=max(f[i][j],f[i+1][j-v[i]]+w[i]);
            }
        }
        int vol=m;
        //最大值放在了f[1][m]，最小的先选
        for(int i=1;i<=n;i++){ //贪心选择
            if(vol>=v[i]&&f[i][vol]==f[i+1][vol-v[i]]+w[i]){ //前面的数，能选则选
                cout<<i<<" ";
                vol-=v[i];
            }
        }
        cout<<endl;
    }
};

// 02
struct Solution2{
    const int inf=0x3f3f3f3f;
    void solve() {
        //freopen("a.in","r",stdin);
        //freopen("a.out","w",stdout);
        int n,m;
        cin>>n>>m;
        vector<int> a(n+1);
        for(int i=1;i<=n;i++) cin>>a[i];
        sort(a.begin()+1,a.end());
        vector<vector<int>> f(n+10,vector<int>(m+10,-inf));
        f[n+1][0]=0;
        for(int i=n;i>=1;i--){
            for(int j=0;j<=m;j++){
                f[i][j]=f[i+1][j];
                if(j>=a[i]) f[i][j]=max(f[i][j],f[i+1][j-a[i]]+a[i]);
            }
        }
        vector<int> ans;
        if(f[1][m]!=m) cout<<"No Solution";
        else{
            int s=m;
            for(int i=1;i<=n;i++){
                if(s>=a[i]&&f[i][s]==f[i+1][s-a[i]]+a[i]){
                    ans.push_back(a[i]);
                    s-=a[i];
                }
            }
            int end=ans.size();
            for(int i=0;i<end;i++){
                if(i==end-1) cout<<ans[i];
                else cout<<ans[i]<<" ";
            }
        }
    }
};