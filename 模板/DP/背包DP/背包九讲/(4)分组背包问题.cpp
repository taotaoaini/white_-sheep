// 选课  https://www.luogu.com.cn/problem/P2014
#include<bits/stdc++.h>
using namespace std;
const int N = 310;
int n, m;
int f[N][N]; // 以i为根节点选择j个子节点
int a[N];
vector<int> e[N];
void dfs(int u, int fa) {
    for (auto v : e[u]) {
        if (v == fa) continue;
        dfs(v, u);
        for (int i = m + 1; i > 0; i--) { // 容量,分组背包
            for (int j = 0; j < i; j++) { // 体积
                f[u][i] = max(f[u][i], f[u][i - j] + f[v][j]);
            }
        }
    }
}
void solve() {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) {
        int x, y;
        cin >> x >> y;
        f[i][1] = y;
        e[x].push_back(i);
    }
    dfs(0, -1);
    cout << f[0][m + 1] << endl;
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    solve();
    return 0;
}