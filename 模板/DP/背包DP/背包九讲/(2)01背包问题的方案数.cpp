#include <bits/stdc++.h>
using namespace std;
// 01 求01背包问题的最优选法的方案数 https://www.acwing.com/problem/content/description/11/
// 02 求01背包恰好型选法的方案数 https://www.acwing.com/problem/content/280/
struct Solution1{
    const int mod=1e9+7;
    const int inf=0x3f3f3f3f;
    void solve() {
        int n,m;
        cin>>n>>m;
        vector<int> v(n+10),w(n+10);
        for(int i=1; i<=n; i++) {
            cin>>v[i]>>w[i];
        }
        vector<int> f(m+10,-inf),g(m+10);//恰好型背包,因为最多型背包不好计数
        f[0]=0;
        g[0]=1;
        for(int i=1; i<=n; i++) {
            for(int j=m; j>=v[i]; j--) {
                int t=max(f[j],f[j-v[i]]+w[i]);
                int s=0;
                if(t==f[j]) s=(s+g[j])%mod;
                if(t==f[j-v[i]]+w[i]) s=(s+g[j-v[i]])%mod;
                g[j]=s;
                f[j]=t;
            }
        }
        int mx=0;
        for(int i=0;i<=m;i++) mx=max(mx,f[i]);
        int res=0;
        for(int i=0;i<=m;i++){
            if(f[i]==mx){
                res=(res+g[i])%mod;
            }
        }
        cout<<res<<endl;
    }
};