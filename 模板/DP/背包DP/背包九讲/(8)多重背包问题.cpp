#include <bits/stdc++.h>
using namespace std;
// 07 多重背包I  https://www.acwing.com/problem/content/4/
// n=100,v=100
struct Solution7{
    void solve1() { //O(nvv)
        int n,m; // n：物品数量，m：体积
        cin>>n>>m;
        vector<int> v(n+10),w(n+10),s(n+10);
        vector<int> f(m+10);
        for(int i=1; i<=n; i++) cin>>v[i]>>w[i]>>s[i];
        for(int i=1; i<=n; i++) {
            for(int j=m; j>=v[i]; j--) { // 体积
                for(int k=0; k<=s[i]&&k*v[i]<=j; k++) {
                    f[j]=max(f[j],f[j-k*v[i]]+k*w[i]);
                    //决策，内循环，每个i只能选择其中的一个决策
                }
            }
        }
        cout<<f[m]<<endl;
    }
};

// 08 多重背包问题 II https://www.acwing.com/problem/content/5/
// n=1000,v=2000
struct Solution8{
    void solve() {
        int n,m; // n：物品数量，m：体积
        cin>>n>>m;
        vector<int> v(n+10),w(n+10),s(n+10); // v:体积，w：价值,s[i]:物品i的个数
        vector<int> f(m+10);
        for(int i=1; i<=n; i++) cin>>v[i]>>w[i]>>s[i];
        auto get=[&](int x,int idx)->vector<array<int,2>>{ // s[i]二进制拆分
            vector<array<int,2>> ans;
            int k=1;
            while(x>=k){
                ans.push_back({k*v[idx],k*w[idx]});
                x-=k;
                k<<=1;
            }
            if(x){
                ans.push_back({x*v[idx],x*w[idx]});
            }
            return ans;
        };
        for(int i=1; i<=n; i++) {
            vector<array<int,2>> ans=get(s[i],i);
            for(int j=0;j<ans.size();j++){
                for(int k=m;k>=ans[j][0];k--){
                    f[k]=max(f[k],f[k-ans[j][0]]+ans[j][1]);
                }
            }
        }
        cout<<f[m]<<endl;
    }
};

// 09 多重背包问题 III https://www.acwing.com/problem/content/6/ :卡常
// 宝物筛选 https://www.luogu.com.cn/problem/P1776
// n=1000,v=20000,vi<=20000
struct Solution9{
    void solve() { //O(n*m)
        int n,m; // n：物品数量，m：体积
        cin>>n>>m;
        vector<int> f(m+10);
        for(int i=1;i<=n;i++){
            int v,w,s;
            cin>>v>>w>>s; // v:体积， w：价值, s:物品i的个数
            vector<int> g=f;
            for(int j=0;j<v;j++){// 分拆成w[i]个类,
                deque<int> q;	//对每个类使用单调队列　
                for(int k=j;k<=m;k+=v){ // 滑动窗口[k-s[i]*v[i],k-v[i]]
                    while(q.size()&&g[k]>=g[q.back()]+(k-q.back())/v*w) q.pop_back();
                    q.push_back(k);
                    while(q.size()&&q.front()<k-s*v) q.pop_front();
                    if(q.size()) f[k]=max(g[k],g[q.front()]+(k-q.front())/v*w);
                }
            }
        }
        cout<<f[m]<<endl;
    }
};