#include <bits/stdc++.h>
using namespace std;
// 06 混合背包问题 https://www.acwing.com/problem/content/7/
// n:1000,v:1000
struct Solution6{
    void solve1() {
        int n,m; //n:物品数量，m：体积
        cin>>n>>m;
        vector<array<int,3>> a;
        for(int i=1;i<=n;i++){
            int v,w,s;
            cin>>v>>w>>s;
            if(s==0){ //完全背包s设为0
                a.push_back({v,w,0});
            }else{
                if(s==-1){
                    a.push_back({v,w,1});
                }else{ // 多重背包重量以二进制形式进行拆分
                    int k=1;
                    while(s>=k){
                        a.push_back({k*v,k*w,1});
                        s-=k;
                        k<<=1;
                    }
                    if(s){
                        a.push_back({s*v,s*w,1});
                    }
                }
            }
        }
        vector<int> f(m+10);
        for(int i=0;i<a.size();i++){
            int v=a[i][0],w=a[i][1],c=a[i][2];
            if(c==1){ //01背包，逆序遍历
                for(int j=m;j>=v;j--){
                    f[j]=max(f[j],f[j-v]+w);
                }
            }else{ //完全背包，顺序遍历
                for(int j=v;j<=m;j++){
                    f[j]=max(f[j],f[j-v]+w);
                }
            }
        }
        cout<<f[m]<<endl;
    }
};