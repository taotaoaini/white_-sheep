#include <bits/stdc++.h>
using namespace std;
// 06 有依赖的背包问题 https://www.acwing.com/problem/content/10/
// https://www.luogu.com.cn/problem/P1064
// https://www.luogu.com.cn/problem/P2014
// 选择了这个节点，则从这个节点到根节点路径上的点都要选
// 转化思想：考虑以u为根节点的子树的所有的物品
// f[u][j]:表示选择以u为子树的物品，在体积和不超过容量j时所活获得的最大价值
// 当前节点u有i个子节点，每个节点可选可不选,把u的i个节点看作i组物品，每组物品按单位体积拆分进行分组背包,0,1...j-v[u]种决策，相对于组装的思想
//按单位体积拆分是因为子孙中可能出现体积为1的物品，拆分到j-v[u]是因为预留v[u]的空间装入节点u
const int N=110;
int n,m; // n：物品数量，m：体积
vector<int> e[N];
int w[N],v[N];
int f[N][N];
int root;
void dfs(int u,int fa) {
    //只选u节点
    for(int i=v[u]; i<=m; i++) {
        f[u][i]=w[u];
    }
    for(auto& x:e[u]) {
        if(x==fa) continue;
        dfs(x,u);
        // 筛选v节点
        for(int i=m; i>=v[u]; i--) { //预留v[u]的空间放v节点
            for(int j=0; j<=i-v[u]; j++) { //决策只能选择其中的一个，内循环
                f[u][i]=max(f[u][i],f[u][i-j]+f[x][j]);
            }
        }
    }
}
void solve1() {
    cin>>n>>m;
    for(int i=1,x; i<=n; i++) {
        cin>>v[i]>>w[i]>>x;
        if(x==-1) {
            root=i;
            continue;
        }
        e[x].push_back(i);
        e[i].push_back(x);
    }
// 	cout<<root<<endl;
    dfs(root,0);
    cout<<f[root][m]<<endl;
}