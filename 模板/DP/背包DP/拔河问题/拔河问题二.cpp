// 拔河 http://codeup.hustoj.com/problem.php?id=1130
// https://www.cnblogs.com/TaylorSwift13/p/11172401.html
#include <bits/stdc++.h>
using namespace std;
const int N = 45010;
const int inf = 0x3f;
int n;
int f[N][110];
int a[110];

void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    int n = 0;
    while (cin >> n) {
        int sum = 0;
        memset(f, -inf, sizeof(f)); // 恰好型选k人，至少型容量背包
        for (int i = 1; i <= n; i++) cin >> a[i], sum += a[i];
        for (int i = 0; i <= sum / 2; i++) f[i][0] = 0; // 0人的任何容量价值为0
        for (int i = 1; i <= n; i++) {
            for (int j = sum / 2; j >= a[i]; j--) {
                for (int k = i; k >= 1; k--) { // 只能选择k个人
                    f[j][k] = max(f[j][k], f[j - a[i]][k - 1] + a[i]);
                }
            }
        }
        int res = f[sum / 2][n / 2];
        if (n & 1) res = max(res, f[sum / 2][n / 2 + 1]);
        cout << res << " " << sum - res << endl;
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    solve();
    return 0;
}