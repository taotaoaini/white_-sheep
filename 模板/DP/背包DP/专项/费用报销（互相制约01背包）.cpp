// 费用报销 https://dashoj.com/d/problemDay/p/15
#include<bits/stdc++.h>
using namespace std;
#define endl '\n'
int mm[13]{0,31,28,31,30,31,30,31,31,30,31,30,31};
void init(){
    for(int i=1;i<=12;i++) mm[i]+=mm[i-1];
}
const int N=1010;
const int M=5010;
int n,m,k;
struct node{
    int x,y;
}a[N];
int f[N][M]; // f[i , j] 表示前 i 张票据金额限制为 j 凑出最大的金额
bool cmp(const node& t1,const node& t2){
    return t1.x<t2.x;
}
void solve(){
    cin>>n>>m>>k;
    init();
    for(int i=1;i<=n;i++){
        int x,y,w;
        cin>>x>>y>>w;
        a[i].x=mm[x-1]+y;
        a[i].y=w;
    }
    sort(a+1,a+n+1,cmp);
    auto find=[&](int i,int j,int k){
        int l=i-1,r=j+1;
        while(l+1<r){
            int mid=(l+r)/2;
            if(a[mid].x<=k) l=mid; // <=k的最后一个
            else r=mid;
        }
        return l;
    };
    for(int i=1;i<=n;i++){
        for(int j=0;j<=m;j++){
            f[i][j]=f[i-1][j];
            if(j>=a[i].y){
                int id=find(1,i-1,a[i].x-k);
                f[i][j]=max(f[i][j],f[id][j-a[i].y]+a[i].y);
            }
        }
    }
    cout<<f[n][m]<<endl;
}
int main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}