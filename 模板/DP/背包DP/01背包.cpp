#include <bits/stdc++.h>

using namespace std;
using i64 = long long;
const int N = 10000;
//[问题]
//具有传递型性问题，从一个节点到另一个节点，再到另一个节点

// 01 背包板子：https://atcoder.jp/contests/dp/tasks/dp_d
// 02 恰好型背包 https://leetcode.cn/problems/length-of-the-longest-subsequence-that-sums-to-target/description/
// 03 恰好型背包(判断方案数是否为0) https://leetcode.cn/problems/partition-equal-subset-sum/description/
// （恰好型背包:小哥派件装载问题）https://leetcode.cn/contest/sf-tech/problems/cINqyA/
// 04 恰好体积为0型背包(意味着有负数的情况，需要偏移) https://codeforces.com/problemset/problem/366/C
// 05 二维01背包  当A:1+B:1的组合才能拼凑出为w:1的价值时，可以考虑将其中一个作为背包条件，另一个作为价值，跑背包。https://codeforces.com/problemset/problem/837/D
// 这题有个思想：一个数通过乘积得到的末尾0的构造方式：因子2*5，就是看min(2,5)的数量
// 06 01背包恰好型背包，求和等于k的方案数的变形题目 https://leetcode.cn/problems/find-the-sum-of-the-power-of-all-subsequences/
// 07 二维恰好型背包 https://codeforces.com/problemset/problem/730/J
// 08 恰好型背包问题：体积为0，价值最少型背包问题 https://ac.nowcoder.com/acm/contest/77231/F
// [01]朴素型背包
struct Solution1 {
    void solve1() { //朴素写法
        int n, m;
        cin >> n >> m;
        vector<int> w(n + 1);
        vector<int> v(n + 1);
        for (int i = 1; i <= n; i++) cin >> w[i] >> v[i];
        vector<vector<i64>> f(n + 1, vector<i64>(m + 1));
        for (int i = 0; i <= n; i++) for (int j = 0; j <= m; j++) f[i][j] = -1e18;
        f[0][0] = 0;
        for (int i = 1; i <= n; i++) { //物品
            for (int j = 0; j <= m; j++) f[i][j] = f[i - 1][j];
            for (int j = w[i]; j <= m; j++)f[i][j] = max(f[i][j], f[i - 1][j - w[i]] + v[i]);
        }
        cout << f[n][m] << endl;
    }

    void solve2() { //状态压缩写法
        int n, m;
        cin >> n >> m;
        vector<int> w(n + 1);//体积
        vector<int> v(n + 1);//价值
        for (int i = 1; i <= n; i++) cin >> w[i] >> v[i];
        vector<i64> f(m + 1);
        for (int i = 1; i <= n; i++) { //物品
            for (int j = m; j >= w[i]; j--) { //体积
                f[j] = max(f[j], f[j - w[i]] + v[i]);
            }
        }
        cout << f[m] << endl;
    }
};

//int main() {
//    solve1();
//    return 0;
//}

// 02恰好型背包
struct Solution2 {
    int solve1(vector<int> &nums, int target) { // 朴素写法
        int n = nums.size();
        vector<vector<int>> f(n + 1, vector<int>(target + 1,
                                                 -1e9)); //需要设置不可达状态,只有(nums[i]+...+nums[k]==target)才可以,加了这句，就是恰好=型，如果没有这段代码就是<=型
        f[0][0] = 0;
        int s = 0;
        for (int i = 1; i <= n; i++) { //物品
            s += nums[i - 1];
            for (int j = 0; j <= min(s, target); j++)
                f[i][j] = f[i - 1][j];
            for (int j = nums[i - 1]; j <= target; j++) { //容量
                f[i][j] = max(f[i][j], f[i - 1][j - nums[i - 1]] + 1); //可达状态一定是之前得到的
            }
        }
        if (f[n][target] <= 0)
            return -1;
        else
            return f[n][target];
    }

    int sovle2(vector<int> &nums, int target) { // 状态压缩写法
        vector<int> f(target + 1, -1e9);
        int s = 0;
        f[0] = 0;
        for (auto &x: nums) { //物品
            s += x;
            for (int j = min(target, s); j >= x; j--) { //容量
                f[j] = max(f[j], f[j - x] + 1);
            }
        }
        return f[target] > 0 ? f[target] : -1;
    }
};

// 03 恰好体积为0型背包
struct Solution3 {
    int a[N];
    int b[N];
    int n, k;

    void solve1() {
        const int inf=-0x3f3f3f3f;
        cin >> n >> k;
        for (int i = 1; i <= n; i++) cin >> a[i];
        for (int i = 1; i <= n; i++) cin >> b[i], b[i] = a[i] - k * b[i];
        const int BASE = 100 * 100 * 10;
        // a-k*b=0;
        vector<int> f(BASE * 2 + 10, inf);
        f[BASE] = 0;
        for (int i = 1; i <= n; i++) {
            auto pd = f;
            for (int j = -BASE; j <= BASE; j++) {
                if (j + BASE + b[i] >= 0 && j + BASE + b[i] <= BASE * 2)
                    f[j + BASE] = max(f[j + BASE], pd[j + BASE + b[i]] + a[i]);
            }
        }
        cout << (f[BASE] <= 0 ? -1 : f[BASE]) << endl;
    }

    void solve2() {
        const int inf = -1e9; //不可达状态
        cin >> n >> k;
        for (int i = 1; i <= n; i++) cin >> a[i];
        for (int i = 1; i <= n; i++) cin >> b[i], b[i] = a[i] - k * b[i];
        const int BASE = 100 * 100 * 10;
        // a-k*b=0;
        vector<int> f(BASE * 2 + 10, inf);
        f[BASE] = 0;
        for (int i = 1; i <= n; i++) {
            auto pd = f; //O(n)
            for (int j = 0; j <= 2 * BASE; j++) {
                if (j + b[i] >= 0) //b可正可负所以需要判断
                    f[j] = max(f[j], pd[j + b[i]] + a[i]);
            }
            // cout<<a[i]<<" "<<b[i]<<endl;
        }
        cout << (f[BASE] <= 0 ? -1 : f[BASE]) << endl;
    }

    void solve3() {
        const int inf = -1e9; //不可达状态
        cin >> n >> k;
        for (int i = 1; i <= n; i++) cin >> a[i];
        for (int i = 1; i <= n; i++) cin >> b[i], b[i] = a[i] - k * b[i];
        const int BASE = 100 * 100 * 10;
        // a-k*b=0;
        vector<int> f(BASE * 2 + 10, inf);
        f[BASE] = 0;
        for (int i = 1; i <= n; i++) {
            auto pd = f; //O(n)
            for (int j = 0; j <= 2 * BASE; j++) {
                if (j - b[i] >= 0 && j - b[i] <= 2 * BASE) //b可正可负所以需要判断
                    f[j] = max(f[j], pd[j - b[i]] + a[i]);
            }
            // cout<<a[i]<<" "<<b[i]<<endl;
        }
        cout << (f[BASE] <= 0 ? -1 : f[BASE]) << endl;
    }
};

struct Solution4{
    void solve1(){ //O(nVM)
        int n,V,W;// n:物品数量，V：体积量，W；重量
        cin>>n>>V>>W;
        vector<int> a(n+1),v(n+1),w(n+1);
        for(int i=1;i<=n;i++) cin>>v[i]; //体积
        for(int i=1;i<=n;i++) cin>>w[i]; //重量
        for(int i=1;i<=n;i++) cin>>a[i]; //价值
        vector<vector<int>> f(V+10,vector<int>(W+10,0));
        for(int i=1;i<=n;i++){
            for(int j=V;j>=v[i];j--){ //体积
                for(int k=W;k>=w[i];k--){ //重量
                    f[j][k]=max(f[j][k],f[j-v[i]][k-w[i]]+a[i]);
                }
            }
        }
        cout<<f[V][W]<<endl;
    }
};

struct Solution5{
    void solve1(){
        const int inf = -1e9;
        int n,m;
        cin>>n>>m;
        vector<int> b(n+1);
        vector<int> c(n+1);
        int s=0;
        auto get2=[&](i64 x)->int{
            int res=0;
            while(x%2==0){
                res+=1;
                x/=2;
            }
            return res;
        };
        auto get5=[&](i64 x)->int{
            int res=0;
            while(x%5==0){
                res+=1;
                x/=5;
            }
            return res;
        };
        i64 x=0;
        for(int i=1;i<=n;i++){
            cin>>x;
            b[i]=get2(x);
            c[i]=get5(x);
            s+=c[i];
        }
        vector<vector<int>> f(m+1,vector<int>(s+1,inf));
        f[0][0]=0;
        for(int i=1;i<=n;i++){
            for(int j=m;j>=1;j--){
                for(int k=s;k>=c[i];k--){
                    f[j][k]=max(f[j][k],f[j-1][k-c[i]]+b[i]); //此处可以
                }
            }
        }
        int res=0;
        for(int i=1;i<=s;i++){ //恰好型选择
            res=max(res,min(i,f[m][i]));
        }
        cout<<res<<endl;
    }
};


// 06 求出所有子序列的能量和 https://leetcode.cn/problems/find-the-sum-of-the-power-of-all-subsequences/description/
// 01恰好型背包的好题
struct Solution6{
    const int mod=1e9+7;
    int qmi(int a,int b){
        int res=1;
        while(b){
            if(b&1) res=1LL*res*a%mod;
            a=1LL*a*a%mod;
            b/=2;
        }
        return res;
    }
    int sumOfPower(vector<int>& a, int k) { //01背包好题
        int n=a.size();
        vector<vector<int>> f(k+10,vector<int>(n+10)); //表示和为i时，用了j个数
        f[0][0]=1; //初始状态
        for(int i=0;i<n;i++){ //物品
            for(int j=k;j>=a[i];j--){ //倒枚举，状态压缩
                for(int z=1;z<=n;z++){ //选的个数
                    f[j][z]=(f[j][z]+f[j-a[i]][z-1])%mod;
                }
            }
        }
        int res=0;
        for(int i=1;i<=n;i++){
            if(f[k][i]==0) continue;
            res=(res+1LL*f[k][i]*qmi(2,n-i))%mod;
        }
        return res;
    }
};

struct Solution8{
    const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
    void solve() {
        //freopen("a.in","r",stdin);
        //freopen("a.out","w",stdout);
        int n;
        cin>>n;
        vector<int> a(n+10);
        for(int i=1;i<=n;i++){
            cin>>a[i];
        }
        //背包DP：恰好型背包问题：体积为0，价值最少背包问题
        vector<int> f(130,inf);
        for(int i=1;i<=n;i++){ //O(n*m)
            f[a[i]]=1;
            for(int j=0;j<=127;j++){
                int v=j&a[i];
                f[v]=min(f[v],f[j]+1);
            }
        }
        cout<<(f[0]==inf?-1:n-f[0])<<endl;
    }
};