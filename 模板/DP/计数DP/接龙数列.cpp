// 接龙数列 https://www.acwing.com/file_system/file/content/whole/index/content/10768187/
// 分析： 12 23
// f[i]：以i为结尾的最长接龙数列
// s: st en  f[en]=max(f[st]+1,f[en]) //他可以接到以st为结尾的后面

#include<bits/stdc++.h>
using namespace std;
const int N = 20;
int f[N]; // 以i为尾的节点
int n;
void solve() {
    cin >> n;
    for (int i = 1; i <= n; i++) {
        string s;
        cin >> s;
        int x = s[0] - '0';
        int y = s[s.length() - 1] - '0';
        f[y] = max(f[x] + 1, f[y]);
    }
    int res = 0;
    for (int i = 0; i <= 9; i++) {

        res = max(res, f[i]);
    }
    cout << n - res << endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    solve();
    return 0;
}