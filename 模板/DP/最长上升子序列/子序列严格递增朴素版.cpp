// 1<=n<=1e5
#include<bits/stdc++.h>

using namespace std;

int f1(vector<int> &nums) { // O(n^2)
    int n = nums.size();
    vector<int> f(n);
    int res = 0;
    for (int i = 0; i < n; i++) {
        f[i] = 0;
        for (int j = 0; j < i; j++) {
            if (nums[j] < nums[i]) {
                f[i] = max(f[i], f[j]);
            }
        }
        f[i]++;
        res = max(res, f[i]);
    }
    return res;
}


int f2(vector<int> &nums) {
    vector<int> g;
    for (int x : nums) {
        auto it = lower_bound(g.begin(),g.end(),x);
        if (it == g.end()) {
            g.push_back(x); // >=x 的 g[j] 不存在
        } else {
            *it = x;
        }
    }
    return g.size();
}