// 最长递增子序列 II https://leetcode.cn/problems/longest-increasing-subsequence-ii/description/
// （1）子序列严格递增 （https://leetcode.cn/problems/longest-increasing-subsequence-ii/description/）
// （2）子序列中相邻元素的差值<=k
// （3）1<=n<=1e5,1<=nums[i]<=1e5

#include<bits/stdc++.h>

using namespace std;
using i64 = long long;
using f64 = long double;
#define fi first
#define se second
#define ls (p*2)
#define rs (p*2+1)
#define mid ((l+r)/2)
const int N = 100010;

vector<int> tr;

void pushdown(int p) {
    tr[p] = max(tr[ls], tr[rs]);
}

void change(int p, int l, int r, int x, int val) {
    if (l == r) {
        tr[p] = max(tr[p], val);
        return;
    }
    if (x <= mid) change(ls, l, mid, x, val);
    else change(rs, mid + 1, r, x, val);
    pushdown(p);
}

int query(int p, int l, int r, int ql, int qr) {
    if (ql <= l && qr >= r) return tr[p];
    int res = 0;
    if (ql <= mid) res = max(res, query(ls, l, mid, ql, qr));
    if (qr > mid) res = max(res, query(rs, mid + 1, r, ql, qr));
    return res;
}

int lengthOfLIS(vector<int> &nums, int k) {
    tr.assign(N * 4 + 10, 0);
    int res = 0;
    int n = nums.size();
    for (int i = 0; i < nums.size(); i++) {
        int l = max(nums[i] - k, 1);
        int r = nums[i] - 1; // 严格递增
        // [l,r]
        int x = 0;
        if (l <= r) {
            x = query(1, 1, N, l, r);
        }
        change(1, 1, N, nums[i], x + 1);
    }
    return tr[1];
}
