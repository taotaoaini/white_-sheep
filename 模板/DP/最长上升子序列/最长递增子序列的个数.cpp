// 最长递增子序列的个数 https://leetcode.cn/problems/number-of-longest-increasing-subsequence/description/
// 1<=n<=1e5
#include<bits/stdc++.h>

using namespace std;
#define fi first
#define se second
#define ls (p*2)
#define rs (p*2)
#define mid ((l+r)/2)
#define mp make_pair
using i64 = long long;
using f64 = long double;
using pii = pair<int, int>;

// 1.DP朴素做法
int f1(vector<int> &nums) { // O(n^2)
    int n = nums.size();
    vector<int> f(n + 1), g(n + 1);
    int res = 0;
    for (int i = 0; i < n; i++) {
        f[i] = 1;  // 以i结尾的最长子序列
        g[i] = 1;  // 以i结尾的最长子序列方案数
        for (int j = 0; j < i; j++) {
            if (nums[i] > nums[j]) {
                if (f[i] < f[j] + 1) {
                    f[i] = f[j] + 1;
                    g[i] = g[j];
                } else if (f[i] == f[j] + 1) {
                    g[i] += g[j];
                }
            }
        }
        res = max(res, f[i]);
    }
    int s = 0;
    for (int i = 0; i < n; i++) if (f[i] == res) s += g[i];
    return s;
}

// 树状数组优化DP
int f2(vector<int> &nums) {
    vector<int> a = nums;
    sort(a.begin(), a.end());
    a.erase(unique(a.begin(), a.end()), a.end()); // 离散去重
    int an = a.size();
    int n = nums.size();
    vector<pii> tr(an + 10);
    auto lowbit = [&](int x) {
        return x & -x;
    };
    auto add = [&](int x, pii t) {
        for (int i = x; i <= an; i += lowbit(i)) {
            int k = tr[i].fi, cnt = tr[i].se; // k:长度,cnt:个数
            if (k == t.fi) {
                cnt += t.se;
            } else if (k < t.fi) {
                k = t.fi;
                cnt = t.se;
            }
            tr[i].fi = k;
            tr[i].se = cnt;
        }
    };
    auto query = [&](int x) {
        pii res;
        int &k = res.fi, &cnt = res.se;
        for (int i = x; i >= 1; i -= lowbit(i)) {
            if (k == tr[i].fi) {
                cnt += tr[i].se;
            } else if (k < tr[i].fi) {
                k = tr[i].fi;
                cnt = tr[i].se;
            }
        }
        return res;
    };
    for (int i = 0; i < n; i++) {
        int id = lower_bound(a.begin(), a.end(), nums[i]) - a.begin() + 1;
        pii tmp = query(id - 1);
        int len = tmp.fi, cnt = tmp.se;
        add(id, mp(len + 1, max(cnt, 1)));
    }
    return query(an).se;
}