// 好数之和 https://www.lanqiao.cn/problems/2226/learning/?subject_code=2
#include<bits/stdc++.h>
#define fi first
#define se second
using namespace std;
using pll = pair<long long,long long>;
using i64 = long long;
using f128 = long double;
pair<i64, i64>dp[22][12][12][12][12][2];
i64 ppow[60];
i64 cnt[60];
pair<i64,i64> dfs(int x,int a,int b,int c,int d,int ok,int is_limit) { // 返回 答案数+ 符合条件要求的和
    if(x==0) {
        if(ok) {
            return {1,0}; // 符合条件
        } else {
            return {0,0}; // 不符合条件
        }
    }
    if(!is_limit&&dp[x][a][b][c][d][ok].se!=0) return dp[x][a][b][c][d][ok];
    pair<i64,i64> ans;
    int up=is_limit?cnt[x]:9;
    for(int i=0; i<=up; i++) {
        pair<i64,i64> t=dfs(x-1,b,c,d,i,ok||(b==2&&c==0&&d==2&&i==2),is_limit&&i==up);
        ans.fi=ans.fi+t.fi;
        ans.se=ans.se+t.se+1LL*i*ppow[x-1]*t.fi; // 累计贡献值 i000..
    }
    if(!is_limit) dp[x][a][b][c][d][ok]=ans;
    return ans;
}

i64 get(i64 x) {
    memset(cnt,0,sizeof(cnt));
    int len=0;
    while(x) {
        cnt[++len]=x%10;
        x/=10;
    }
    return dfs(len, 0, 0, 0, 0, 0, 1).se;
}
void solve() {
    ppow[0]=1;
    i64 a,b;
    for(int i=1; i<=10; i++) ppow[i]=ppow[i-1]*10;
    cin>>a>>b;
    cout<<get(b)-get(a-1)<<endl;
}
int main() {
//	freopen("a.in","r",stdin);
//	freopen("a.out","w",stdout);
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    int t=1;
    // cin>>t;
    while(t--) {
        solve();
    }
}
