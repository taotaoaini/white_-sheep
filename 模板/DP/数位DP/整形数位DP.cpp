// 1.启示录  https://www.acwing.com/problem/content/312/


// 这题做法是二分+数位DP（check）
// 此时数位DP会跑很多次
// 优化点：
// 如果dfs顺序是从0开始遍历s=“123456”的，记忆化搜索数组不能复用,因为当s不相同时，其数组表示存储的含义发送改变
// 此时应该逆序遍历 从最高位开始遍历 s="123456" dfs(s.lenght())开始,此时记忆化数组确定了位数，f[i]表式 9..99（一共i位）的方案数

#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
const int N=40;
i64 f[N][N][5];
int a[N];
i64 dfs(int pos,int cnt,bool k,bool limit,bool is_num) {
    if(!pos) return k;
    i64 res=0;
    if(!is_num) res+=dfs(pos-1,0,0,false,false);
    if(is_num&&!limit&&f[pos][cnt][k]!=-1) return f[pos][cnt][k];
    int up=limit?a[pos]:9;
    int low=is_num?0:1;
    for(int d=low; d<=up; d++) {
        if(d==6){
            res+=dfs(pos-1,cnt+1,k||cnt+1>=3,limit&&d==up,true); // 只要取值了就是数字了，is_num=true
        }else{
            res+=dfs(pos-1,0,k,limit&&d==up,true);
        }
    }
    if(is_num&&!limit) f[pos][cnt][k]=res;
    return res;
}
i64 cal(i64 x){ //将x的各个数位取出，逆序存放在数组中
    int cnt=0;
    while(x) a[++cnt]=x%10,x/=10;
    return dfs(cnt,0,0,1,0);
}
void solve() {
    int T;
    cin>>T;
    memset(f,-1,sizeof(f)); //此时只需要初始化一次
    while(T--) {
        int n;
        cin>>n;
        i64 l=0,r=5e10+10;
        while(l+1<r) {
            i64 mid=(l+r)/2;
            if(cal(mid)>=n) r=mid;
            else l=mid;
        }
        cout<<r<<endl;
    }
}
int main() {
    solve();
    return 0;
}