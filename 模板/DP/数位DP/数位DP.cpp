#include <bits/stdc++.h>
using namespace std;
// (1)给定正整数 n，返回在 [1, n] 范围内具有 至少 1 位 重复数字的正整数的个数。
//至少有 1 位重复的数字 https://leetcode.cn/problems/numbers-with-repeated-digits/description/
//正难则反
struct Solution1{
        int numDupDigitsAtMostN(int n) { //整数型数位DP
            auto s = to_string(n);
            int m = s.length(), memo[m][1 << 10];
            memset(memo, -1, sizeof(memo)); // -1 表示没有计算过
            function<int(int, int, bool, bool)> f = [&](int i, int mask, bool is_limit, bool is_num) -> int {
                if (i == m)
                    return is_num; // is_num 为 true 表示得到了一个合法数字
                if (!is_limit && is_num && memo[i][mask] != -1)
                    return memo[i][mask];
                int res = 0;
                if (!is_num) // 可以跳过当前数位
                    res = f(i + 1, mask, false, false);
                int up = is_limit ? s[i] - '0' : 9; // 如果前面填的数字都和 n 的一样，那么这一位至多填数字 s[i]（否则就超过 n 啦）
                for (int d = 1 - is_num; d <= up; ++d) // 枚举要填入的数字 d
                    if ((mask >> d & 1) == 0) // d 不在 mask 中
                        res += f(i + 1, mask | (1 << d), is_limit && d == up, true);
                if (!is_limit && is_num)
                    memo[i][mask] = res;
                return res;
            };
            return n - f(0, 0, true, false);
        }
};

//（2）二进制数位DP
// 不含连续1的非负整数 https://leetcode.cn/problems/non-negative-integers-without-consecutive-ones/description/
struct Solution2{
    int findIntegers(int n) {
        int m = __lg(n), dp[m + 1][2]; //__lg(x) 从0开始算，最高位：m位
        memset(dp, -1, sizeof(dp));
        function<int(int, bool, bool)> f = [&](int i, bool pre1, bool is_limit) -> int { //pre1:pre表示第 i−1位是否为1，如果为真则当前位不能填1
            if (i < 0) return 1;
            if (!is_limit && dp[i][pre1] >= 0) return dp[i][pre1];
            int up = is_limit ? n >> i & 1 : 1;
            int res = f(i - 1, false, is_limit && up == 0); // 填 0
            if (!pre1 && up == 1) res += f(i - 1, true, is_limit); // 填 1
            if (!is_limit) dp[i][pre1] = res;
            return res;
        };
        return f(m, false, true); // i 从 m 往小枚举，方便位运算
    }
};