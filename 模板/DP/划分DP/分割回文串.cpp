// 分割回文串 II https://leetcode.cn/problems/palindrome-partitioning-ii/description/
#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
    int minCut(string s) {
        int n = s.length();
        s = " " + s;
        vector<vector<int>> f(n + 1, vector<int>(n + 1));
        for (int i = n; i >= 1; i--) { // 需要i+1状态
            for (int j = i; j <= n; j++) { // 需要j-1状态
                if (i == j) f[i][j] = 1;
                else {
                    if (s[i] == s[j]) { // 当长度只有2，或者[i+1:j-1]符合条件
                        if (j - i + 1 == 2 || f[i + 1][j - 1]) f[i][j] = 1;
                    }
                }
            }
        }
        vector<int> dp(n + 1); // 表示当前1到i的划分情况
        for (int i = 1; i <= n; i++) {
            if (f[1][i]) {
                dp[i] = 0;
            } else {
                dp[i] = i - 1;
                for (int j = i; j >= 1; j--) {
                    if (f[j][i]) {
                        dp[i] = min(dp[i], dp[j - 1] + 1);
                    }
                }
            }
        }
        return dp[n];
    }
};