// 积木画 https://www.lanqiao.cn/problems/2110/learning/?page=1&first_category_id=1&name=%E7%A7%AF%E6%9C%A8%E7%94%BB
#include<bits/stdc++.h>

// 必须手玩，手玩，手玩。。。。。。
/*
 DP状态定义: 当前位置i，状态量为0.刚好铺平上下位置，1.上面差一个，2.下面差一个
【DP】分三种情况：
1.刚好能铺平两个dp[i][0]
2.上面差一个dp[i][1]
3.下面差一个dp[i][2]
于是dp[i][0] = dp[i - 1][0] + dp[i - 2][0] + dp[i - 1][1] + dp[i - 1][2]
即当前铺满可以从前面一个只差竖着的一块再加上竖着的一块，
前面只差两块补上横着的两块，前面没铺满上面吐出来一块，
前面没铺满下面凸出来一块。
dp[i][1] = dp[i - 2][0] + dp[i - 1][2]
dp[i][2] = dp[i - 2][0] + dp[i - 1][1]
*/

using namespace std;
const int mod = 1e9 + 7;
const int N = 10000010;
using i64 = long long;
i64 dp[N][4];

void solve() {
    int n;
    cin >> n;
    dp[1][0] = 1;
    dp[1][1] = 0;
    dp[1][2] = 0;

    dp[2][0] = 2;
    dp[2][1] = 1;
    dp[2][2] = 1;
    for (int i = 3; i <= n; i++) {
        dp[i][0] = (dp[i - 1][0] + dp[i - 2][0] + dp[i - 1][1] + dp[i - 1][2]) % mod;
        dp[i][1] = (dp[i - 1][2] + dp[i - 2][0]) % mod;
        dp[i][2] = (dp[i - 1][1] + dp[i - 2][0]) % mod;
    }
    cout << dp[n][0] << endl;
}

int main() {
    solve();
    return 0;
}