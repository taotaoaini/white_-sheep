// https://www.luogu.com.cn/problem/P1300
#include<bits/stdc++.h>
using namespace std;

//  北、西、南、东 0、1、2、3 //按时钟顺序才有mod方式
// 方向是w， 左转：(w+1) mod 4 ,右转：(w+3) mod 4 ,掉头: (w+2) mod 4
const int N=35;// 先预处理一下各个方向位移之后坐标的变化量
int dx[4]{-1,0,1,0},dy[4]{0,-1,0,1};
char s[N];
int n,m,sx,sy,sw,res=1<<30,dis[N][N][4];//dis[x][y][w]表示到达第x行第j列方向为w的最小代价
short mp[N][N];
void dfs(int x,int y,int w,int c){
    if(dis[x][y][w]<=c||c>=res) return;//如果发现当前代价大于等于当前点的最小值或者答案，就停止搜索,减枝，可能会存在环
    dis[x][y][w]=c;
    if(mp[x][y]==2) res=min(res,dis[x][y][w]);//到达终点，刷新最小值
    bool flg=0;
    int fx=x+dx[w],fy=y+dy[w];//直行
    int lw=(w+1)%4,lx=x+dx[lw],ly=y+dy[lw];//左转
    int bw=(w+2)%4,bx=x+dx[bw],by=y+dy[bw];//掉头
    int rw=(w+3)%4,rx=x+dx[rw],ry=y+dy[rw];//右转
    if(mp[fx][fy]) flg=1,dfs(fx,fy,w,c);//直行
    if(mp[lx][ly]) flg=1,dfs(lx,ly,lw,c+1);//左转
    if(mp[rx][ry]) flg=1,dfs(rx,ry,rw,c+5);//右转
    if(mp[bx][by]&&!flg) dfs(bx,by,bw,c+10);//掉头
}

int main(){
    scanf("%d%d",&n,&m);
    for(int i=1;i<=n;i++){
        scanf("%s",s+1);
        for(int j=1;j<=m;j++){
            mp[i][j]=1; //把能到达的赋值
            switch(s[j]){
                case '.':{mp[i][j]=0;break;}//不能到达的归零
                case '#':{break;}
                case 'F':{mp[i][j]=2;break;}//终点记号
                case 'N':{sx=i,sy=j,sw=0;break;}
                case 'W':{sx=i,sy=j,sw=1;break;}
                case 'S':{sx=i,sy=j,sw=2;break;}
                case 'E':{sx=i,sy=j,sw=3;break;}
            }
        }
    }
    memset(dis,63,sizeof dis);
    dfs(sx,sy,sw,0);
    printf("%d\n",res);
    return 0;
}