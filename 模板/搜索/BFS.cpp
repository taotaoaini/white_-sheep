#include <bits/stdc++.h>

using namespace std;

//  扫雷 https://www.acwing.com/problem/content/689/
using i64 = long long;
using pii = pair<int,int>;
#define fi first
#define se second
const int N = 310;
string a[N];
int n;
int dx[8]{0, 0, 1, 1, 1, -1, -1, -1};
int dy[8]{1, -1, 0, 1, -1, 0, 1, -1};
int b[N][N];

void solve(int t) {
    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
        a[i] = " " + a[i];
    }
    //BFS
    int res = 0;
    auto check = [&](int x, int y) -> bool {
        return x >= 1 && x <= n && y >= 1 && y <= n;
    };
    auto gg = [&](int x, int y) -> int {
        int res = 0;
        for (int i = 0; i < 8; i++) {
            int xx = dx[i] + x;
            int yy = dy[i] + y;
            if (check(xx, yy) && a[xx][yy] == '*') res++;
        }
        return res;
    };
    auto bfs = [&](int x, int y) {
        queue<pii> q;
        q.push({x, y});
        b[x][y] = -1;
        while (!q.empty()) {
            auto p = q.front();
            q.pop();
            int x = p.fi, y = p.se;
            int sgn = b[x][y];
            for (int i = 0; i < 8; i++) {
                int xx = x + dx[i];
                int yy = y + dy[i];
                if (check(xx, yy)) {
                    if (b[xx][yy] == 0) q.push({xx, yy});
                    b[xx][yy] = -1;
                }
            }
        }
    };
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (a[i][j] == '*') b[i][j] = -1;
            else {
                b[i][j] = gg(i, j);
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (b[i][j] == 0) {
                bfs(i, j);
                res++;
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (b[i][j] > 0) res += 1;
        }
    }
    cout << "Case #" << t << ": " << res << endl;
    memset(b, 0, sizeof(b));
}