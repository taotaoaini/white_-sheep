// Lights G  https://www.luogu.com.cn/problem/P2962
// 折半搜索
#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
const int N=40;
const int inf=0x3f3f3f3f;
int n,m;
i64 a[N];
map<i64,int> pp;
void solve(){
    cin>>n>>m;
    a[0]=1;
    for(int i=1;i<n;i++) a[i]=a[i-1]*2;
    for(int i=1;i<=m;i++){
        int x,y;
        cin>>x>>y;
        x--;
        y--;
        a[x]|=(1LL<<y);
        a[y]|=(1LL<<x);
    }
    int md=(1+n)/2;
    for(int i=0;i<(1LL<<(md));i++){
        int cnt=0;
        i64 x=0;
        for(int j=0;j<md;j++){
            if(i&(1LL<<j)){
                x^=(a[j]);
                cnt+=1;
            }
        }
        if(!pp.count(x)) pp[x]=cnt;
        else pp[x]=min(pp[x],cnt);
    }
    int res=inf;
//    for(i64 i=(1LL<<md);i<(1LL<<(n));i++){
//        int cnt=0;
//        i64 x=0;
//        i64 y=(1LL<<n)-1;
//        for(int j=md;j<n;j++){
//            if(i&(1LL<<j)){
//                x^=(a[j]);
//                cnt+=1;
//            }
//        }
//        y^=x;
//        if(pp.count(y)) res=min(res,pp[y]+cnt);
//    }
    for(i64 i=0;i<(1LL<<(n-md));i++){
        int cnt=0;
        i64 x=0;
        i64 y=(1LL<<n)-1;
        for(int j=0;j<n-md;j++){
            if(i&(1LL<<j)){
                x^=(a[j+md]);
                cnt+=1;
            }
        }
        y^=x;
        if(pp.count(y)) res=min(res,pp[y]+cnt);
    }
//    for (int i = 0; i < (1 << (n - md)); ++i) {  // 对后一半进行搜索
//        i64 x = 0;
//        int cnt = 0;
//        for (int j = 0; j < (n - n / 2); ++j) {
//            if ((i >> j) & 1) {
//                x ^= a[md + j];
//                ++cnt;
//            }
//        }
//        if (pp.count(((1LL << n) - 1) ^ x))
//            res = min(res, cnt + pp[((1LL << n) - 1) ^ x]);
//    }
    cout<<res<<endl;
}
int main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}