// 折半搜索（又称meet in the middle）
// 将原有的数据分成两部分分别进行搜索，最后在中间合并的算法。
// 对n的大小进行搜索所需要的时间复杂度为O(f(n)),合并的时间复杂度为O(g(n))
// 折半的时间复杂度为O(2*f(n/2))+O(g(n))


// 世界冰球锦标赛  https://www.luogu.com.cn/problem/P4799
//  Balanced Cow Subsets G https://www.luogu.com.cn/problem/P3067
// 这题n（40）很小，考虑暴力搜索，选和不选2^40
// 折半搜索思想，将比赛分为L和R两部分，分别算出两部分的所有可能的花费方案数,再合并方案数
// 思考：能记忆化搜索吗

// https://www.cnblogs.com/pjykk/p/15369832.html

#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 50;
int n;
i64 m; // 不开long long见祖宗
i64 a[N];
i64 L[1 << 21]; // 存放左半数据
i64 R[1 << 21]; // 存放右半数据
int cnt1 = 0, cnt2 = 0;
void dfs1(int l, int r, i64 s) { // [l,r]
    if (s > m) return ;
    if (l > r) { //注意判定方法
        L[++cnt1] = s; //增添一种新方案
        return;
    }
    dfs1(l + 1, r, s + a[l]); // 看l这场比赛
    dfs1(l + 1, r, s); // 不看
}
void dfs2(int l, int r, i64 s) {
    if (s > m) return ;
    if (l > r) {
        R[++cnt2] = s;
        return;
    }
    dfs2(l + 1, r, s + a[l]);
    dfs2(l + 1, r, s);
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> m;
    for (int i = 1; i <= n; i++) cin >> a[i];
    dfs1(1, n / 2, 0);
    dfs2(n / 2 + 1, n, 0);
    sort(L + 1, L + cnt1 + 1); // 对l数组进行排序，方便后续的合并操作
    i64 res = 0;
    for (int i = 1; i <= cnt2; i++) {
        // 显然，如果两部分价钱的和不超过m，那就有了一种总的方案
        res += upper_bound(L + 1, L + cnt1 + 1, m - R[i]) - L - 1;
    }
    cout << res << endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}
