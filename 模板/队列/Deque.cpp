#include<bits/stdc++.h>
#include<stdlib.h>

using namespace std;

typedef struct node {
    char tag; //对应的值
    int next;//下一个节点
    int pre;//前一个节点
} node;

//记录开始的位置
int front = 0;
int n;
int delC = 0;
queue<int> doing;


void del(int p, vector <node> &data) {

    //不考虑已经删除过的元素
    if (data[p].tag == 1) {
        return;
    }

    //算法题，不考虑内存溢出，中间丢失也没关系

    // 最后一个节点，没有再后面的了，不用修改后面节点的前驱
    if (data[p].next < n) {
        data[data[p].next].pre = data[p].pre;
        doing.push(data[p].next);
    }

    //头节点发生变更
    if (data[p].pre < 0) {
        front = data[p].next;
    } else {
        data[data[p].pre].next = data[p].next;
        doing.push(data[p].pre);
    }
    data[p].tag = 1;//标记已经删除过了
    delC++; //增加删除的数量
}

int main() {
    queue<int> q;
    string s;
    cin >> s;

    n = s.length();

    //用于保存信息
    vector <node> data(n);
    for (int i = 0; i < n; i++) {
        doing.push(i);
        data[i].tag = 0;
        data[i].pre = i - 1;
        data[i].next = i + 1;
    }

    int p;

    while (!doing.empty()) {
        while (!doing.empty()) {
            p = doing.front();
            doing.pop();

            //已经删除的不再判断
            if (data[p].tag == 1) {
                continue;
            }

            //表示没有前面一个
            if (data[p].pre < 0) {
                p = data[p].next;
                continue;
            }

            //表示一轮结束了
            if (data[p].next >= n) {
                break;
            }

            if (s[data[p].pre] == s[p]) {
                //后面要不相等
                if (s[data[p].next] != s[p]) {
                    q.push(p);
                    q.push(data[p].next);
                }
            } else {
                //如果后面相等也行
                if (s[data[p].next] == s[p]) {
                    q.push(p);
                    q.push(data[p].pre);
                }
            }

            p = data[p].next;
        }

        while (!q.empty()) {
            del(q.front(), data);
            q.pop();
        }
    }

    if (delC >= n) {
        printf("EMPTY");
    } else {
        p = front;
        while (p < n) {
            printf("%c", s[p]);
            p = data[p].next;
        }
    }
    return 0;
}