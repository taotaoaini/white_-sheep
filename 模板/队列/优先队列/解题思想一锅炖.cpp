#include<bits/stdc++.h>
using namespace std;
using i64=long long;
using pll =pair<long long,long long>;
#define fi first
#define se second

// 1.https://www.lanqiao.cn/problems/3541/learning/
// 给m个一次函数，找n个点使总权值最大 => f(x)*x,此问题可以考虑做差，f(x+1)*(x+1)-f(x)*x，最大堆存储差值。
// (k*(x+1)+b)*(x+1)-(k*x+b)*x=2*k*x+k+b
priority_queue<array<i64,3>,vector<array<i64,3>>,less<array<i64,3>>> q;
void solve1(){
    int n,m;
    cin>>n>>m;
    i64 k,b;
    vector<array<i64,2>> a(m);
    for(int i=0;i<m;i++){
        cin>>k>>b;
        a[i][0]=k;
        a[i][1]=b;
        i64 y=b+k;
        if(y<=0) continue;
        q.push({y,1,i});
    }
    i64 res=0;
    for(int i=0;i<n;i++){
        if(q.empty()) break;
        auto p=q.top();
        q.pop();
        i64 y=p[0];
        i64 x=p[1];
        i64 idx=p[2];
        res+=y;
        y=2*a[idx][0]*x+a[idx][0]+a[idx][1];
        if(y<=0) continue;
        q.push({y,x+1,idx});
    }
    cout<<res<<endl;
}