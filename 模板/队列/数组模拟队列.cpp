#include<bits/stdc++.h>

using namespace std;
using i64 = long long;
#define first fi
#define second se

//1.数目模拟队列
//整数删除 https://www.lanqiao.cn/problems/3515/learning/
struct Solution1 {
    struct st {
        i64 val;
        int id;
    };

    struct cmp { // 重写cmp
        bool operator()(st x, st y) {
            if (x.val == y.val) {
                return x.id > y.id;
            }
            return x.val > y.val;
        }
    };

    void solve1() {
        int n, k;
        cin >> n >> k;
        priority_queue<st, vector<st>, cmp> q;
        vector<int> L(n + 10);
        vector<int> R(n + 10);
        vector<i64> add(n + 10, 0);
        for (int i = 1, x; i <= n; i++) {
            cin >> x;
            q.push({x, i});
            L[i] = i - 1;
            R[i] = i + 1;
        }
        while (q.size() > n - k) {
            auto p = q.top();
            q.pop();
            i64 x = p.val;
            int y = p.id;
            if (add[y]) {
                q.push({x + add[y], y});
                add[y] = 0;
            } else {
                int pl = L[y], pr = R[y];
                add[pl] += x, add[pr] += x;
                L[pr] = pl;
                R[pl] = pr;
            }
        }
        vector<i64> ans(n + 1, -1);
        while (q.size()) {
            auto p = q.top();
            q.pop();
            i64 x = p.val;
            int y = p.id;
            ans[y] = x + add[y];
        }
        for (int i = 1; i <= n; i++) if (ans[i] != -1) cout << ans[i] << " ";
        cout << endl;
    }
};