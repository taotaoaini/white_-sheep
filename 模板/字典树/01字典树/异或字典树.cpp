#include<bits/stdc++.h>

using namespace std;
 const int N=50010;
// 1.异或字典树
// 动态开点
struct xorTrie {//xor trie
public:
    const int L = 30;

    xorTrie *children[2] = {};

    void add(int val) {
        xorTrie *node = this;
        for (int i = L - 1; i >= 0; --i) {
            int bit = (val >> i) & 1;
            if (node->children[bit] == nullptr) {
                node->children[bit] = new xorTrie();
            }
            node = node->children[bit];
        }
    }

    int get(int val) {
        int ans = 0;
        xorTrie *node = this;
        for (int i = L - 1; i >= 0; --i) {
            int bit = (val >> i) & 1;
            if (node->children[bit ^ 1] != nullptr) {
                ans |= 1 << i;
                bit ^= 1;
            }
            node = node->children[bit];
        }
        return ans;
    }
};
//void solve(){
//    xorTrie* root=new xorTrie();
//    root->add(x);
//    root->get(x);
//}

// (2)带删除的01字典树
// 找出强数对的最大异或值 II https://leetcode.cn/problems/maximum-strong-pair-xor-ii/description/
// 静态数组写法
struct Solution2 {
public:
    int tr[30*N][2];
    int pre[30*N];
    int cnt[30*N];
    int tot;//代表
    // 总结点数
    void change(int x,int add)//插入和删除操作
    {
        int p=0;
        for(int i=21;i>=0; i--)
        {
            int id=x>>i&1;
            if(!tr[p][id])
                tr[p][id]=++tot;
            p=tr[p][id];
            pre[p]+=add;
        }
        cnt[p]=x;
    }
    int find_num(int x)
    {
        int p=0;
        for(int i=21;i>=0; i--)
        {
            int id=x>>i&1;
            if(pre[tr[p][id^1]]>0)//贪心思路
                p=tr[p][id^1];
            else
                p=tr[p][id];
        }
        return cnt[p];
    }
    int maximumStrongPairXor(vector<int>& nums) {
        int n=nums.size();
        int res=0;
        sort(nums.begin(),nums.end());
        for(int i=0,l=0;i<n;i++){
            int x=nums[i];
            while(x>2*nums[l]){
                change(nums[l],-1);
                l+=1;
            }
            int y=find_num(x);
            if(y!=0) res=max(res,x^y);
            change(nums[i],1);
        }
        return res;
    }
};


