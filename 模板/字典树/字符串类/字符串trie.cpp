#include<bits/stdc++.h>

using namespace std;

const int N = 100010;//长度根据问题决定，长度=n*max(字符串长度)
using i64 = long long;

// 1.模板一 ： 基本版本（静态数组实现）
struct Solution1 {
    int ch[N][26]; // 容器
    int idx; //步数计数器
    int cnt[N]; // s的插入次数
    i64 pre[N]; // s[0:i]的插入次数
    void insert(string s) { // 插入字符串s,统计s的插入次数
        int p = 0;
        int n = s.length();
        for (int i = 0; i < n; i++) {
            int j = s[i] - 'a';
            if (!ch[p][j]) ch[p][j] = ++idx;
            p = ch[p][j];
            pre[p]++;//前缀子串的插入次数
        }
        cnt[p]++;//字符串的插入次数
    }

    int query(string s) { // 查看有多少个字符串跟s相同
        int p = 0;
        int n = s.length();
        for (int i = 0; i < n; i++) {
            int j = s[i] - 'a';
            if (!ch[p][j]) return 0;
            p = ch[p][j];
        }
        return cnt[p];
    }

    int query1(string s) { //查看有多少个字符串与s[0:i]相同
        int p = 0;
        int n = s.length();
        i64 res = 0;
        for (int i = 0; i < n; i++) {
            int j = s[i] - 'a';
            if (!ch[p][j]) return res;
            p = ch[p][j];
            res += cnt[p];
        }
        return res;
    }

    i64 query3(string s) { //查询s的前缀子串s[0:i]的插入次数
        int p = 0;
        int n = s.length();
        i64 res = 0;
        for (int i = 0; i < n; i++) {
            int j = s[i] - 'a';
            if (!ch[p][j]) return res;
            p = ch[p][j];
            res += pre[p];
        }
        return res;
    }
};

// 2.模板二：可删除的Trie树（静态数组实现）
struct Solution2 {
    int ch[N][26]; // 容器
    int idx; //步数计数器
    int cnt[N]; // s的插入次数
    i64 pre[N]; // s[0:i]的插入次数
    void insert(string s, int val) { //插入字符串s，统计s的前缀子串s[0:i]插入次数
        int p = 0;
        int n = s.length();
        for (int i = 0; i < n; i++) {
            int j = s[i] - 'a';
            if (!ch[p][j]) ch[p][j] = ++idx;
            p = ch[p][j];
            pre[p] += val; //前缀子串的插入次数
        }
        cnt[p] += val;
    }

    int query(string s) { // 查看有多少个字符串跟s相同
        int p = 0;
        int n = s.length();
        for (int i = 0; i < n; i++) {
            int j = s[i] - 'a';
            if (pre[ch[p][j]] <= 0) return 0;
            p = ch[p][j];
        }
        return cnt[p];
    }

    i64 query1(string s) { //查询s的前缀子串s[0:i]的插入次数
        int p = 0;
        int n = s.length();
        i64 res = 0;
        for (int i = 0; i < n; i++) {
            int j = s[i] - 'a';
            p = ch[p][j];
            if (pre[p] == 0) return res;
            res += pre[p];
        }
        return res;
    }

    int query2(string s) { //查看有多少个字符串与s[0:i]相同
        int p = 0;
        int n = s.length();
        int res = 0;
        for (int i = 0; i < n; i++) {
            int j = s[i] - 'a';
            if (pre[ch[p][j]] <= 0) return res;
            p = ch[p][j];
            res += cnt[p];
        }
        return res;
    }
};
