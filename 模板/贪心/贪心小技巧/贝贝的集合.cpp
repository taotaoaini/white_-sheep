// 贝贝的集合 https://www.lanqiao.cn/problems/17159/learning/?contest_id=179

// 这题保证集合的大小至多为2
// 因为2的某个次幂比所有比他小的次幂之和还大这个性质（很好证明）,完全可以通过最大的数将前面的数进位到只剩下一个数

// 在什么情况下集合的大小为1
// 发现
// 操作一:
// 选择x,y(x==y)将2x插入到集合中，相当于二进制加法：2^i+2^i = 2^{i+1}
// 操作二：
// 选择x,y(x<y)将2x插入到集合中，2^y-2^x插入到集合中
// 集合中的数的总和没有发生改变(重点)
// 所以只需要判断集合中的数的总和是否为2^k就可以了(怎么判断）
// 每个位置进行操作一进位后，如果出现两个以上的位置为1，就说明总和不会是2^k,则答案为2
// 其实就是出现两个以上的位置为奇数，答案为2
#include<bits/stdc++.h>

using namespace std;
int n;
priority_queue<int, vector<int>, greater<int>> q;

int find() {
    while (q.size() >= 2) {
        int x = q.top();
        q.pop();
        int y = q.top();
        q.pop();
        if (x != y) return 2;
        q.push(x + 1);
    }
    return 1;
}

void solve() {
    cin >> n;
    for (int i = 1, x; i <= n; i++) {
        cin >> x;
        q.push(x);
    }
    int res = find();
    cout << res << endl;
}
int main(){
    solve();
    return 0;
}