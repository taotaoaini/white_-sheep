#include<bits/stdc++.h>
using namespace std;

// 1.到了一段连续的区间且区间长度不小于m且平均数大于等于平均数k,有：YES，没有：NO
// 处理：对于一段序列，每个数减去我们所算的平均数，如果大于0 那么他本身就大于平均数，如果小于0 那么它本身就小于平均数
// 通过前缀和和哈希表的方法解决问题（m没有限制也没有问题）
// 存在性：解决
// 个数：树状数组二分是可以解决此问题，但需要偏移。
struct Solution1{
        void sovle1(){
            int n,m;
            cin>>n>>m;
            vector<int> a(n+1,0);
            for(int i=1;i<=n;i++) cin>>a[i];
            auto check=[&](int md){ //存在性问题
                vector<int> sum(n+1,0);
                for(int i=1;i<=n;i++){
                    sum[i]=sum[i-1]+(a[i]-md); //做差
                }
                int mi=0;
                for(int i=0,j=m;j<=n;j++,i++){ //双指针可以快速判断>=m的区间的情况
                    mi=min(mi,sum[i]);
                    if(sum[j]-mi>=0) return true;
                }
                return false;
            };
//            bool res=check(10);
        }
};


int main(){

}