#include<bits/stdc++.h>

using namespace std;

//1.vector离散化
void solve() {
    int n;
    cin >> n;
    vector<int> a(n + 1);
    vector<int> b(n + 1);
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
        b[i] = a[i];
    }
    sort(b.begin() + 1, b.end());
    b.erase(unique(b.begin() + 1, b.end()), b.end());//去除重复值，unique返回重复元素的首地址
    //获取id： int id=lower_bound(b.begin()+1,b.end(),a[pos])-b.begin();
}

// 2.int离散化
const int N = 100010;
int a[N], b[N];
int n; // 原数组的长度
void solve1() {
    cin >> n;
    for (int i = 1; i <= n; i++) cin >> a[i], b[i] = a[i];
    sort(b + 1, b + n + 1);
    int bn = unique(b + 1, b + n + 1) - b - 1;
    for (int i = 1; i <= n; i++) a[i] = lower_bound(b + 1, b + n + 1, a[i]) - b;
}

