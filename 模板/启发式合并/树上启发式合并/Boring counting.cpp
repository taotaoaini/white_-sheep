// Boring counting https://acm.hdu.edu.cn/showproblem.php?pid=4358
// https://www.cnblogs.com/kikokiko/p/12774456.html

#include<bits/stdc++.h>

using i64 = signed long long;
using namespace std;

const int N = 100010;

int n, k;
vector<int> a;
vector<int> b;
vector<int> id;
int ans[N];
int cnt[N];
int app[N];
int son[N], sz[N];
vector<int> e[N];

void init() {
    a.clear();
    b.clear();
    id.clear();
    for (int i = 1; i <= n; i++) {
        ans[i] = 0;
        son[i] = 0;
        sz[i] = 0;
        app[i] = 0;
        cnt[i] = 0;
        e[i].clear();
    }
}

void inc(int x) {
    cnt[x]++;
    app[cnt[x]]++;
    app[cnt[x] - 1]--;
}

void dec(int x) {
    cnt[x]--;
    app[cnt[x]]++;
    app[cnt[x] + 1]--;
}

void dfs(int u, int fa) {
    sz[u] = 1;
    for (auto v: e[u]) {
        if (v == fa) continue;
        dfs(v, u);
        if (sz[v] > sz[son[u]]) son[u] = v;
        sz[u] += sz[v];
    }
}

void update(int u, int fa, bool add) {
    if (add) inc(id[u]);
    else dec(id[u]);
    for (int v: e[u]) { if (v != fa) update(v, u, add); }
}

void query(int u, int fa, bool cls) {
    for (auto v: e[u]) {
        if (v == fa || v == son[u]) continue;
        query(v, u, true);
    }
    if (son[u]) query(son[u], u, false);
    for (auto v: e[u]) {
        if (v == fa || v == son[u]) continue;
        update(v, u, true);
    }
    inc(id[u]);
    ans[u] = app[k];
    if (cls) update(u, fa, false);
}

void solve() {
    cin >> n >> k;
    a.assign(n + 1, 0);
    id.assign(n + 1, 0);
    for (int i = 1; i <= n; i++) cin >> a[i];
    b = a;
    sort(b.begin() + 1, b.end());
    b.erase(unique(b.begin() + 1, b.end()), b.end());
    for (int i = 1; i <= n; i++) id[i] = lower_bound(b.begin() + 1, b.end(), a[i]) - b.begin();
    for (int i = 1; i <= n - 1; i++) {
        int x, y;
        cin >> x >> y;
        e[x].push_back(y);
        e[y].push_back(x);
    }
    dfs(1, 0);
    query(1, 0, 0);
    int q;
    cin >> q;
    while (q--) {
        int x;
        cin >> x;
        cout << ans[x] << endl;
    }
    init();
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    //init();//全局初始化

    int t = 1;
    cin >> t;

    for (int i = 1; i <= t; i++) {
        cout << "Case #" << i << ":" << endl;
        solve();
    }
    return 0;
}