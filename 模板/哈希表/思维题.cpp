#include<bits/stdc++.h>
using namespace std;

// 枚举右，维护左：需要维护两种值（pair）
// 统计美丽子字符串 II https://leetcode.cn/problems/count-beautiful-substrings-ii/description/
// 1)区间满足x的出现次数和y的出现次数相等，x:+1,y:-1,维护区间值为0的数=>哈希表
// 区间长度 (len/2) % k ==0
// 2) len为偶数，y1=(x/2)^2%k==0 => y2=x^2 mod 4k == 0 ==> 求的余数成倍数关系 (y2的余数)r2=4r1
// 3)L的平方能把n整除，平方很难维护，如果不是平方，直接就可以通过哈希表维护
// n为一个质数：L必须包含质因子 n，L是n的倍数
// n为一个质数p的e次幂：
// ..e为偶数, L必须是p^(e/2)的倍数
// ..e为奇数，L必须是p^(e/2+1)的倍数
//==>(e+1)/2：向上取整
//如果n可以分解出多个质因子，只需要把每个质因子及其幂次按照上面的方法处理，把结果相乘，就得到 L必须是什么数的倍数了。
// 把平法除去了
// k'=sqrt(k),质因子处理
// [j,i),L=i-j. (i-j)mod k' = 0 => i mod k' =j mod k'
//  哈希表存双值: pair(i mod k',s[i])
struct Solution1 {
    string d="aeiou";
    int init(int n){
        int res=1;
        for(int i=2;i*i<=n;i++){
            int i2=i*i;
            while(n%i2==0){ //处理偶数次
                res*=i;
                n/=i2;
            }
            if(n%i==0){ //奇数次的时候会存在
                res*=i;
                n/=i;
            }
        }
        if(n>1){
            res*=n;
        }
        return res;
    }
    long long beautifulSubstrings(string s, int k) {
        //L^2%4k==0
        // sqrt(4k)的质因子乘积的向上取整，p0^x0+p1^x1;奇数次幂向上整除2，（x+1）/2，==L=sqrt(4k)
        // 把 pair 压缩成 long long（或者 int）就可以用 umap 了
        k=init(4*k);
        map<pair<int,int>,int> cnt;
        cnt[{k-1,0}]++;
        long long res=0;
        int sum=0;
        for(int i=0;i<s.length();i++){
            int flag=(d.find(s[i])!=-1); // 查看是否为元音
            sum+=flag*2-1; // 1->1  0->-1
            res+=cnt[{i%k,sum}]++;
        }
        return res;
    }
};