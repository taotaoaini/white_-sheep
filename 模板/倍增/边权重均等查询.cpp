#include <bits/stdc++.h>

using namespace std;

// （1）LCA模板+统计路径信息
// 边权重均等查询 https://leetcode.cn/problems/minimum-edge-weight-equilibrium-queries-in-a-tree/description/

vector<int> minOperationsQueries(int n, vector<vector<int>> &edges, vector<vector<int>> &queries) {
    vector<vector<pair<int, int>>> e(n + 10);
    for (auto &t: edges) {
        int x = t[0] + 1, y = t[1] + 1, w = t[2] - 1; // 节点下标从1开始
        e[x].emplace_back(y, w);
        e[y].emplace_back(x, w);
    }

    int m = 20;
    vector<vector<int>> pa(n + 10, vector<int>(m));
    vector<vector<array<int, 26>>> cnt(n + 1, vector<array<int, 26>>(m)); // 表示当前节点向上跳2^i的信息
    vector<int> depth(n + 1);
    function<void(int, int)> dfs = [&](int x, int fa) {
        pa[x][0] = fa;
        for (int i = 1; i < m; i++) {
            for (int j = 0; j < 26; ++j) {
                cnt[x][i][j] = cnt[x][i - 1][j] + cnt[pa[x][i - 1]][i - 1][j];
            }
            pa[x][i] = pa[pa[x][i - 1]][i - 1];
        }
        for (auto [y, w]: e[x]) {
            if (y != fa) {
                cnt[y][0][w] = 1;
                depth[y] = depth[x] + 1;
                dfs(y, x);
            }
        }
    };
    dfs(1, 0);
    vector<int> ans;
    for (auto &q: queries) {
        int x = q[0] + 1, y = q[1] + 1;
        int path_len = depth[x] + depth[y]; // 最后减去 depth[lca] * 2
        int cw[26]{};
        if (depth[x] > depth[y]) {
            swap(x, y);
        }
        for (int k = m - 1; k >= 0; k--) { // 让 y 和 x 在同一深度，同时记录信息
            int p = pa[y][k];
            if (depth[p] < depth[x]) continue;
            for (int j = 0; j < 26; ++j) {
                cw[j] += cnt[y][k][j];
            }
            y = p;
        }

        if (y != x) {
            for (int i = m - 1; i >= 0; i--) {
                int px = pa[x][i], py = pa[y][i];
                if (px != py) {
                    for (int j = 0; j < 26; j++) {
                        cw[j] += cnt[x][i][j] + cnt[y][i][j];
                    }
                    x = px;
                    y = py; // x 和 y 同时上跳 2^i 步
                }
            }
            for (int j = 0; j < 26; j++) {
                cw[j] += cnt[x][0][j] + cnt[y][0][j];
            }
            x = pa[x][0];
        }

        int lca = x;
        path_len -= depth[lca] * 2;
        int mx = 0;
        for (int i = 0; i < 26; i++) mx = max(mx, cw[i]);
        ans.push_back(path_len - mx);
    }
    return ans;
}//
// Created by 35231 on 2024/5/31.
//
