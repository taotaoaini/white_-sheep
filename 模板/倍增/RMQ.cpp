#include<bits/stdc++.h>
using namespace std;

// (1)结合律且可重复贡献的信息查询都可以使用ST表，但不通过修改
// 最大值，最小值，最大公因数，最小公倍数，按位或，按位与
// RMQ
// ST表（稀疏表）
// 倍增思想: 用两个等长小区间拼凑一个大区间
// f[i][j]:以第i个数为启起点，长度2^j的区间的最大值
// f[i][j]=max(f[i][j-1],f[i+2^{j-1}][j-1])
// i+2^j-1<=n
//  处理查询
// 对[L,R]进行分割
// k=log2(R-L+1)
// 2^k<=R-L+1<2*2^k
//区间[L,R]必可以通过两个长度为2^k的区间重叠拼凑
struct Solution1{
    void solve1(){
        int n,m;
        cin>>n>>m;
        vector<array<int,22>> f(n+1); //第二位根据n大小情况而定,//2^{20}>=1e6
        vector<int> a(n+1);
        for(int i=1;i<=n;i++) cin>>a[i];
        for(int i=1;i<=n;i++) f[i][0]=a[i];
        for(int j=1;j<=20;j++){
            for(int i=1;i+(1<<j)-1<=n;i++){
                f[i][j]=max(f[i][j-1],f[i+(1<<(j-1))][j-1]); // [i:i+2^(j-1)-1] [i+(j-1):j]
            }
        }
        auto get=[&](int L,int R)->int{
            int k=log2(R-L+1);
            return max(f[L][k],f[R-(1<<k)+1][k]); //r前(1<<k)-1个
        };
        for(int i=1,L,R;i<=m;i++){
            cin>>L>>R;
            cout<<get(L,R)<<endl;
        }
    }
};
