// 有效的数独 https://leetcode.cn/problems/valid-sudoku/description/

// 数字 1-9 在每一行只能出现一次。
// 数字 1-9 在每一列只能出现一次。
// 数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次.


#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        int a[10][10]{0};
        int b[10][10]{0};
        int c[5][5][10]{0};
        int n=board.size();
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(board[i][j]=='.') continue;
                int x=board[i][j]-'0';
                if(a[i][x]!=0||b[j][x]!=0||c[i/3][j/3][x]!=0) return false;
                a[i][x]+=1;
                b[j][x]+=1;
                c[i/3][j/3][x]+=1;
            }
        }
        return true;
    }
};