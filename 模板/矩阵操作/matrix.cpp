#include<bits/stdc++.h>
using namespace std;
// 1.矩阵顺时针旋转90度
class Solution1{
    void rotate(vector<vector<int>>& matrix) { // 辅助数组旋转
        int n = matrix.size();
        auto res = matrix;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < matrix[i].size(); j++) {
                res[j][n - i - 1] = matrix[i][j];
            }
        }
        matrix = res;
    }
};

// 2.两个矩阵的交集区域的正方形的面积
struct Solution2 {
public:
    using i64=long long;
    long long largestSquareArea(vector<vector<int>>& a, vector<vector<int>>& b) {// a:存放矩阵的左下坐标，b：存放矩阵的右上坐标 ，共n=1e3个矩阵
        int n=a.size();
        auto query=[&](int i,int j){
            int x=min(b[i][0],b[j][0])-max(a[i][0],a[j][0]);
            int y=min(b[i][1],b[j][1])-max(a[i][1],a[j][1]);
            x=min(x,y);
            x=max(x,0);
            return 1LL*x*x;
        };
        i64 res=0;
        for(int i=0;i<n;i++){
            for(int j=i+1;j<n;j++){
                res=max(res,query(i,j));
            }
        }
        return res;
    }
};

