#include<bits/stdc++.h>

using namespace std;
using i64 = long long;

// 1.二维前缀和 sum[i][j]([1:n][1:m])  a[i][j]([0:n-1][0:m-1])
// sum[i][j]=sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1]+a[i-1][j-1];
struct Solution1 {
    void solve1() {
        int n = 100, m = 100;
        vector<vector<int>> a(n, vector<int>(m));
        vector<vector<i64>> sum(n + 1, vector<i64>(m + 1));
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                sum[i][j] = sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1] + a[i - 1][j - 1];
            }
        }
        auto get = [&](int x1, int y1, int x2, int y2) -> i64 { //子矩阵和
            return sum[x2][y2] - sum[x1 - 1][y2] - sum[x2][y1 - 1] + sum[x1 - 1][y1 - 1];
        };
//        i64 res=get(1,1,2,2);
    }
};