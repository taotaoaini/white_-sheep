// 难 ： https://codeforces.com/problemset/problem/691/E
// 看懂了

// 理解方式一：
//  抽象成图，找出符合条件长度为k的路径。
// dp[i][j]:表示a[i]到a[j]的合法路径数
// dp[i][j]=sum_k(dp[i][k]*dp[k][j])
// 每计算一次就相当于添加了一个端点（一条边）,因此只需要k-1次操作就行了
// 维护一个n*n的矩阵，当a[i]^a[j]==3k,则m[i][j]=1,或者为0

#include<iostream>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#include<vector>
#include<queue>
#include<sstream>
#include<cmath>
#include<list>
#include<bitset>
#include<unordered_map>
#include<unordered_set>
#include<stack>
#include<iomanip>//设置浮点数精度问题
#include<functional>//编写内部函数

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

int sz;
struct mat {
    i64 a[115][115];
    inline mat() { memset(a, 0, sizeof a); }

    inline mat operator-(const mat& T) const {
        mat res;
        for (int i = 1; i <= sz; ++i)
            for (int j = 1; j <= sz; ++j) {
                res.a[i][j] = (a[i][j] - T.a[i][j]) % mod;
            }
        return res;
    }

    inline mat operator+(const mat& T) const {
        mat res;
        for (int i = 1; i <= sz; ++i)
            for (int j = 1; j <= sz; ++j) {
                res.a[i][j] = (a[i][j] + T.a[i][j]) % mod;
            }
        return res;
    }

    inline mat operator*(const mat& T) const {
        mat res;
        i64 r;
        for (int i = 1; i <= sz; ++i)
            for (int k = 1; k <= sz; ++k) {
                r = a[i][k];
                for (int j = 1; j <= sz; ++j)
                    res.a[i][j] = (res.a[i][j] + T.a[k][j] * r) % mod;
                // res.a[i][j] += 1LL*T.a[k][j] * r, res.a[i][j] %= mod;
            }
        return res;
    }

    inline mat operator^(i64 x) const {
        mat res, bas;
        for (int i = 1; i <= sz; ++i) res.a[i][i] = 1;
        for (int i = 1; i <= sz; ++i)
            for (int j = 1; j <= sz; ++j) bas.a[i][j] = a[i][j] % mod;
        while (x) {
            if (x & 1) res = res * bas;
            bas = bas * bas;
            x >>= 1;
        }
        return res;
    }
};
const double eps = 1e-8;
void solve() {
    int n;
    i64 k;
    cin >> n >> k;
    vector<i64> a(n + 1);
    for (int i = 1; i <= n; i ++) cin >> a[i];
    mat C;
    sz = n;
    for (int i = 1; i <= n; i ++) {
        for (int j = 1; j <= n; j ++) {
            i64 x = a[i] ^ a[j];
            int cnt = 0;
            while (x) {
                if (x & 1) cnt++;
                x >>= 1;
            }
            if (cnt % 3 == 0) C.a[i][j] = 1;
        }
    }
    C = C.operator ^ (k - 1);
    i64 ans = 0;
    mat Ori;
    for (int i = 1; i <= n; i ++) Ori.a[1][i] = 1;
    for (int i = 1; i <= n; i ++) {
        for (int j = 1; j <= n; j ++) {
            ans = (ans + Ori.a[1][j] * C.a[j][i] % mod) % mod;
        }
    }
    cout << ans << endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}