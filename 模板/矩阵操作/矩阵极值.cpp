#include<bits/stdc++.h>
using namespace std;

// https://www.lanqiao.cn/problems/17002/learning/?contest_id=174
// （1）在矩阵中求固定n‘*m’的子矩阵的最大值和最小值
// 单调队列处理，先处理列（把每一列当做一维去做，预处理出（n-（n‘-1））个子数组的极值，再处理行，就达到了O(1)求出一个子矩阵的最大值和最小值了

//最小值
struct Solution1{
    void solve1(){
        int n, m, nn, mm;
        cin >> n >> m >> nn >> mm;
        vector<vector<int>> a(n, vector<int>(m));
        vector<vector<int>> b(n, vector<int>(m));
        vector<vector<int>> bb(n, vector<int>(m));
        for (int i = 0; i < m; i++) { //最小值
            deque<int> q;
            for (int j = 0; j < nn; j++) {
                while (q.size() && a[q.back()][i] >= a[j][i]) q.pop_back();
                q.push_back(j);
            }
            b[nn - 1][i] = a[q.front()][i];
            for (int j = nn; j < n; j++) {
                while (q.size() && a[q.back()][i] >= a[j][i]) q.pop_back();
                while (q.size() && q.front() < j - nn + 1) q.pop_front();
                q.push_back(j);
                b[j][i] = a[q.front()][i];
            }
        }
        for (int i = nn - 1; i < n; i++) { //对列操作，最小值
            deque<int> q;
            for (int j = 0; j < mm; j++) {
                while (q.size() && b[i][q.back()] >= b[i][j]) q.pop_back();
                q.push_back(j);
            }
            bb[i][mm - 1] = b[i][q.front()];
            for (int j = mm; j < m; j++) {
                while (q.size() && b[i][q.back()] >= b[i][j]) q.pop_back();
                while (q.size() && q.front() < j - mm + 1) q.pop_front();
                q.push_back(j);
                bb[i][j] = b[i][q.front()];
            }
        }
        for (int i = nn - 1; i < n; i++) {
            for (int j = mm - 1; j < m; j++) {
               cout<<bb[i][j]<<endl;
            }
        }
    }
};

//最大值
struct Solution2{
    void solve2(){
        int n, m, nn, mm;
        cin >> n >> m >> nn >> mm;
        vector<vector<int>> a(n, vector<int>(m));
        vector<vector<int>> c(n, vector<int>(m));
        vector<vector<int>> cc(n, vector<int>(m));
        for (int i = 0; i < m; i++) { //最大值
            deque<int> q;
            for (int j = 0; j < nn; j++) {
                while (q.size() && a[q.back()][i] <= a[j][i]) q.pop_back();
                q.push_back(j);
            }
            c[nn - 1][i] = a[q.front()][i];
            for (int j = nn; j < n; j++) {
                while (q.size() && a[q.back()][i] <= a[j][i]) q.pop_back();
                while (q.size() && q.front() < j - nn + 1) q.pop_front();
                q.push_back(j);
                c[j][i] = a[q.front()][i];
            }
        }
        for (int i = nn - 1; i < n; i++) {//对列操作，最大值
            deque<int> q;
            for (int j = 0; j < mm; j++) {
                while (q.size() && c[i][q.back()] <= c[i][j]) q.pop_back();
                q.push_back(j);
            }
            cc[i][mm - 1] = c[i][q.front()];
            for (int j = mm; j < m; j++) {
                while (q.size() && c[i][q.back()] <= c[i][j]) q.pop_back();
                while (q.size() && q.front() < j - mm + 1) q.pop_front();
                q.push_back(j);
                cc[i][j] = c[i][q.front()];
            }
        }
        for (int i = nn - 1; i < n; i++) {
            for (int j = mm - 1; j < m; j++) {
                cout<<cc[i][j]<<" ";
            }
            cout<<endl;
        }
    }
};
