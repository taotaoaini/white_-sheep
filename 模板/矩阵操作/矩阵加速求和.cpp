#include <bits/stdc++.h>

using namespace std;
using i64 = long long;
const int mod=1e9+7;
// 结构体存放矩阵
//
//矩阵维度长
#define  MATN 3
struct matrix {
    i64 c[MATN+1][MATN+1]; //矩阵从下标1开始

    matrix() { memset(c, 0, sizeof(c)); }
};

matrix operator*(matrix &x, matrix &y) {
    matrix res;
    for (int i = 1; i <= MATN; i++) {
        for (int j = 1; j <= MATN; j++) {
            for (int k = 1; k <= MATN; k++) {
             res.c[i][j]=(res.c[i][j]+x.c[i][k]*y.c[k][j])%mod;
            }
        }
    }
}
matrix qmi(matrix x,i64 k){ //x^k
    matrix res;
    res.c[1][1]=1;
    res.c[2][2]=1;
    res.c[3][3]=1; //单位矩阵
    while(k){
        if(k&1) res=res*x;
        x=x*x;
        k>>=1;
    }
    return res;
}
int main() {
    matrix x;
    x.c[1][1]=x.c[2][1]=x.c[2][2]=x.c[2][3]=x.c[3][2]=1;
    matrix res=qmi(x,10);
    for(int i=1;i<=3;i++){
        for(int j=1;j<=3;j++){
            cout<<x.c[i][j]<<" ";
        }
        cout<<endl;
    }
}