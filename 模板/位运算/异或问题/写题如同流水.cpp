#include<bits/stdc++.h>
using namespace std;

// (1)数组中两个数的最大异或值 https://leetcode.cn/problems/maximum-xor-of-two-numbers-in-an-array/description/
// 1<=n<=2e5
// 0<=a[i]<=2e^{31}-1
// 秒懂题解：https://leetcode.cn/problems/maximum-xor-of-two-numbers-in-an-array/solutions/2511644/tu-jie-jian-ji-gao-xiao-yi-tu-miao-dong-1427d/
//  不需要01trie就可以做，很帮的思路:从最高位开始贪心，判断这个位置是否可以为1
struct Solution1{
    int findMaxXor(vector<int>& nums) {
        int res=0;
        for(int i=31;i>=0;i--){
            unordered_set<int> d;
            res|=(1<<i);
            bool flag=false;
            for(auto x:nums){
                x&=res;  //将i+1往后的1置0,以免影响查找
                if(d.count(x^res)>=1){
                    flag=true;
                }
                d.insert(x);
            }
            if(!flag) res^=(1<<i);
        }
        return res;
    }
};

