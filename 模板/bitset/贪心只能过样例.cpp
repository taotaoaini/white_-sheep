// 贪心只能过样例 https://loj.ac/p/515
#include<bits/stdc++.h>
using namespace std;
const int N = 110;

// 01背包+bitset
// f[i][j] ：前i个数的平方和能否为j
bitset<N*N*N> f[N]; //二进制中1表示可以，0表示不可以
int n,a[N],b[N];
void solve() {
    cin>>n;
    for(int i=1;i<=n;i++) cin>>a[i]>>b[i];
    f[0][0]=1;
    for(int i=1;i<=n;i++){
        for(int j=a[i];j<=b[i];j++){
            f[i]|=(f[i-1]<<(j*j)); //左移:消除f[i-1][s-j*j+1:s]的影响，直接左移就可以
        }
    }
    cout<<f[n].count()<<endl;
}
