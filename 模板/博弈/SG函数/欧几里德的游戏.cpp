 // 欧几里德的游戏  https://www.luogu.com.cn/problem/P1290

// 必胜点，它所能转移的状态中有一必败点；
// 必败点，它所能转移的状态中都为必胜点；
// 无中间态；
// 可分为三种情况 m为较大数 n为较小数
// 当m%n==0时：必胜态
// 当m/n等于1时 只有一种取法，需要接着往下一状态转移进行判断(m,n)->(n,m%n)
// 当m/n>=2时 谁取则谁胜:
// 1）因为当(n,m%n)为必败态时，取者可将其取为（m%n+n,n），则必败态转移给另一人
// 2）如果（m%n+n,n）为必败态，取者可取为(n,m%n)
// 此模型为先手占优
#include<bits/stdc++.h>
using namespace std;
int main(){
    int n,m;
    while(cin>>n>>m){
        if(n==0&&m==0) break;
        int k=1;
        while(1)
        {
            if(n<m)swap(n,m);
            if(n%m==0||n-m>m)break;
            n-=m;k=!k;
        }
        if(k)printf("xiaoming wins\n");
        else
            printf("xiaohong wins\n");
    }
    return 0;
}
