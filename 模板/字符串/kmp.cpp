#include<bits/stdc++.h>

using namespace std;
const int N = 1000010;

//1.kmp算法
//时间复杂度：O(n+m)
//ne数组定义：最长公共前后缀 abab ne[3]=1 ne[4]=2
struct Solution1 {
    int m, n;//m:len(s),n=len(p)
    string s, p;// s：匹配串，p:模式串
    int ne[N];// 回跳数组
    void init() { // 构造ne数组
        ne[1] = 0;
        for (int i = 2, j = 0; i <= n; i++) {
            while (j && p[i] != p[j + 1]) j = ne[j]; // j回跳
            if (p[i] == p[j + 1]) j++;
            ne[i] = j;
        }
    }

    int find() { // 匹配
        for (int i = 1, j = 0; i <= m; i++) {
            while (j && s[i] != p[j + 1]) j = ne[j];
            if (s[i] == p[j + 1]) j++;
            if (j == n) return i - n + 1;
        }
        return -1;
    }

    void solve1(string s, string p) {//字符串从下标0开始,s:匹配串，p：模式串(p在s上进行匹配,找到出现的起始位置)
        m = s.length(), n = p.length();
        this->s = " " + s;
        this->p = " " + p;
        init();//初始化ne数组
    }

    void print() { //输出ne数组
        for (int i = 1; i <= n; i++) {
            cout << ne[i] << " ";
        }
        cout << endl;
    }
};
//int main(){
//    Solution1* sol=new Solution1();
//    sol->solve1("abcd","e");
//    sol->print();
//    cout<<sol->get()<<endl;
//}
// s:abacabab
// p:abab

//2.扩展kmp（z函数）
//时间复杂度：O(n)
// s公共最长后缀匹配s[i:n],使用加速盒子思想,盒内加速，盒外暴力
struct Solution2 {
    string t, s;
    int z[N], p[N];//

    void get_z(string &s, int n) {
        z[1] = n;
        for (int i = 2, l, r = 0; i <= n; i++) {
            if (i <= r)z[i] = min(z[i - l + 1], r - i + 1);
            while (s[1 + z[i]] == s[i + z[i]])z[i]++;
            if (i + z[i] - 1 > r)l = i, r = i + z[i] - 1;
            // printf("i=%d z=%d [%d %d]\n",i,z[i],l,r);
        }
    }

    void get_p(string &s, int n, string &t, int m) {
        for (int i = 1, l, r = 0; i <= m; i++) {
            if (i <= r)p[i] = min(z[i - l + 1], r - i + 1);
            while (1 + p[i] <= n && i + p[i] <= m
                   && s[1 + p[i]] == t[i + p[i]])
                p[i]++;
            if (i + p[i] - 1 > r)l = i, r = i + p[i] - 1;
        }
    }

    void solve2() {
        cin >> t;
        cin >> s;
        int m = t.length(), n = s.length();
        s = " " + s;
        t = " " + t;
        get_z(s, n);
        get_p(s, n, t, m);
    }
};

