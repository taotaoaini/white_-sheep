#include<bits/stdc++.h>

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i64 = long long;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

//int[]:6.7e7
const int inf = 0x3f3f3f3f;//1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f;//4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;
const int P = 131;

// 1.最长回文子串
// 马拉车算法
struct Solution1 {
    int get(string s) {
        vector<char> a;
        a.push_back('$');
        a.push_back('#');
        int n = 1;
        for (int i = 0; i < s.length(); i++) {
            a.push_back(s[i]);
            a.push_back('#');
            n += 2;
        }
        a.push_back('&');
        vector<int> d(n + 10, 0);
        for (int i = 1, l = 0, r = 0; i <= n; i++) {
            if (i <= r) d[i] = min(r - i + 1, d[r - i + l]);
            while (a[i - d[i]] == a[i + d[i]]) d[i] += 1;
            if (i + d[i] - 1 > r) l = i - d[i] + 1, r = i + d[i] - 1;
        }
        int res = 0;
        for (int i = 1; i <= n; i++) res = max(res, d[i]);
        return res - 1;
    }
};