#include <bits/stdc++.h>
using namespace std;
//预头
using i64 = long long;
//end--

// 1.判断[L,R]是以[L,L+len-1]作为循环节
struct Solution1{
    //同余技巧，可以(x-1)%p+1，这样就可以确保(p)%p=p
    bool check(string s,int L,int R,int len)
    {//周期：(n-L)%len+L-1+1 :12312: (5-1)%3+1-1+1
        for(int i=L;i<=R;++i)
            if(s[i]!=s[(i-L)%len+L]) return false;//偏移量
        return true;
    }
};
//end--

// 2.[1,n]中字典序第k小数字 1<=k,n<=1e9
// 时间复杂度：O(logn*logn)
//思想：按照字典树的特性将<=n的数字按字典序的方式重构
// 前序遍历字典树,但不需要构出字典树，因为子树大小已知，只需要判断是否在当前的子树下
class Solution2 {
public:
    i64 gets(i64 pre,i64 n){
        i64 res=0;
        for(i64 a=pre,b=pre+1;a<=n;a*=10,b*=10){
            res+=min(b,n+1)-a; //可以通过兄弟节点算出a子树大小
            // (b-1)-a+1:表示a子树下（包括a）的子树大小,
            //n-a+1:表示a子树下不完整
        }
        return res;
    }
    int findKthNumber(int n, int k) {
        i64 x=1;
        i64 t=1; //计数
        for(i64 t=1;t<k;){
            i64 cnt=gets(x,1LL*n);
            if(t+cnt>k){
                t+=1; //向下递归
                x*=10;
            }else{
                t+=cnt; //转移到兄弟节点x+1
                x+=1;
            }
        }
        return x;
    }
};
// end--