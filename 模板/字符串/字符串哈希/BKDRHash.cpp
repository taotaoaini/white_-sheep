#include<bits/stdc++.h>

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int,int>;
using pll = std::pair<long long,long long>;
using pci=std::pair<char,int>;
using namespace std;

//int[]:6.7e7
const int inf=0x3f3f3f3f;//1061109567,是10^9级别的
const i64 dinf=0x3f3f3f3f3f3f3f3f;//4.62*10^18级别的
const int mod=1e9+7;
const int M=100010;
const int N=100010;
const int P=131;

//1.字符串哈希
//(1)自然溢出法
//const int P=131;
struct Solution1{//需要设置P=131(大素数)
    const int P=131;
    vector<u64> p;
    vector<u64> h;
    string s;
    void init(string s){//s下标从1开始
        int n=s.length();
        p.assign(n+10,0);
        h.assign(n+10,0);
        s=" "+s;
        p[0]=1,h[0]=0;
        for(int i=1;i<=n;i++){
            p[i]=p[i-1]*P;//阶乘
            h[i]=h[i-1]*P+s[i];
        }
    }
    u64 get(int l,int r){////字符串从下标1开始
        return h[r]-h[l-1]*p[r-l+1];
    }
    bool substr(int l1,int r1,int l2,int r2){//判断s[1l:r1]和s[l2:r2]相同
        return get(l1,r1)==get(l2,r2);
    }
};