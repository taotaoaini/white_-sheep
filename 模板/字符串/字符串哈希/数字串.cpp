// 数字串 https://www.matiji.net/exam/brushquestion/7/4347/179CE77A7B772D15A8C00DD8198AAC74?from=1

#include<bits/stdc++.h>

using namespace std;
using i64 = long long;
using Hash = std::array<int, 2>;
#define mid ((l+r)/2)
const int mod = 1e9 + 7;
vector<int> a, b;

vector<int> add(vector<int> &a, vector<int> &b) {
    if (a.size() < b.size()) swap(a, b);
    vector<int> c;
    int t = 0;
    for (int i = 0; i < a.size(); i++) {
        t += a[i];
        if (i < b.size()) t += b[i];
        c.push_back(t % 10);
        t /= 10;
    }
    while (t) {
        c.push_back(t % 10);
        t /= 10;
    }
    return c;
}

int n, k;
string s;

struct Solution1 {//需要设置P=131(大素数)
    vector<i64> p;
    vector<i64> h;
    string s;

    bool isprime(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    int findPrime(int n) {
        while (!isprime(n)) {
            n++;
        }
        return n;
    }

    void init(string s) {//s下标从1开始 s=" "+s;
        std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());
        const int P = findPrime(rng() % 900000000 + 100000000);
        int n = s.length() - 1;
        p.assign(n + 10, 0);
        h.assign(n + 10, 0);
        p[0] = 1, h[0] = 0;
        for (int i = 1; i <= n; i++) {
            p[i] = (p[i - 1] * P) % mod;//阶乘
            h[i] = (h[i - 1] * P + s[i]) % mod;
        }
    }

    i64 get(int l, int r) {//字符串从下标1开始
        return (h[r] - h[l - 1] * p[r - l + 1] % mod + mod) % mod;
    }

    bool substr(int l1, int r1, int l2, int r2) {//判断s[1l:r1]和s[l2:r2]相同
        return get(l1, r1) == get(l2, r2);
    }
} tr;

int lcp(int i, int j) {
    int l = -1, r = k + 1;
    while (l + 1 < r) {
        if (tr.substr(i, i + mid - 1, j, j + mid - 1)) l = mid;
        else r = mid;
    }
    return l;
}

void solve() {
    cin >> n >> k;
    cin >> s;
    s = " " + s;
    k = n - k;
    tr.init(s);
    int idx = 1;
    for (int i = 2; i + k - 1 <= n; i++) {
        int x = lcp(idx, i);// 返回最长公共前缀
        if (x != k && s[i + x] > s[idx + x]) idx = i;
    }
    i64 x = 0;
    int i = 1;
    for (int i = 1; i < idx; i++) x += (s[i] - '0');
    for (int i = idx + k; i <= n; i++) x += (s[i] - '0');
    for (int i = idx + k - 1; i >= idx; i--) a.push_back(s[i] - '0');
    while (x) {
        b.push_back(x % 10);
        x /= 10;
    }
    vector<int> ans = add(a, b);
    for (int i = ans.size() - 1; i >= 0; i--) {
        cout << ans[i];
    }
    cout << endl;
}

int main() {
    solve();
    return 0;
}