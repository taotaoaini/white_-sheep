// 分割回文串II https://leetcode.cn/problems/palindrome-partitioning-ii/description/
#include<bits/stdc++.h>

using namespace std;
const int N = 1010;
int n;
string s; // s从下标1开始
bool f[N][N];

void init() { // 预处理出了字符串回文的判断数组
    for (int r = 1; r <= n; r++) {
        for (int l = 1; l <= r; l++) {
            if (l == r) f[l][r] = 1;
            else {
                if (s[l] == s[r]) {
                    if (r - l == 1 || f[l + 1][r - 1]) {
                        f[l][r] = 1;
                    }
                }
            }
        }
    }
}