// 不重叠回文子字符串的最大数目 https://leetcode.cn/problems/maximum-number-of-non-overlapping-palindrome-substrings/description/

#include <bits/stdc++.h>
using namespace std;
int maxPalindromes(string s, int k) {
    int n = s.length(), f[n + 1];
    // f[i] 表示s[0..i-1] 中的不重叠回文子字符串的最大数目
    // 更加优雅的方式枚举所有奇数和偶数的中心点位置
    memset(f, 0, sizeof(f));
    for (int i = 0; i < 2 * n - 1; ++i) {
        int l = i / 2, r = l + i % 2; // 中心扩展法
        f[l + 1] = max(f[l + 1], f[l]);
        for (; l >= 0 && r < n && s[l] == s[r]; --l, ++r)
            if (r - l + 1 >= k) {
                f[r + 1] = max(f[r + 1], f[l] + 1);
                break;
            }
    }
    return f[n];
}