#include<bits/stdc++.h>

using namespace std;

//1.字符串中子串个数
int get1(const std::string &str, const std::string &sub) {//子串可重叠overlap
    int num = 0;
    //str.find(sub,开始查询的起点)
    for (int i = 0; (i = str.find(sub, i)) != std::string::npos; num++, i++);
    return num;
}

int get2(const std::string &str, const std::string &sub) {//子串不可重叠
    int num = 0;
    size_t len = sub.length();
    if (len == 0)len = 1;//应付空子串调用
    for (int i = 0; (i = str.find(sub, i)) != std::string::npos; num++, i += len);
    return num;
}



