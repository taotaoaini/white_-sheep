#include<bits/stdc++.h>
using namespace std;

// 1. Z(i) 定义为 S 与 s[i...n−1] 后缀子字符串的 最长公共前缀(LCP)。下表从0开始
// 设 Z(i)≠0 ，那么我们定义区间 [i,i+Z[i]−1] 为一个Z-Box，
// 显然，Z-Box对应的子串一定是整个字符串的一个前缀。
// 我们从左往右枚举下标 i，同时维护 l 和 r ，用 [l,r] 表示满足 l≤i 且 r 最大的Z-Box。那么就可能出现三种情况：
// (1)如果 i>r，越过Z-Box,暴力更新Z[i]的值，再更新l和r
// (2)如果 i<=r, s[l:r]==s[0:r-1]，因此s[i:r]==s[i-l:r-l]:（偏移量：[0+(i-l):(0+r-l)]）
// Z[i-l]<r-i+1:说明从 i 开始匹配和从 i−l 开始匹配是一样的,匹配一定会在离开Z-Box前停止。即， Z[i]=Z[i−l] 。
// 如果 Z[i−l]≥r−i+1 ，说明整个Z-Box的剩余部分都可以匹配，但后面的情况我们不清楚，所以不能说 Z[i]=Z[i−l] 。
// 但我们知道 Z[i] 至少有 r−i+1 ，接下来我们暴力计算Z-Box还能向后延长多少即可。
struct Solution1{
//    int mx1=0;//最大匹配长度
    vector<int> getz(string s){ //s下标从1开始，匹配自串
        int n=s.length();
        s=" "+s;
        vector<int> z(n+1);
        z[1]=n;
        for(int i=2,L=1,R=0;i<=n;i++){
            if(i<=R)  z[i]=min(z[i-L+1],R-i+1);
            while(i+z[i]<=n&&s[1+z[i]]==s[i+z[i]]) z[i]++;
            if(i+z[i]-1>R) L=i,R=i+z[i]-1;
        }
    }
    vector<int> getzz(string s,string t){//下标从1开始，t的后缀子串匹配s的最长公共前缀
        int sn=s.length();
        int tn=t.length();
        s=" "+s;
        t=" "+t;
        vector<int> z(tn+1);
        for(int i=1,L=1,R=0;i<=tn;i++){
            if(i<=R) z[i]=min(z[i-L+1],R-i+1);
            while(1+z[i]<=sn&&i+z[i]<=tn&&s[1+z[i]]==t[i+z[i]]) z[i]++;
            if(i+z[i]-1>R) L=i,R=i+z[i]-1;
        }
    }
};