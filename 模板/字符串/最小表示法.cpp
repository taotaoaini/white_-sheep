#include <bits/stdc++.h>
using namespace std;
// 【模板】最小表示法 https://www.luogu.com.cn/problem/P1368
// 最小表示法：找出字符串S的循环同构串的字典序最小的那一个
// 循环同构串：S[i:n]+S[1:i-1]=T,T则是S的循环同构串

// 循环串，技巧： 破环成链，复制一倍，然后扫描
// 这是一个三指针控制扫描很好的题目，指针i，j控制匹配起始位置，指针k控制匹配长度
// 如果 此时s[i+k]>s[j+k],s[i:i+k]的所以位置都会被淘汰，i跳到i+k+1位置
// 因为如果能匹配到k，则s[i:i+k]一定是单调不减的序列，所以任何一个都成为不了头
struct Solution1{
    int get_mi(string s){ //O(n)
        s=s+s;
        int n=s.length();
        int i=0,j=1,k=0;
        while(i<n&&j<n){
            for(k=0;k<n&&s[i+k]==s[j+k];k++);
            s[i+k]>s[j+k] ? i=i+k+1:j+k+1;
            if(i==j) j++;
        }
        return min(i,j);
    }
    void solve1(){
        string s;
        cin>>s;
        int n=s.length();
        int k=get_mi(s);
        for(int i=0;i<n;i++){
            cout<<s[k+i];
        }
        cout<<endl;
    }
};