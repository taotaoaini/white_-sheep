#include <bits/stdc++.h>
using  namespace  std;

// （1）L3-025 那就别担心了 https://pintia.cn/problem-sets/994805046380707840/exam/problems/1336215880692482060?type=7&page=1
// 统计A->B的路径方案数，并判断是否从A出发的路径都会到达B
// n=500,记忆化搜索yyds
// 秒点就是将终点的方案f[en]=1,dfs逆推，父亲节点合并子树
#include<bits/stdc++.h>
using namespace std;
using pis=pair<int,string>;
#define fi first
#define se second
const int N=510;
vector<int> e[N];
int n,m;
int f[N];
int st,en; // 起点和终点
bool flag=false; // 标记是否st的路径都是到达en的
int dfs(int u){
    if(f[u]!=-1) return f[u]; // -1表示没有到达的状态
    f[u]=0; //此时，开始合并子树的方案数
    for(int v:e[u]){
        if(f[v]==-1) dfs(v); // 表示v没走过
        f[u]+=max(0,f[v]);
    }
    if(f[u]==0) flag=true;
    return f[u];
}
void solve() {
    cin>>n>>m;
    memset(f,-1,sizeof(f));
    for(int i=1; i<=m; i++) {
        int x,y;
        cin>>x>>y;
        e[x].push_back(y);
    }
    cin>>st>>en;
    f[en]=1;
    dfs(st);
    cout<<f[st]<<" "<<(flag?"No":"Yes");
}
int main() {
    solve();
    cout<<fixed<<setprecision(2);
    return 0;
}
