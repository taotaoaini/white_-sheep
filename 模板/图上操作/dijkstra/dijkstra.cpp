#include<bits/stdc++.h>

using namespace std;
//预头
#define fi first
#define se second
using i64 = long long;
using pii = pair<int, int>;
using pli = pair<i64, int>;
const i64 dinf = 0x3f3f3f3f3f3f3f3f;
const i64 mod = 1000000007;
const int inf=0x3f3f3f3f;
//
// 1.dijkstra
struct Solution1 {
    vector<vector<pii>> e;
    priority_queue<pli, vector<pli>, greater<pli>> q;
    int n, m;
    vector<int> pre; // 打印路径

    int dijkstra(int st, int en) {
        vector<int> dis(n + 1);
        vector<bool> vis(n + 1);
        for (int i = 1; i <= n; i++) dis[i] = dinf, vis[i] = false;
        dis[st] = 0;
        q.push({dis[st], st});
        while (!q.empty()) {
            auto p = q.top();
            q.pop();
            int uw = p.fi;
            int u = p.se;
            if (vis[u]) continue;
            vis[u] = true;
            for (int i = 0; i < e[u].size(); i++) {
                int v = e[u][i].se;
                int vw = e[u][i].fi;
                // if(vis[v]) continue; // 丢弃已经找到最短路径的邻居节点
                if (dis[v] > dis[u] + vw) {
                    dis[v] = dis[u] + vw;
                    q.push({dis[v], v});
                    pre[v] = u;
                }
            }
        }
        return dis[en];
    }

    void solve1() {
        cin >> n >> m;
        // if(n==0&&m==0) return 0;
        e.resize(n + 10);
        for (int i = 1, x, y, w; i <= n; i++) {
            cin >> x >> y >> w;
            e[x].push_back({w, y});
            e[y].push_back({w, x});
        }
        int st = 1, en = n;
        dijkstra(st, en);
        for (int i = 1; i <= n; i++) e[i].clear();
    }
};
//end--

struct Solution2 {
    vector<vector<pii>> e;  //边集数组
    priority_queue<pli, vector<pli>, greater<pli>> q;
    int n, m; //n:顶点，m:边数
    vector<int> f; //方案数
    int dijkstra(int st, int en) { //求最短路的方案数
        f[st] = 1;
        vector<i64> dis(n + 1);
        for (int i = 1; i <= n; i++) dis[i] = dinf;
        dis[st] = 0;
        q.emplace(dis[st], st);
        while (!q.empty()) {
            auto p = q.top();
            q.pop();
            i64 uw = p.fi;
            int u = p.se;
            if (u == n) return f[n]; //如果已达n就直接返回，如果是要求其他所有点则不能写这个
            if (uw > dis[u]) continue; //不是最新的一次更新
            for (int i = 0; i < e[u].size(); i++) {
                int v = e[u][i].se;
                i64 vw = e[u][i].fi;
                i64 nw=uw+vw;
                if (dis[v] >nw) {
                    dis[v] = nw;
                    q.push({dis[v], v});
                    f[v] = f[u];
                } else if (dis[v] == nw) {
                    f[v] = (f[v] + f[u]) % mod;
                }
            }
        }
        return f[n];
    }

    void solve2() {
        cin >> n >> m;
        e.resize(n);
        f.resize(n + 1);
        for (int i = 1, x, y, w; i <= n; i++) {
            cin >> x >> y >> w;
            e[x].emplace_back(w, y);
            e[y].emplace_back(w, x);
        }
        dijkstra(1, n);
        cout << f[n] << endl;
    }
};