//   最短路径 http://codeup.hustoj.com/problem.php?id=1956
// 第k条道路，长度为2^k
// 性质 2^1+2^2+2^3+2^4+2^5<2^6,很简单证明，所以贪心地建边，如果两点已经连通，则不连，并查集维护
// 最后图为有原点的有根树，尽管边已经被取模处理了，但每个点只有一条根节点来的边和连接子节点的边，所以不会对答案造成影响
#include <bits/stdc++.h>
using namespace std;
const int mod = 100000;
const int N = 110;
int n, m;
int fa[N];
int e[N][N];
int dis[N];
int inf = 0x3f3f3f3f;
bool vis[N];
int find(int x) { // 并查集
    while (x != fa[x]) x = fa[x] = fa[fa[x]];
    return x;
}
int f(int k) { // 计算 2^k
    int res = 1;
    for (int i = 1; i <= k; i++) {
        res = (res * 2) % mod;
    }
    return res;
}
void dij(int s) { // 单源最短路
    for (int i = 0; i < n; i++) {
        dis[i] = inf;
        vis[i] = false;
    }
    dis[s] = 0;
    vis[s] = true;
    for (int i = 0; i < n; i++) dis[i] = e[s][i];
    for (int i = 1; i < n; i++) {
        int mi = inf;
        int u = -1;
        for (int j = 0; j < n; j++) {
            if (!vis[j] && dis[j] < mi) {
                mi = dis[j];
                u = j;
            }
        }
        if (u == -1) return;
        vis[u] = true; //只需要经历一次转移的中介点
        for (int j = 0; j < n; j++) {
            if (!vis[j] && e[u][j] + dis[u] < dis[j]) {
                dis[j] = dis[u] + e[u][j];
            }
        }
    }
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    while (cin >> n >> m) {
        for (int i = 0; i < n; i++) { //初始化
            for (int j = 0; j < n; j++) {
                if (i != j)
                    e[i][j] = inf;
            }
        }
        for (int i = 0; i < n; i++) fa[i] = i;
        for (int i = 1; i <= m; i++) { //建边
            int x, y;
            cin >> x >> y;
            int fx = find(x);
            int fy = find(y);
            if (fx == fy) continue; //贪心，如果已经连通，则不练了
            fa[fx] = fy;
            e[x][y] = e[y][x] = f(i - 1);
        }
        dij(0);
        for (int i = 1; i < n ; i++) {
            if (dis[i] == inf) cout << -1 << endl;
            else cout << (dis[i]) % mod << endl;
        }
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    solve();
    return 0;
}