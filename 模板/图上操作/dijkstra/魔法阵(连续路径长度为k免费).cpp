// 3.魔法阵：https://www.lanqiao.cn/problems/3542/learning/
// 在最短路中，可以选出连续出现的K条边，抵消其的边权(可以出现重复的边),求1到n的最短路
// 定义: f[i][j]:表示到达i节点删除了以i为终点的j条边的最短路
// f[v][0]=min(f[u][0]+vw,f[v][0])
// f[v][j]=f[u][j-1]
// f[v][k]=f[u][k]+vw;

// 思考：
// 连续免费k次，和任意免费k次，区别在于
// 任意免费k次 :f[v][cnt]=f[u][cnt]转移过来
// 连续免费k次 : 这k次不能中断, f[v][cnt]=f[v][cnt-1]转移过来,（NO：不能通过f[v][cnt]=f[u][cnt]转移过来，显然因为会发生中断）
// 只有不用这次机会的时候可以转移，f[v][0]=f[u][0]+w,或者用完机会 f[v][k]=f[u][k]+w;
// 朴素写法

#include<bits/stdc++.h>

using namespace std;
#define fi first
#define se second
using pii = pair<int, int>;
const int inf = 0x3f3f3f3f;
int n, k, m;
vector<vector<pii>> e;
vector<array<int, 16>> f;

void dijkstra(int st, int en) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j <= k; j++) {
            f[i][j] = inf;
        }
    }
    queue<int> q;
    q.push(st);
    f[st][0] = 0;
    while (!q.empty()) {
        int u = q.front();
        q.pop();
        for (int i = 0; i < e[u].size(); i++) {
            bool vis = false;
            int vw = e[u][i].fi;
            int v = e[u][i].se;
            if (f[u][0] + vw < f[v][0]) { //(1)
                vis = true;
                f[v][0] = f[u][0] + vw;
            }
            for (int j = 1; j <= k; j++) { //(2)
                if (f[u][j - 1] < f[v][j]) {
                    vis = true;
                    f[v][j] = f[u][j - 1];
                }
            }
            if (f[u][k] + vw < f[v][k]) { //(3)
                vis = true;
                f[v][k] = f[u][k] + vw;
            }
            if (vis) q.push(v);
        }
    }
}

void solve() {
    cin >> n >> k >> m;
    f.resize(n + 1);
    e.resize(n + 1);
    for (int i = 0, x, y, w; i < m; i++) {
        cin >> x >> y >> w;
        e[x].emplace_back(w, y);
        e[y].emplace_back(w, x);
    }
    int st = 0, en = n - 1;
    dijkstra(st, en);
    cout << f[n - 1][k] << endl;
}

int main() {
    solve();
}