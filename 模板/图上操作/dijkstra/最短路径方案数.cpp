// (求1到n最短路的方案数) 到达目的地的方案数:https://leetcode.cn/problems/number-of-ways-to-arrive-at-destination/description/
// 堆优化 Dijkstra（适用于稀疏图）
// 时间复杂度：O(mlogm)

#include<bits/stdc++.h>

using namespace std;
#define fi first
#define se second
using pii = pair<int, int>;
using pli = pair<long long, int>;
using i64 = long long;
const i64 dinf = 0x3f3f3f3f3f3f3f3f;
const int mod = 1e9 + 7;
const int N = 1e5 + 10;
vector<pii> e[N];
priority_queue<pli, vector<pli>, greater<pli>> q;
int n, m; //n:顶点，m:边数
int f[N]; //方案数
i64 dis[N];

int dijkstra(int st, int en) { //求最短路的方案数
    f[st] = 1;
    for (int i = 1; i <= n; i++) dis[i] = dinf;
    dis[st] = 0;
    q.emplace(dis[st], st);
    while (!q.empty()) {
        auto p = q.top();
        q.pop();
        i64 uw = p.fi;
        int u = p.se;
        if (u == en) return f[n]; //如果已达n就直接返回，如果是要求其他所有点则不能写这个
        if (uw > dis[u]) continue; //不是最新的一次更新
        for (int i = 0; i < e[u].size(); i++) {
            int v = e[u][i].se;
            i64 vw = e[u][i].fi;
            i64 nw = uw + vw;
            if (dis[v] > nw) {
                dis[v] = nw;
                q.push({dis[v], v});
                f[v] = f[u];
            } else if (dis[v] == nw) {
                f[v] = (f[v] + f[u]) % mod;
            }
        }
    }
    return f[en];
}

void clear(int n) {
    for (int i = 0; i <= n; i++) {
        f[i] = 0;
        e[i].clear();
    }
}

void solve2() {
    cin >> n >> m;
    for (int i = 1, x, y, w; i <= n; i++) {
        cin >> x >> y >> w;
        e[x].emplace_back(w, y);
        e[y].emplace_back(w, x);
    }
    dijkstra(1, n);
    cout << f[n] << endl;
    clear(n);
}

