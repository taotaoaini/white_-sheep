//  [JLOI2011] 飞行路线 https://www.luogu.com.cn/problem/P4568
// 题解：https://blog.csdn.net/swust5120171204/article/details/98958733
// 每个地点的访问次数不限，免费k次消费,求0到n-1的最小代价
// n=1e4,m=5e4
// 稠密图和系数图判断
// 以m=nlogn作为区别稀疏图和稠密图的的标准

// 1.dp思考+单源最短路最小堆优化
// f[i][j] : 表示到达i节点，使用了j次免费机会

#include <bits/stdc++.h>
using namespace std;
const int N=1e4+10;
using pii = pair<int,int>;
#define fi first
#define se second
int n,m,k;
int st,en;
int f[N][20];
vector<pii> e[N];
int dijkstra(int st,int en){
    memset(f,0x3f,sizeof(f)); // 初始化不可达值
    priority_queue<array<int,3>,vector<array<int,3>>,greater<array<int,3>>> q; // 小顶堆
    q.push({0,st,0}); // 存入初始化
    f[st][0]=0; // 初始化
    while(q.size()){
        auto p=q.top();
        q.pop();
        int w=p[0];
        int u=p[1];
        int cnt=p[2];
        if(w!=f[u][cnt]) continue;
        if(u==en) {return w;} // 表示此时到达en已经是最优的了
        for(auto x:e[u]){
            int v=x.fi;
            int vw=x.se;
            if(cnt+1<=k&&f[v][cnt+1]>w){ //f[v][cnt+1]=f[u][cnt]
                f[v][cnt+1]=w;
                q.push({f[v][cnt+1],v,cnt+1});
            }
            if(f[v][cnt]>w+vw){ //f[v][cnt]=f[u][cnt]+vw
                f[v][cnt]=w+vw;
                q.push({f[v][cnt],v,cnt});
            }
        }
    }
    return -1;
}

void solve(){
    cin>>n>>m>>k;
    cin>>st>>en;
    for(int i=1;i<=m;i++){
        int u,v,w;
        cin>>u>>v>>w;
        e[u].push_back({v,w});
        e[v].push_back({u,w});
    }
    cout<<dijkstra(st,en)<<endl;
}
int main(){
    solve();
    return 0;
}
