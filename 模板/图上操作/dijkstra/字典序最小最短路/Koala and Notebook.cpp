// F. Koala and Notebook https://codeforces.com/problemset/problem/1209/F
// 考虑到达某个点时的数长度要尽量短，那么可以把边长看成此边十进制下的位数
// 边长都很小，所以可以暴力拆边，把边权都拆成1,这样就可以BFS了
//
#include<bits/stdc++.h>

using namespace std;
const int N = 1000010;
const int mod = 1e9 + 7;
int n, m;
vector<int> e[N][10], q[N];
int tot;
bool vis[N];
int ans[N];

vector<int> g(int n) {
    vector<int> ans;
    while (n) {
        int x = n % 10;
        n /= 10;
        ans.push_back(x);
    }
    reverse(ans.begin(), ans.end());
    return ans;
}

void solve() {
    cin >> n >> m;
    tot = n;
    for (int i = 1; i <= m; i++) {
        int x, y;
        cin >> x >> y;
        vector<int> a = g(i); // 获取i的每位数
        int sz = a.size();
        int pre = x;
        for (int j = 0; j < sz; j++) {
            int ne = (j == sz - 1) ? y : ++tot;
            e[pre][a[j]].push_back(ne);
            pre = ne;
        }
        pre = y;
        for (int j = 0; j < sz; j++) {
            int ne = (j == sz - 1) ? x : ++tot;
            e[pre][a[j]].push_back(ne);
            pre = ne;
        }
    }
    int T = 0;
    q[++T].push_back(1);
    vis[1] = 1;
    for (int i = 1; i <= T; i++) {
        for (int j = 0; j <= 9; j++) {
            bool flag = 0;
            for (auto x: q[i]) {
                for (auto v: e[x][j]) {
                    if (vis[v]) continue;
                    vis[v] = flag = 1;
                    q[T + 1].push_back(v);
                    ans[v] = (10LL * ans[x] + j) % mod;
                }
            }
            if (flag) T++;
        }
    }
    for (int i = 2; i <= n; i++) cout << ans[i] << endl;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    solve();
    return 0;
}