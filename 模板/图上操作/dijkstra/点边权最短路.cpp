//  出差  https://dashoj.com/d/problemDay/p/54
// 同时存在点权、边权的图上最短路问题，起点和终点的点权不需要考虑
// 最小堆优化最短路

#include<bits/stdc++.h>

using namespace std;
#define fi first
#define se second
#define endl '\n'
using pii = pair<int, int>;
const int N = 1010;
int n, m;
int a[N];
vector<pii> e[N];
int dis[N];
int S, T;

void dij() {
    memset(dis, 0x3f, sizeof(dis));
    dis[S] = 0;
    priority_queue<pii, vector<pii>, greater<pii>> q;
    q.push({0, S});
    while (q.size()) {
        auto p = q.top();
        q.pop();
        int u = p.se, uw = p.fi;
        if (dis[u] < uw) continue;
        if (u == n) return;
        for (auto t: e[u]) {
            int v = t.fi;
            int w = t.se + uw + (v == n ? 0 : a[v]); // 同时考虑边权和点权
            if (dis[v] > w) {
                dis[v] = w;
                q.push({w, v});
            }
        }
    }
}

void solve() {
    cin >> n >> m;
    S = 1, T = n;
    for (int i = 1; i <= n; i++) cin >> a[i];
    for (int i = 1; i <= m; i++) {
        int x, y, w;
        cin >> x >> y >> w;
        e[x].push_back({y, w});
        e[y].push_back({x, w});
    }
    dij();
    cout << dis[n] << endl;
}

int main() {
//	freopen("a.in","r",stdin);
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    solve();
    return 0;
}