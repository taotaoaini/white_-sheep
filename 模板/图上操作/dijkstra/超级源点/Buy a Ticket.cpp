// Buy a Ticket  https://codeforces.com/problemset/problem/938/D
// 从最小的a[i]为起始点出发，可以更新到周围的点的答案，从次小的a[i]出发...
// 能否Dijkstra做,一个点到达另一个点的代价是路径边权和+终点点权
// 正向不好考虑，可以从终点考虑，这样点权固定，路径边权和就好求了
// 暴力求法：以每个点作为起点，跑一次单源最短路
// 优化：拆点，将点权拆成i->n+1，这样就只需跑一次能否Dijkstra了。
#include<bits/stdc++.h>
using namespace std;
const int N = 200010;
using i64 = long long;
using pll = pair<i64, i64>;
#define fi first
#define se second
vector<pll> e[N];
int n, m;
i64 dis[N];
priority_queue<pll, vector<pll>, greater<pll>> q;
void dijkstra(int st) {
    memset(dis, 0x3f, sizeof(dis));
    dis[st] = 0;
    q.push({dis[st], st});
    while (!q.empty()) {
        auto p = q.top();
        q.pop();
        i64 uw = p.fi;
        i64 u = p.se;
        if (uw > dis[u]) continue;
        for (int i = 0; i < e[u].size(); i++) {
            i64 v = e[u][i].fi;
            i64 w = e[u][i].se + uw;
            if (dis[v] > w) {
                dis[v] = w;
                q.push({w, v});
            }
        }
    }
}
void solve() {
    cin >> n >> m;
    for (int i = 1; i <= m; i++) {
        i64 x, y, w;
        cin >> x >> y >> w;
        e[x].push_back({y, 2 * w});
        e[y].push_back({x, 2 * w});
    }
    for (int i = 1; i <= n; i++) {
        i64 x;
        cin >> x;
        e[n + 1].push_back({i, x});
        e[i].push_back({n + 1, x});
    }
    dijkstra(n + 1);
    for (int i = 1; i <= n; i++) cout << dis[i] << " ";
    cout << endl;
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    solve();
    return 0;
}