// 最短路径中的边 https://leetcode.cn/problems/find-edges-in-shortest-paths/description/

// 1.跑dijkstra，求出最短路
// 2.通过DFS，BFS从终点找到全部符合条件的边
#include<bits/stdc++.h>

using namespace std;
#define fi first
#define se second
using i64 = long long;
using pli = pair<i64, int>;
const i64 dinf = 0x3f3f3f3f3f3f3f3f;

vector<bool> findAnswer(int n, vector<vector<int>> &edges) {
    vector<vector<array<int, 3>>> g(n);
    for (int i = 0; i < edges.size(); i++) {
        auto &e = edges[i];
        int x = e[0], y = e[1], w = e[2];
        g[x].push_back({y, w, i});
        g[y].push_back({x, w, i});
    }
    vector<i64> dis(n, dinf);
    dis[0] = 0;
    priority_queue<pli, vector<pli>, greater<>> pq;
    pq.push({0, 0});
    while (!pq.empty()) {
        auto p = pq.top();
        pq.pop();
        int dx = p.fi;
        int x = p.se;
        if (dx > dis[x]) continue;
        for (auto &e: g[x]) {
            int dy = dx + e[1];
            int y = e[0]; // 节点
            if (dy < dis[y]) {
                dis[y] = dy;
                pq.push({dy, y});
            }
        }
    }
    vector<bool> ans(edges.size());
    if (dis[n - 1] == dinf) return ans;
    vector<bool> vis(n);
    function<void(int)> dfs = [&](int u) { //从终点进行dfs
        vis[u] = true;
        for (auto e: g[u]) {
            int v = e[0];
            int w = e[1];
            int idx = e[2]; // 输入时边的编号
            if (dis[v] + w != dis[u]) continue;
            ans[idx] = true;
            if (!vis[v]) dfs(v);
        }
    };
    dfs(n - 1); // 从终点递归，找到符合条件的边
    return ans;
}

};