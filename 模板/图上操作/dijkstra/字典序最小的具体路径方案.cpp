// 无向图 有n个城市m条道路（n<1000, m<10000)，每条道路有个长度，请找到从起点s到终点t的最短距离和经过的城市名。
// 输入n，m，s（起点），t（重点）
#include<bits/stdc++.h>
using namespace std;
using pii = std::pair<int, int>;
#define fi first
#define se second

const int N = 1010;
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
int dis[N];
vector<pii> e[N];
int pre[N];
int n, m, s, t;
void dij(int s, int t) {
    for (int i = 1; i <= n; i++) dis[i] = inf, pre[i] = i;
    queue<pii> q;
    q.push({0, s});
    dis[s] = 0;
    while (q.size()) {
        auto p = q.front();
        q.pop();
        int u = p.se, w = p.fi;
        if (w != dis[u]) continue;
        for (auto ed : e[u]) {
            int v = ed.fi, vw = ed.se;
            if (dis[v] > w + vw) {
                dis[v] = w + vw;
                pre[v] = u;
                q.push({dis[v], v});
            } else if (dis[v] == w + vw) {
                pre[v] = min(pre[v], u);
            }
        }
    }
}
void init() {
    for (int i = 1; i <= n; i++) {
        e[i].clear();
    }
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);

    while (cin >> n >> m >> s >>t) {
        for (int i = 1; i <= m; i++) {
            int x, y, w;
            cin >> x >> y >> w;
            e[x].push_back({y, w});
            e[y].push_back({x, w});
        }
        dij(s, t);
        if (dis[t] == inf) {
            cout << "can't arrive" << endl;
            return;
        }
        cout << dis[t] << endl;
        vector<int> ans;
        int x = t;
        while (x != pre[x]) { // 路径打印
            ans.push_back(x);
            x = pre[x];
        }
        ans.push_back(x);
        reverse(ans.begin(), ans.end());
        for (int i = 0; i < ans.size(); i++) {
            cout << ans[i] << " ";
        }
        cout << endl;
        init();
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}