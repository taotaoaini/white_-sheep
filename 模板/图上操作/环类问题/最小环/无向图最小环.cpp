// 观光之旅 https://www.acwing.com/problem/content/description/346/

#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
const int N=110;
const int inf=0x3f3f3f3f;
int n,m;
int e[N][N];
int f[N][N];
int res=inf;
int path[N],cnt=0;
int pos[N][N];
void dfs(int i,int j){ //顺序打印[i:j]的路径
    if(pos[i][j]==0) return;
    int k=pos[i][j];
    dfs(i,k);
    path[++cnt]=k; // 中序遍历
    dfs(k,j);
}
void solve(){
    cin>>n>>m;
    for(int i=1;i<=n;i++)
        for(int j=1;j<=n;j++)
            if(i!=j) e[i][j]=inf;
    for(int i=1;i<=m;i++){
        int x,y,w;
        cin>>x>>y>>w;
        e[x][y]=e[y][x]=min(e[x][y],w);
    }
    memcpy(f,e,sizeof(f));
    for(int k=1;k<=n;k++){
        for(int i=1;i<k;i++){
            for(int j=1;j<i;j++){
                if((i64)f[i][j]+e[i][k]+e[k][j]<res){ // 理解一下过程
                    // 找环的时间复杂度分析，状态量，一个点被访问多少次，上界：O(n^2)
                    res=f[i][j]+e[i][k]+e[k][j];
                    cnt=0;
                    path[++cnt]=k; // 顺序打印
                    path[++cnt]=i;
                    dfs(i,j);
                    path[++cnt]=j;
                }
            }
        }
        for(int i=1;i<=n;i++){ // 更新需要在找环后面,防止k在[i:j]路径中
            for(int j=1;j<=n;j++){
                if(f[i][j]>f[i][k]+f[k][j]){
                    f[i][j]=f[i][k]+f[k][j];
                    pos[i][j]=k;
                }
            }
        }
    }
    if(res==inf) cout<<"No solution."<<endl;
    else{
        for(int i=1;i<=cnt;i++) cout<<path[i]<<" ";
        cout<<endl;
    }
}
int main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}