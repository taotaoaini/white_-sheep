//  Roadblocks G https://www.luogu.com.cn/problem/P2865
// 思考：
// 考虑 Dijkstra ，通过归纳法很容易证明一个点第二次出队就能得到次短路。
#include<bits/stdc++.h>
#define fi first
#define se second
#define mp make_pair
using namespace std;
using pii= pair<int,int>;
const int N=5010;
priority_queue<pii,vector<pii>,greater<pii>> q;
int n,m;
vector<pii> e[N];
int dis[N][2];
int S,T; // 起点，终点
void dij(){
    memset(dis,0x3f,sizeof(dis));
    dis[S][0]=0;
    q.push(mp(0,S));
    while(q.size()){
        auto p= q.top();
        q.pop();
        int u=p.se,uw=p.fi;
        if(uw>dis[u][1]) continue; // 比最短路和次短路还要小，直接跳过
        for(auto t:e[u]){
            int v=t.fi,nw=t.se+uw;
            if(dis[v][0]>nw){ //比最短路小，更新最短路和次短路
                dis[v][1]=dis[v][0];
                dis[v][0]=nw;
                q.push(mp(nw,v));
            }
            if(dis[v][1]>nw&&dis[v][0]<nw){ //更新次短路
                dis[v][1]=nw;
                q.push(mp(nw,v));
            }
        }
    }
}
void solve(){
    cin>>n>>m;
    S=1,T=n;
    for(int i=1;i<=m;i++){
        int x,y,w;
        cin>>x>>y>>w;
        e[x].push_back({y,w});
        e[y].push_back({x,w});
    }
    dij();
    cout<<dis[n][1]<<endl;
}
int main(){
    solve();
    return 0;
}