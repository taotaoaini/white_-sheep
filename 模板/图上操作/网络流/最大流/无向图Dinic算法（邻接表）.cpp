// 无向图,注意正反建边，容量都为c
// 前面的位置都没有影响，最后在T都会限流

#include <bits/stdc++.h>
// 时间复杂度：O(n^2*m)  n=10000,m=100000完全能过
using namespace std;
using i64 = long long;
const int N = 10010;
const int M = 2000010;
const i64 inf = 1e16;
int n, m, S, T;
struct edge {
    int v;
    i64 c;
    int ne;
} e[M];
int h[N], idx = 1;
int d[N], cur[N];

void add(int a, int b, i64 c) {
    e[++idx] = {b, c, h[a]};
    h[a] = idx;
}

bool bfs() { //对点分层，找增广路
    memset(d, 0, sizeof d);
    queue<int> q;
    q.push(S);
    d[S] = 1;
    while (q.size()) {
        int u = q.front();
        q.pop();
        for (int i = h[u]; i; i = e[i].ne) {
            int v = e[i].v;
            if (d[v] == 0 && e[i].c) {
                d[v] = d[u] + 1;
                q.push(v);
                if (v == T) return true;
            }
        }
    }
    return false;
}

i64 dfs(int u, i64 mf) {
    if (u == T) return mf;
    i64 sum = 0;
    for (int i = cur[u]; i; i = e[i].ne) {
        cur[u] = i; // 当前弧优化(前面的边已经找完了)
        int v = e[i].v;
        if (d[v] == d[u] + 1 && e[i].c) {
            i64 f = dfs(v, min(mf, e[i].c));
            e[i].c -= f;
            e[i ^ 1].c += f; // 更新残留网
            sum += f; // 累加u的流出流量
            mf -= f; // 减少u的剩余流量
            if (mf == 0) break; //余量优化，剩余流量减为0，不需要再找下去了
        }
    }
    if (sum == 0) d[u] = 0; // 残枝优化，找不到可行流，直接从图中删除该节点,下次不会递归到次节点了
    return sum;
}

i64 dinic() {
    i64 res = 0;
    while (bfs()) {
        memcpy(cur, h, sizeof h);
        res += dfs(S, inf);
    }
    return res;
}

void solve() {
    cin >> n >> m;
    S=1,T=n;
    for (int i = 1; i <= m; i++) {
        int a, b;
        i64 c;
        cin >> a >> b >> c;
        add(a, b, c);
        add(b, a, c);
    }
    cout << dinic() << endl;
}

int main() {
    solve();
    return 0;
}