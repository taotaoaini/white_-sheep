// 【模板】网络最大流 https://www.luogu.com.cn/problem/P3376
// [USACO4.2] 草地排水Drainage Ditches
#include <bits/stdc++.h>

using namespace std;
using i64 = long long;
const int N = 1010;
const int M = 200010;
const int inf = 0x3f3f3f3f;
int n, m, S, T;
struct edge {
    int u, v;
    i64 c;
};
vector<edge> e;
vector<int> h[N]; // 边集数组
i64 mf[N];
int pre[N];

void add(int a, int b, i64 c) {
    e.push_back({a, b, c});
    h[a].push_back(e.size() - 1);
    e.push_back({b, a, 0});
    h[b].push_back(e.size() - 1);
}

bool bfs() { // 找增广路，找到一条正值的路径
    memset(mf, 0, sizeof(mf));
    queue<int> q;
    q.push(S);
    mf[S] = inf ;
    while (q.size()) {
        int u = q.front();
        q.pop();
        for (int i = 0; i < h[u].size(); i++) {
            int j = h[u][i];
            int v = e[j].v;
            if (mf[v] == 0 && e[j].c) {
                mf[v] = min(mf[u], e[j].c);
                pre[v] = j; //存前驱边
                q.push(v);
                if (v == T) return true;
            }
        }
    }
    return false;
}

i64 EK() { //累加可行流
    i64 res = 0;
    while (bfs()) {
        int v = T;
        while (v != S) { //更新残留网
            int i = pre[v];
            e[i].c -= mf[T];
            e[i ^ 1].c += mf[T]; // 构造回退容量
            v = e[i ^ 1].v;
        }
        res += mf[T];
    }
    return res;
}

void solve() {
    cin >> n >> m;
    S = 1, T = n;
    for (int i = 1; i <= m; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        add(a, b, c);
    }
    cout << EK() << endl;
}

int main() {
    solve();
    return 0;
}