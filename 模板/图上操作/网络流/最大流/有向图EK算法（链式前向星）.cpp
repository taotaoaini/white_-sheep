// 【模板】网络最大流 https://www.luogu.com.cn/problem/P3376

// 算法步骤
// 1.从源点开始，用BFS找一条最短的增广路径，计算该路径上的残量最小值，累加到最大流值
// 2.沿着该路径修改流量值，实际是修改是残量网络的边权
// 3.重复上述步骤，直到找不到增广路时，此时得到的流就是最大流。
// 存图：
// 广度优先搜索：如果采用邻接矩阵作为图的存储结构，时间复杂度为O(V*2)
// 如果采用邻接表作为图的存储结构，时间复杂度为O(V+E)

// EK算法
// O(n*m^2):；理论上限 n=1000,m=10000 (证明)

#include <bits/stdc++.h>
using namespace std;
using i64 = long long;
const int M=200010;
const int N=1010;
struct edge{
    int v;
    i64 w;
    int ne;
}e[M];
int h[N],idx=1; // 从2，3开始配对
i64 mf[N],pre[N];
int n,m,S,T;
void add(int a,int b,int c){
    e[++idx]={b,c,h[a]};
    h[a]=idx;
}
bool bfs(){ // 找增广路
    memset(mf,0,sizeof(mf));
    queue<int> q;
    q.push(S);
    mf[S]=1e9; // 后面用的边权，所以源点mf[S]=1e9;
    while(q.size()){  //(O(m))
        int u=q.front();q.pop();
        for(int i=h[u];i;i=e[i].ne){
            int v=e[i].v;
            if(mf[v]==0&&e[i].w){ // mf[v]最小流
                mf[v]=min(mf[u],e[i].w);
                pre[v]=i; // 存前驱边
                q.push(v);
                if(v==T) return true;
            }
        }
    }
    return false;
}
i64 EK(){
    i64 res=0;
    while(bfs()){
        int v=T;
        while(v!=S){
            int i=pre[v];
            e[i].w-=mf[T];
            e[i^1].w+=mf[T];
            v=e[i^1].v;
        }
        res+=mf[T];
    }
    return res;
}
void solve(){
    cin>>n>>m>>S>>T;
    for(int i=1;i<=m;i++){
        int a,b,c;
        cin>>a>>b>>c;
        add(a,b,c);
        add(b,a,0); // 反向边
    }
    cout<<EK()<<endl;
}
int main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}