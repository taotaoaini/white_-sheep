//  最长不下降子序列问题 https://www.luogu.com.cn/problem/P2766
// 题解： https://blog.csdn.net/Yubing792289314/article/details/104478470

// n=500
#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
const int M = 200010;
const int N = 510;
const int NN=300010;
int dp[N];
int a[N];
const int inf = 0x3f3f3f3f;
// 计算其最长不下降子序列的长度s
int LIS(int n) {
    int res = 0;
    for (int i = 1; i <= n; i++) {
        dp[i] = 1;
        for (int j = 1; j < i; j++) {
            if (a[i] >= a[j]) dp[i] = max(dp[i], dp[j] + 1);
        }
        res = max(res, dp[i]);
    }
    return res;
}
// 每个元素只允许使用一次，计算从给定的序列中最多可取出多少个长度为 s 的不下降子序列。
// 分析
// 将所有的点i拆分为ia,ib两点，两点相连，容量为1
// 根据规则，当i<=j,ai<=aj,且dp[i]+1=dp[j]时，让ia和jb相连，容量为1
// dp[i]=1时，将源点与i相连，容量为1
// dp[i]=s时，将汇点与i相连，容量为1

// 思考：
// 为什么要拆点，不可以直接根据关系建图吗
// 考虑到每个点只能用一次，所以 ia − ib这条边表示的就是一个点，因为网络流里边只能用一次(容量为1嘛)，这不就是符合题目第二问要求每个点只能用一次嘛
struct edge {
    int v;
    i64 w;
    int ne;
} e[M];
int h[NN], idx = 1; // 从2，3开始配对
i64 mf[NN], pre[NN];
int n, m, S, T;
int mx;
void add(int a, int b, int c) {
    e[++idx] = {b, c, h[a]};
    h[a] = idx;
    e[++idx] = {a, 0, h[b]};
    h[b] = idx;
}
bool bfs() { // 找增广路
    memset(mf, 0, sizeof(mf));
    queue<int> q;
    q.push(S);
    mf[S] = 1e9; // 后面用的边权，所以源点mf[S]=1e9;
    while (q.size()) { //(O(m))
        int u = q.front(); q.pop();
        for (int i = h[u]; i; i = e[i].ne) {
            int v = e[i].v;
            if (mf[v] == 0 && e[i].w) { // mf[v]最小流
                mf[v] = min(mf[u], e[i].w);
                pre[v] = i; // 存前驱边
                q.push(v);
                if (v == T) return true;
            }
        }
    }
    return false;
}
i64 EK() {
    i64 res = 0;
    while (bfs()) {
        int v = T;
        while (v != S) {
            int i = pre[v];
            e[i].w -= mf[T];
            e[i ^ 1].w += mf[T];
            v = e[i ^ 1].v;
        }
        res += mf[T];
    }
    return res;
}
void init() { // 建边
    idx = 1;
    memset(h, 0, sizeof h);
    for (int i = 1; i <= n; i++) {
        add(i, i + n, 1);
        for (int j = 1; j < i; j++) {
            if (a[i] >= a[j] && dp[i] == dp[j] + 1) {
                add(j + n, i, 1); //! 节点不要写错了
            }
        }
    }
    for (int i = 1; i <= n; i++) {
        if (dp[i] == 1) {
            add(S, i, 1);
        }
        if (dp[i] == mx) { //！s有等于1时
            add(i + n, T, 1);
        }
    }
}
void solve() {
    cin >> n;
    for (int i = 1; i <= n; i++) cin >> a[i];
    if (n == 1) { // n=1特判
        cout << 1 << endl << 1 << endl << 1 << endl;
        return;
    }
    S = 0, T = 2 * n + 1;
    mx = LIS(n);
    cout << mx << endl;
    init();
    cout << EK() << endl;
    init();
    add(S, 1, inf); // 直接添加无穷大的边 原来的那根有没有无所谓了
    add(1, 1 + n, inf); // if(dp[1]==1)
    add(n, 2 * n, inf);
    if (dp[n] == mx) { //if(dp[n]==mx)
        add(2 * n, T, inf);
    }
    cout << EK() << endl;
}
int main() {
    solve();
    return 0;
}