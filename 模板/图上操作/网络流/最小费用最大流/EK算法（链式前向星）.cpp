// 【模板】最小费用最大流 https://www.luogu.com.cn/problem/P3381

// 在最大流的前提下，费用最小

// 时间复杂度：O(n^2*m)
// n=5010,m=50010;(没有压力)
#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
const int N = 5010;
const int M = 100010;
const int inf = 0x3f3f3f3f;
int n, m, S, T;
struct edge {
    int v;
    i64 c;
    i64 w;
    int ne;
} e[M];
int h[N], idx = 1; // 从2，3开始配对
int pre[N], vis[N];
i64 d[N], mf[N];
i64 flow, cost;

void add(int a, int b, int c, int d) {
    e[++idx] = {b, c, d, h[a]};
    h[a] = idx;
}

bool spfa() {
    memset(d, 0x3f, sizeof d);
    memset(mf, 0, sizeof mf);
    queue<int> q;
    q.push(S);
    d[S] = 0;
    mf[S] = inf;
    vis[S] = 1;
    while (q.size()) {
        int u = q.front();
        q.pop();
        vis[u] = 0;
        for (int i = h[u]; i; i = e[i].ne) {
            int v = e[i].v;
            i64 c = e[i].c, w = e[i].w;
            if (d[v] > d[u] + w && c) {
                d[v] = d[u] + w;
                pre[v] = i;
                mf[v] = min(mf[u], c);
                if (!vis[v]) {
                    q.push(v);
                    vis[v] = 1;
                }
            }
        }
    }
    return mf[T]>0;
}

void EK() {
    while (spfa()) {
        for (int v = T; v != S;) {
            int i = pre[v];
            e[i].c -= mf[T];
            e[i ^ 1].c += mf[T];
            v = e[i ^ 1].v;
        }
        flow += mf[T];  //累加可行流
        cost += mf[T] * d[T];//累加费用
    }
}

void solve() {
    cin >> n >> m >> S >> T;
    for (int i = 1; i <= m; i++) {
        int a, b, c, d;
        cin >> a >> b >> c >> d;
        add(a, b, c, d);
        add(b, a, 0, -d); // 回边容量为0，费用为负
    }
    EK();
    cout << flow << " " << cost << endl;
}

int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}