#include<bits/stdc++.h>

using namespace std;
using i64 = long long;
const i64 dinf = 0x3f3f3f3f3f3f3f3f;
// 1.Belloman-Ford 源点s->所有n个节点的最短路
// 时间复杂度 O(nm)
vector<array<int, 3>> e;
int n, m, cnt; //n:顶点，m：边
vector<int> pre;

void path(int st, int en) {
    if (st == en) {
        cout << st << " ";
        return;
    }
    path(st, pre[en]);
    cout << en << " ";
}

int bell(int st, int en) {
    vector<int> dis(n);
    for (int i = 1; i <= n; i++) dis[i] = dinf;
    dis[st] = 0;
    for (int k = 1; k <= n; k++) {
        for (int i = 0; i < cnt; i++) { //检查每条边
            int x = e[i][0], y = e[i][1], w = e[i][2];
            if (dis[x] > dis[y] + w) {
                //x 通过y到达起点s，如果距离更短，更新
                dis[x] = dis[y] + w;
                pre[x] = y;
            }
        }
    }
    for (int i = 0; i < cnt; i++) {
        int x = e[i][0], y = e[i][1], w = e[i][2];
        if (dis[x] > dis[y] + w) {
            //x 通过y到达起点s，如果距离更短，更新
            return -1; // 存在负圈
        }
    }
    return dis[en];
}

void solve() {
    cin >> n >> m;
    pre.resize(n + 10);
    for (int i = 1, x, y, w; i <= m; i++) {
        cin >> x >> y >> w;
        e.push_back({x, y, w});
        e.push_back({y, x, w});
    }
    cnt = e.size();
    int st = 1, en = n;
    bell(st, en);
}
int main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}