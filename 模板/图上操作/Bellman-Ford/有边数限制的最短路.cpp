// 有边数限制的最短路 https://www.acwing.com/problem/content/description/855/

#include<bits/stdc++.h>
using namespace std;

const int N = 510, M = 10010;

struct edge {
    int u;
    int v;
    int w;
} e[M];//把每个边保存下来即可
int dis[N];
int g[N];//备份数组防止串联
int n, m, k;//k代表最短路径最多包涵k条边
void bell(int s) {
    memset(dis, 0x3f, sizeof(dis));
    dis[s] = 0;
    for (int i = 0; i < k; i++) {//k次循环
        memcpy(g, dis, sizeof(dis));
        for (int j = 0; j < m; j++) {//遍历所有边
            int u = e[j].u, v = e[j].v, w = e[j].w;
            dis[v] = min(dis[v], g[u] + w);
            //使用backup:避免给a更新后立马更新b, 这样b一次性最短路径就多了两条边出来
        }
    }
}
void solve(){
    cin>>n>>m>>k;
    for (int i = 0; i < m; i++) {
        int a, b, w;
        cin>>a>>b>>w;
        e[i] = {a, b, w};
    }
    bell(1);
    if (dis[n] > 0x3f3f3f3f / 2) puts("impossible");
    else cout<<dis[n]<<endl;
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}