// Ehab's Last Theorem https://codeforces.com/problemset/problem/1325/F
// 题解：https://codeforces.com/blog/entry/68138
// 无向图 1<=n<=100000,n-1<=m<=200000，必连通，可能是树或者带环图

// 问题1：求图上简单环，板子题，只需要构造dfs树，根据dfs树的性质，直接在dfs树上找返祖边即可
// 如果存在点数>=sqrt(n)的环直接输出

// 问题2：求独立集问题
// 用d-1种颜色，给dfs树上的节点按dep[i]%(d-1)的规则染色
// 根据dfs树的特性，相同深度的节点必定构成一个独立集
// 在模意义下相同的任意两个不同深度节点之间，如果存在直连边就说明存在一个节点数>=d的环，简单画个图就能理解。
// 因此我们只需要找到一个点数>=d的集合并恰好输出d个点就好
#include<bits/stdc++.h>
using namespace std;
const int N = 100010;
int n, m;
vector<int> e[N];
int dep[N]; // dfs树 i的深度
int c[N]; // 抽屉
int fa[N]; // dfs树 i的父节点
int d; // ceil(sqrt(n))
bool flag; // 表示问题1是否可行
vector<int> ans; // 问题1的集合

void dfs(int u) {
    // 染色 (d-1) 抽屉原理，反证法：如果没有颜色的数量>=d,则总的最大数量为(d-1)*(d-1)<d*d,不符合条件
    c[dep[u] % (d - 1)] += 1;
    for (auto v : e[u]) {
        if (v == fa[u]) continue;
        if (dep[v] == -1) {
            dep[v] = dep[u] + 1;
            fa[v] = u;
            dfs(v);
        } else {
            if (dep[u] - dep[v] + 1 >= d && !flag) { // 问题1达标
                flag = true;
                int x = u;
                while (x != fa[v]) {
                    ans.push_back(x);
                    x = fa[x];
                }
            }
        }
    }
}

void solve() {
    cin >> n >> m;
    d = ceil(sqrt(1.0 * n));
    for (int i = 0; i <= n; i++) dep[i] = -1;
    dep[1] = 0;
    for (int i = 1; i <= m; i++) {
        int x, y;
        cin >> x >> y;
        e[x].push_back(y);
        e[y].push_back(x);
    }
    dfs(1);
    if (flag) {
        cout << 2 << endl;
        cout << ans.size() << endl;
        for (auto x : ans) cout << x << " ";
    } else {
        cout << 1 << endl;
        int mx[2] {0, -1};
        for (int i = 0; i < d; i++) {
            if (mx[0] < c[i]) {
                mx[0] = c[i];
                mx[1] = i;
            }
        }
        int cnt = d;
        for (int i = 1; i <= n; i++) {
            if (dep[i] % (d - 1) == mx[1]) {
                cnt--;
                cout << i << " ";
                if (!cnt) break;
            }
        }
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    solve();
    return 0;
}