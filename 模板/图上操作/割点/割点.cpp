// 【模板】割点（割顶） https://www.luogu.com.cn/problem/P3388
// 割点定义：如果去掉一个点以及与它连接的边，该点原来所在的图被分成两部分（不连通），则称该点为割点。
// 判断方法：
// 1）如果x不是根节点，当搜索树上存在x的一个子节点y，满足low[y]>=dfn[x],那么x就是割点。
// 2）如果x是根节点，当x存在至少两个子节点y1,y2,那么x就是割点。

// n=20000,m=100000,编号从1开始
// tarjan算法，时间复杂度：O(n+m)
#include<bits/stdc++.h>
using namespace std;

const int N = 20010;
vector<int> e[N];
int n, m, res;
bool cnt[N];
int dfn[N], low[N], tot;
void tarjan(int u, int top) { // top：祖先根节点
    dfn[u] = low[u] = ++tot;
    int child = 0;
    for (auto v : e[u]) {
        if (!dfn[v]) { // 表示没有访问
            tarjan(v, top);
            low[u] = min(low[u], low[v]); // 通过子节点能够到达更上的位置
            if (low[v] >= dfn[u] && u != top) { // if(u!=top),可能v的父节点是u，并且u就v一个孩子，u就不是割点
                cnt[u] = true;
            }
            if (u == top) child++;
        } else {
            low[u] = min(low[u], dfn[v]); // 此时v是u的祖先节点,dfn[v]<=dfn[u]
        }
    }
    if (child >= 2 && top == u) cnt[u] = true; // 判断根节点
}
void solve() {
    cin >> n >> m;
    for (int i = 1; i <= m; i++) {
        int x, y;
        cin >> x >> y;
        e[x].push_back(y);
        e[y].push_back(x);
    }
    for (int i = 1; i <= n; i++) if (!dfn[i]) tarjan(i, i); //图可能不连通
    for (int i = 1; i <= n; i++) if (cnt[i]) res++;
    cout << res << endl;
    for (int i = 1; i <= n; i++) {
        if (cnt[i])  {cout << i << " ";}
    }
    cout << endl;
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    solve();
    return 0;
}

// 判断割点所割的连通分量数
// 1)设 cut[i] 表示割点 i 所割连通分量数。
// 2)对于根节点的割点，显而易见的，它的孩子数就是所割的连通分量数，故 cut[i]=child[i] 。
// 3) 对于非根节点的割点，它所割的连通分量数为其满足条件 low[v]>=dfn[u] 的孩子数+1。因为其与父节点的连通，也会因为割点的去除而失去。