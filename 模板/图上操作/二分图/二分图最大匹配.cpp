// https://www.luogu.com.cn/problem/P3386
// 卡片 https://www.luogu.com.cn/problem/P2065 

#include <bits/stdc++.h>

using namespace std;
const int M = 100010;
const int N = 110;
// 匈牙利算法，男女匹配，男选女，可占可让，贪心匹配 因为边权为1
// 时间复杂度：O(n*m)
int n, m, k, res;
int e[M], ne[M];
int h[N], idx;
bool vis[N]; // 存女生是否被访问
int match[N];

void add(int u, int v) {
    e[++idx] = v;
    ne[idx] = h[u];
    h[u] = idx;
}

bool dfs(int u) {
    for (int i = h[u]; i; i = ne[i]) {
        int v = e[i];
        if (vis[v]) continue;
        vis[v] = 1; // 每个v只需要访问一次即可
        if (!match[v] || dfs(match[v])) {
            match[v] = u;
            return 1;
        }
    }
    return 0;
}

void solve() {
    cin >> n >> m >> k;
    for (int i = 0; i < k; i++) {
        int a, b;
        cin >> a >> b;
        add(a, b);
    }
    for (int i = 1; i <= n; i++) { //时间复杂度：O(n*m)
        memset(vis, 0, sizeof(vis));
        if (dfs(i)) res++;
    }
    cout << res << endl;
}
