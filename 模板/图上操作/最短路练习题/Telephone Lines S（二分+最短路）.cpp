// Telephone Lines S https://www.luogu.com.cn/problem/P1948
// 解题思路：
// 观察题目，费用具有二段性，当最优解为res,x<=res的费用都满足条件，当x>res的费用都不满足条件。
// 如何check，>md的边权为1，<=md的边权为0，01单源最短路，查看dis[T]<=k,满足，则表示可以使用<=k的边，可以到达终点。
#include <bits/stdc++.h>
using namespace std;
using pii = pair<int, int>;
#define fi first
#define se second
#define mid ((l+r)/2)
#define mp make_pair
const int N = 100010;
int n, p, k;
vector<pii> e[N];
int dis[N];
int S, T;
bool check(int md) {
    priority_queue<pii, vector<pii>, greater<pii>> pq;
    memset(dis, 0x3f, sizeof(dis));
    dis[S] = 0;
    pq.push(mp(0, S));
    while (pq.size()) {
        auto p = pq.top();
        pq.pop();
        int uw = p.fi, u = p.se;
        if (uw > dis[u]) continue;
        if (u == T) return uw <= k;
        for (auto t : e[u]) {
            int v = t.fi, w = (t.se > md ? 1 : 0) + uw;
            if (dis[v] > w) {
                dis[v] = w;
                pq.push({w, v});
            }
        }
    }
    // cout<<dis[T]<<endl;
    return dis[T] <= k;
}
void solve() {
    cin >> n >> p >> k;
    S = 1, T = n;
    for (int i = 1; i <= p; i++) {
        int x, y, w;
        cin >> x >> y >> w;
        e[x].push_back({y, w});
        e[y].push_back({x, w});
    }
    bool flag = false;
    int l = -1, r = 1000010;
    while (l + 1 < r) {
        if (check(mid)) {r = mid; flag = true;}
        else l = mid;
    }
    if (flag) cout << r << endl;
    else cout << -1 << endl;
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    solve();
    return 0;
}