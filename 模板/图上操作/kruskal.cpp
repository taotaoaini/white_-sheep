#include<bits/stdc++.h>
using namespace std;
using i64 =long long;
const i64 dinf=0x3f3f3f3f3f3f3f3f;
// 1.krusal算法
// 对片进行排序,依次添加最短的边到集合中
// 判断圈,处理连通性问题,并查集简单高效
struct Solution1{
    vector<array<int,3>> e;
    vector<int> fa;
     int n,m;// n:点,m:边
    bool cmp(const array<int,3>& a,const array<int,3>& b){
        return a[2]<b[2];
    }
    int find(int x){
        while(x!=fa[x]) x=fa[x]=fa[fa[x]];
        return x;
    }
    i64 kruskal(){
            i64 res=0;
            for(int i=1;i<=n;i++) fa[i]=0;
            sort(e.begin()+1,e.end(),cmp);
            for(int i=1;i<=m;i++){
                int fu=find(e[i][0]);
                int fv=find(e[i][1]);
                int w=e[i][2];
                if(fu==fv) continue;
                fa[fu]=fv;
                res+=w;
            }
            return res;
    }
    void sovle1(){
        cin>>n>>m;
        e.resize(m+1);
        fa.resize(n+1);
        for(int i=1;i<=m;i++){
            cin>>e[i][0]>>e[i][1]>>e[i][2];
        }
        i64 res=kruskal();
        cout<<res<<endl;
    }
};