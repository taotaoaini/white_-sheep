#include <bits/stdc++.h>
using namespace std;
// (1)求给定坐标集合间最大曼哈顿距离
// 对于(x1,y1),(x2,y2) 它们的曼哈顿距离为：∣ x1 − x2 ∣ + ∣ y1 − y2 ∣
// 给定一个包含若干个点的集合，求其中任意两点之间的最大/最小曼哈顿距离；

// ∣ x1 − x2 ∣ + ∣ y1 − y2 ∣
// 简单推一下就知道了
// |x1+y1-(x2+y2)|
// |x1-y1-(x2-y2)|
struct Solution1{
    int maxDistance(vector<vector<int>>& points) { //(x,y时间复杂度：O（nlogn）
        int n = points.size();
        vector<int> b;
        vector<int> c;
        for (int i = 0; i < n; i++) { //存坐标
            b.push_back(points[i][0] - points[i][1]);
            c.push_back(points[i][0] + points[i][1]);
        }
        sort(b.begin(), b.end());
        sort(c.begin(), c.end());
        return max(b[n-1]-b[0],c[n-1]-c[0]);
    }
};