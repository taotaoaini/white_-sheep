// 关键路径 http://codeup.hustoj.com/problem.php?cid=100000624&pid=0
// 网的源点是入度为0的顶点，汇点是出度为0的顶点。网的关键路径是指从源点到汇点的所有路径中，具有最大路径长度的路径。
// 根据给出的网的邻接矩阵求该网的关键路径及其长度。
// 第一个输出是图的关键路径（用给出的字母表示顶点， 用括号将边括起来，顶点逗号相隔）
// 第二个输出是关键路径的长度
#include <bits/stdc++.h>
using namespace std;
const int N = 20;
int n, m;
string s;
int e[N][N];
int in[N];
int a[N]; // in的副本
int out[N];
int f[N];
int pre[N];
int ans[2];
void init() {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            e[i][j] = 0;
        }
    }
    for (int i = 0; i < n; i++) {
        in[i] = out[i] = pre[i] = a[i] = f[i] = 0;
    }
    ans[0] = ans[1] = 0;
}
void bfs() {
    queue<int> q;
    for (int i = 0; i < n; i++) {
        if (a[i] == 0) {
            f[i] = 0;
            q.push(i);
        }
    }
    while (q.size()) {
        auto p = q.front();
        q.pop();
        if (!out[p]) {
            if (ans[0] < f[p]) {
                ans[0] = f[p], ans[1] = p;
            }
            continue;
        }
        for (int i = 0; i < n; i++) {
            if (!e[p][i] || p == i) continue;
            if (--a[i] == 0) q.push(i);
            if (f[i] < f[p] + e[p][i]) {
                f[i] = f[p] + e[p][i];
                pre[i] = p;
            }
        }
    }
}
void solve() {
    cin >> n >> m;
    cin >> s;
    for (int i = 1; i <= m; i++) {
        char x, y;
        int w;
        cin >> x >> y >> w;
        int idx = s.find(x);
        int idy = s.find(y);
        e[idx][idy] = w;
        out[idx] += 1;
        in[idy] += 1;
    }
    for (int i = 0; i < n; i++) a[i] = in[i];
    bfs();
    vector<char> tmp;
    int x = ans[1];
    while (in[x] != 0) {
        tmp.push_back(s[x]);
        x = pre[x];
    }
    tmp.push_back(s[x]);
    reverse(tmp.begin(), tmp.end());
    for (int i = 0; i < tmp.size() - 1; i++) {
        cout << "(" << tmp[i] << "," << tmp[i + 1] << ") ";
    }
    cout << ans[0] << endl;
    init();
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    int t = 1;
    cin >> t;
    while (t--) {
        solve();
    }
    return 0;
}