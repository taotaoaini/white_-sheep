// 炸铁路 https://www.luogu.com.cn/problem/P1656
// 无向图
// 时间复杂度：O(n+m)
// 输出割边点对 <a,b>,(a<b)
#include<bits/stdc++.h>

using namespace std;
const int N = 210;
const int M = 10010;
int n, m, cnt;
struct edge {
    int u, v;
};
vector<edge> e; //边集数组
vector<int> h[N]; //头节点
int dfn[N], low[N], tot;

struct bridge { //桥
    int x, y;

    bool operator<(const bridge &tmp) const {
        if (x == tmp.x) return y < tmp.y;
        return x < tmp.x;
    }
} br[M];

void add(int a, int b) { // 添加边
    e.push_back({a, b});
    h[a].push_back(e.size() - 1);
}

void tarjan(int u, int in) {
    dfn[u] = low[u] = ++tot;
    for (int i = 0; i < h[u].size(); i++) {
        int j = h[u][i], v = e[j].v;
        if (!dfn[v]) {
            tarjan(v, j);
            low[u] = min(low[u], low[v]);
            if (low[v] > dfn[u]) { //注意与割点的不同处
                if (u < v)
                    br[cnt++] = {u, v};
                else
                    br[cnt++] = {v, u};
            }
        } else if (j != (in ^ 1)) { // 判断不能为反边
            low[u] = min(low[u], dfn[v]);
        }
    }
}

void solve() {
    cin >> n >> m;
    for (int i = 1; i <= m; i++) {
        int a, b;
        cin >> a >> b;
        add(a, b);
        add(b, a);
    }
    for (int i = 1; i <= n; i++) {
        if (!dfn[i]) tarjan(i, 0);
    }
    sort(br, br + cnt);
    for (int i = 0; i < cnt; i++)
        cout << br[i].x << " " << br[i].y << endl;
}

int main() {
    solve();
    return 0;
}