// eDcc  Cactus https://codeforces.com/contest/231/problem/E
// https://blog.csdn.net/kalilili/article/details/46975253
// https://codeforces.com/contest/412/problem/D

// 我们把每一个e-DCC都看成一个节点，
// 把所有桥边(x,y)看成连接编号为c[x]和c[y]的两个e-DCC间的边，这样我们就会得到一棵树或者森林(原图不连通)。
#include<bits/stdc++.h>
using namespace std;
#define endl '\n'
const int N = 100010;
const int M = 200010;
const int mod = 1e9 + 7;
int n, m, q;
struct edge {
    int v, ne;
} e[M];
int h[N], idx = 1; // 从2开始配对，判断正反边
int dfn[N], low[N], tot;
int dcc[N], cnt, d[N]; //
int br[M];
int dep[N];
stack<int> st;
vector<int> g[N]; //g从下标一开始，有大量的重边需要注意
int fa[N][20];
int dp[N];
bool vis[N]; //
void add(int u, int v) {
    e[++idx].v = v;
    e[idx].ne = h[u];
    h[u] = idx;
}
 void tarjan(int u, int in_edge) {
     dfn[u] = low[u] = ++tot;
     st.push(u);
     for (int i = h[u]; i; i = e[i].ne) {
         int v = e[i].v;
         if (!dfn[v]) {
             tarjan(v, i);
             low[u] = min(low[u], low[v]);
             if (low[v] > dfn[u]) br[i] = br[i ^ 1] = true;
         } else if (i != (in_edge ^ 1)) { // 不是反向边
             low[u] = min(low[u], dfn[v]);
         }
     }
     if (dfn[u] == low[u]) { // u下面的节点
         ++cnt;
         while (1) {
             int x = st.top(); st.pop();
             dcc[x] = cnt; //记录eDCC
             d[cnt] += 1;
             if (x == u) break;
         }
     }
 }
void rebuild() { // 重建边
    for (int i = 2; i <= idx; i++) {
        int x = e[i].v;
        int y = e[i ^ 1].v;
        if (dcc[x] == dcc[y]) continue;
        g[dcc[x]].push_back(dcc[y]);
    }
}
void dfs(int u, int f) {
    fa[u][0] = f;
    dep[u] = dep[f] + 1;
    for (int i = 1; i < 20; i++) {
        fa[u][i] = fa[fa[u][i - 1]][i - 1];
    }
    if (d[u] >= 2) dp[u] += 1; // 只有缩点可以制造出两条路径,计算两点间的缩点个数
    vis[u] = true;
    for (auto v : g[u]) {
        if (vis[v]) continue;
        dp[v] += dp[u];
        dfs(v, u);
    }
}
int lca(int u, int v) {
    if (dep[u] < dep[v]) swap(u, v);
    for (int i = 19; i >= 0; i--) {
        if (dep[fa[u][i]] >= dep[v]) u = fa[u][i];
    }
    if (v == u) return u;
    for (int i = 19; i >= 0; i--) {
        if (fa[u][i] != fa[v][i]) u = fa[u][i], v = fa[v][i];
    }
    return fa[u][0];
}
int pw[N];
void solve() {
    cin >> n >> m;
    pw[0] = 1;
    for (int i = 1; i < N; i++) pw[i] = pw[i - 1] * 2 % mod;
    for (int i = 1; i <= m; i++) {
        int x, y;
        cin >> x >> y;
        add(x, y);
        add(y, x);
    }
    for (int i = 1; i <= n; i++) if (!dfn[i]) tarjan(i, 0);
    rebuild();
    dfs(1, 0);
    cin >> q;
    for (int i = 1; i <= q; i++) {
        int x, y;
        cin >> x >> y;
        x = dcc[x], y = dcc[y];
        if (x == y) {
            cout << 2 << endl;
            continue;
        }
        int cc = lca(x, y);
        int t = dp[x] + dp[y] - 2 * dp[cc] + (d[cc] >= 2 ? 1 : 0);
        cout << pw[t] << endl;
    }
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}