#include <bits/stdc++.h>
using  namespace std;
const int inf=0x3f3f3f3f;
// 1.图的规模小（n<=200），用Floyd，如果边的权值有负的，可以判断负圈
// e[i][i]设置为inf,e[i][i]：表示i到外面绕一圈回来的最小路径，可以判断负圈
// 环路上的权值之和为负数，每走一次负圈，总权值都会减少。
// e[i][i]<0 :存在负圈
struct Solution1{
    vector<vector<int>> e;
    int n=100,m=100;
    void floyd(){
        int s=1;
        for(int k=1;k<=n;k++){
            for(int i=1;i<=n;i++){
                if(e[i][k]!=inf){ //小优化
                    for(int j=1;j<=n;j++){
                        if(e[i][j]>e[i][k]+e[k][j]){ // 不使用min，小优化
                            e[i][j]=e[i][k]+e[k][j];
                        }
                    }
                }
            }
        }
    }
    void solve1(){
        e.assign(n+1,vector<int>(n+1));
        for(int i=1;i<=n;i++){
            for(int j=1;j<=n;j++){
                e[i][j]=inf;
            }
        }
        for(int i=1,x,y,w;i<=m;i++){
            cin>>x>>y>>w;
            e[x][y]=e[y][x]=w;
        }
        floyd();
        for(int i=1;i<=n;i++){ //判断是否存在负圈
            if(e[i][i]<0) {cout<<"存在负圈"<<endl;}
        }
    }
};
