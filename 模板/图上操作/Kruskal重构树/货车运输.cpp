// 货车运输 https://www.luogu.com.cn/problem/P1967
// https://www.cnblogs.com/ACMSN/p/10646306.html
// https://zhuanlan.zhihu.com/p/620736214
// https://www.cnblogs.com/ruierqwq/p/kruskal-reconstruction-tree.html
// 无向图，每条边都有权值，求两点间所有路径的最小权值的最大值
// kruskal最大重构树的模板题
// x,y两点的LCA的点权就是x,y路径上的瓶颈
#include<bits/stdc++.h>
using namespace std;
#define endl '\n'
using i64 = long long;
const int N=2e5+10;
const int LOGN=20;
int n,m,q,now,dep[N],fa[N][LOGN],val[N];
struct Edge {
    int u,v,w;
    Edge(int x=0,int y=0,int z=0): u(x),v(y),w(z) {}
} g[N];
vector<int> e[N];
struct Dsu {
    int fa[N];
    void init(int x) {
        for(int i=1; i<=x; i++) fa[i] = i;
    }
    int find(int x) {
        while(x!=fa[x]) x=fa[x]=fa[fa[x]];
        return x;
    }
    bool merge(int x, int y) {
        int u = find(x), v = find(y);
        if(u == v) return 0;
        fa[u] = v;
        return 1;
    }
} dsu;
void kruskal() {
    dsu.init(2*n-1);
    sort(g+1, g+1+m, [](const Edge& x, const Edge& y) {
        return x.w > y.w;
    });
    now = n;
    for(int i=1; i<=m; i++) {
        int u = dsu.find(g[i].u), v = dsu.find(g[i].v), w = g[i].w;
        if(u != v) {
            val[++now] = w;
            dsu.merge(u, now);
            dsu.merge(v, now);
            e[now].push_back(u);
            e[now].push_back(v);
        }
    }
}
void dfs(int u,int f) {
    dep[u]=dep[f]+1;
    fa[u][0]=f;
    for(int i=1; i<LOGN; i++) {
        fa[u][i]=fa[fa[u][i-1]][i-1];
    }
    for(int v:e[u]) {
        if(v==f) continue; //没有连向父亲的边，可以不写
        dfs(v,u);
    }
}
int lca(int u, int v) {
    if(dep[u] < dep[v]) swap(u, v);
    for(int i=LOGN-1; i>=0; i--)if(dep[fa[u][i]] >= dep[v]) u = fa[u][i];
    if(u == v) return u;
    for(int i=LOGN-1; i>=0; i--) if(fa[u][i] != fa[v][i]) u = fa[u][i], v = fa[v][i];
    return fa[u][0];
}
void solve() {
    cin>>n>>m;
    for(int i=1; i<=m; i++) {
        int u,v,w;
        cin>>u>>v>>w;
        g[i]=Edge(u,v,w);
    }
    kruskal();
    for(int i=now; i>=1; i--) { //可能是一个森林 ,now为新的节点总数,必须从后往前遍历否则每棵子树都需要遍历一遍了
        if(!dep[i]) dfs(i,0);  //最小的节点在根节点，所以根节点分布在最后的
    }
    cin>>q;
    for(int i=1; i<=q; i++) {
        int u,v;
        cin>>u>>v;
        int c=lca(u,v);

        if(!c) cout<<-1<<endl; //表示不连通
        else cout<<val[c]<<endl;
    }
}
int main() {
    ios::sync_with_stdio(false);cin.tie(0);
    solve();
    return 0;
}
