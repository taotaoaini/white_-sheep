// 冗余连接 https://leetcode.cn/problems/redundant-connection/description/
#include <bits/stdc++.h>

using namespace std;

vector<int> findRedundantConnection(vector<vector<int>> &edges) {
    int n = edges.size();
    vector<int> fa(n + 1);
    for (int i = 1; i <= n; i++) fa[i] = i;
    auto find = [&](int x) {
        while (x != fa[x]) x = fa[x] = fa[fa[x]];
        return x;
    };
    int res = -1;
    for (int i = 0; i < edges.size(); i++) {
        int fax = find(edges[i][0]);
        int fay = find(edges[i][1]);
        if (fax == fay) {
            res = i;
        } else {
            fa[fax] .= fay;
        }
    }
    return edges[res];
}