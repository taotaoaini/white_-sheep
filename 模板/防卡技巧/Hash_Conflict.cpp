#include <iostream>
#include <cstdint>        // 用于定义固定大小的整数类型（例如 uint64_t）
#include <utility>        // 用于定义 pair
#include <chrono>
#include<unordered_map>

using namespace std;

//cf卡哈希，处理方案
struct myhash {//重写key
    static uint64_t hash(uint64_t x) {
        x += 0x9e3779b97f4a7c15;
        x = (x ^ (x >> 30)) * 0xbf58476d1ce4e5b9;
        x = (x ^ (x >> 27)) * 0x94d049bb133111eb;
        return x ^ (x >> 31);
    }

    size_t operator()(uint64_t x) const {
        static const uint64_t SEED = chrono::steady_clock::now().time_since_epoch().count();
        return hash(x + SEED);
    }

    size_t operator()(pair<uint64_t, uint64_t> x) const {
        static const uint64_t SEED = chrono::steady_clock::now().time_since_epoch().count();
        return hash(x.first + SEED) ^ (hash(x.second + SEED) >> 1);
    }
};

int main() {
    unordered_map<uint64_t, int, myhash> mp;
    mp[110] = 10;
    cout << mp[110] << endl;
}