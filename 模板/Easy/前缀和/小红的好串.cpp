// 小红的好串  https://ac.nowcoder.com/acm/contest/80742/D
#include <bits/stdc++.h>

using namespace std;

#define endl '\n'
#define fi first
#define se second

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
const int N = 100010;
int n, q;
string s;
int a[3][N];
const int inf = 0x3f3f3f3f;
int f(int l, int r, int x, int y) {
    int n = r - l + 1;
    int z = n - x - y;
    int c1 = x - (a[0][l + x - 1] - a[0][l - 1]);
    int c2 = y - (a[1][l + x + y - 1] - a[1][l + x - 1]);
    int c3 = z - (a[2][r] - a[2][r - z]);
    return c1 + c2 + c3;
}
void solve() {
    cin >> n >> q;
    cin >> s;
    for (int i = 1; i <= n; i++) {
        a[0][i] = (s[i - 1] == 'r');
        a[1][i] = (s[i - 1] == 'e');
        a[2][i] = (s[i - 1] == 'd');
    }
    for (int i = 1; i <= n; i++) {
        a[0][i] += a[0][i - 1];
        a[1][i] += a[1][i - 1];
        a[2][i] += a[2][i - 1];
    }
    while (q--) {
        int l, r;
        cin >> l >> r;
        int len = r - l + 1;
        if (len < 3) {
            cout << 0 << endl;
            continue;
        }
        int x = len / 3;
        int t = len % 3;
        int res = inf;
        if (t == 0) {
            res = min(res, f(l, r, x, x));
        } else if (t == 1) {
            res = min(res, f(l, r, x, x));
            res = min(res, f(l, r, x + 1, x));
            res = min(res, f(l, r, x, x + 1));
        } else {
            res = min(res, f(l, r, x + 1, x + 1));
            res = min(res, f(l, r, x + 1, x));
            res = min(res, f(l, r, x, x + 1));
        }
        cout << res << endl;
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    solve();
    return 0;
}