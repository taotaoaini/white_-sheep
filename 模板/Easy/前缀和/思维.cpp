#include<bits/stdc++.h>
using namespace std;

//（1）前缀和的前缀和 巫师的总力量和 https://leetcode.cn/problems/sum-of-total-strength-of-wizards/description/
// 单调栈 + 前缀和
// 规定： left[i] 为左侧严格小于 strength[i] 的最近元素位置（不存在时为 -1）
//       right[i] 为右侧小于等于 strength[i] 的最近元素位置（不存在时为 n）

// （2） 壁画 https://acwing.com/problem/content/564/
// 纯前缀和题目，但有点技巧在里面，就是最多选择(n+1)/2段壁画，从两边摧毁