#include<bits/stdc++.h>
using namespace std;
const int N = 30010;
int fa[N];
int sz[N];
int dis[N];
int n;
int find(int x) { // 不能直接状态压缩
    if (fa[x] == x) return x;
    int fax = find(fa[x]);
    dis[x] += dis[fa[x]]; //在状态压缩的过程中累加
    return fa[x] = fax;
}
void init() {
    for (int i = 0; i < N; i++) {
        fa[i] = i;
        sz[i] = 1;
    }
}
void solve() {
    cin >> n;
    init();
    while (n--) {
        char op;
        int x, y;
        cin >> op;
        cin >> x >> y;
        int fax = find(x);
        int fay = find(y);
        if (op == 'M') {
            if (fax == fay) continue;
            dis[fax] += sz[fay]; // 更新距离
            sz[fay] += sz[fax]; // 更新集合大小
            fa[fax] = fay; // 更新集合
        } else {
            if (fax != fay) cout << -1 << endl;
            else {
                cout << abs(dis[x] - dis[y]) - 1 << endl; // 前缀和思想
            }
        }
    }
}

int main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}