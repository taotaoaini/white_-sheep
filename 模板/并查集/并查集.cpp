#include<bits/stdc++.h>

using namespace std;
//【問題】
// (1)使某些点不在环上需要删除的最少边数 https://ac.nowcoder.com/acm/contest/7780/C



//預頭
#define fi first
#define se second
using pii = pair<int, int>;

//(1)使某些点不在环上需要删除的最少边数 https://ac.nowcoder.com/acm/contest/7780/C
//采用并查集将编号都大于K的边进行合并，这样相当于将一些无关的边进行缩点，然后再进行一次并查集，找到剩余成环的边。
struct Solution1 {
    void solve1() {
        int n, m, k;
        cin >> n >> m >> k;
        vector<int> fa(n + 1);
        for (int i = 1; i <= n; i++) fa[i] = i;
        auto find = [&](int x) -> int {
            while (x != fa[x]) x = fa[x] = fa[fa[x]];
            return x;
        };
        vector<pii> e;
        for (int i = 1, x, y; i <= m; i++) {
            cin >> x >> y;
            if (x > k && y > k) { //直接合并，可以將huan縮點
                int fax = find(x);
                int fay = find(y);
                fa[fax] = fay;
            } else {
                e.emplace_back(x, y);
            }
        }
        int res = 0;
        for (int i = 0; i < e.size(); i++) {
            int x = e[i].fi, y = e[i].se;
            int fax = find(x);
            int fay = find(y);
            if (fax == fay) res++;
            else fa[fax] = fay;
        }
        cout << res << endl;
    }
};