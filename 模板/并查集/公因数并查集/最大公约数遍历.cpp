// 最大公约数遍历 https://leetcode.cn/problems/greatest-common-divisor-traversal/description/

#include<bits/stdc++.h>
using namespace std;
#define MAXX ((int) 1e5)
bool inited = false;
vector<int> fac[MAXX + 10];

// 全局预处理每个数的质因数
void init() {
    if (inited) return;
    inited = true;

    for (int i = 2; i <= MAXX; i++) if (fac[i].empty()) for (int j = i; j <= MAXX; j += i) fac[j].push_back(i);
}

class Solution {
public:
    bool canTraverseAllPairs(vector<int>& nums) {
        init();

        int n = nums.size();
        int mx = 0;
        for (int x : nums) mx = max(mx, x);

        // 初始化并查集
        vector<int> fa(n+mx+1);
        for (int i = 0; i <= n + mx; i++) fa[i] = i;

        // 查询并查集的根
        function<int(int)> find = [&](int x) {
            if (fa[x] != x) fa[x] = find(fa[x]);
            return fa[x];
        };

        // 对每个 nums[i]，向它们的质因数连边
        for (int i = 0; i < n; i++)
            for (int p : fac[nums[i]]) {
                int x = find(i), y = find(n + p); // 将质数p映射到n+p
                if (x == y) continue;
                fa[x] = y;
            }

        // 检查是否所有位置点都在同一连通块内
        unordered_set<int> st;
        for (int i = 0; i < n; i++) st.insert(find(i));
        return st.size() == 1;
    }
};