// 包含每个查询的最小区间 https://leetcode.cn/problems/minimum-interval-to-include-each-query/description/
#include<bits/stdc++.h>

using namespace std;

vector<int> minInterval(vector<vector<int>> &intervals, vector<int> &queries) {
    // 按照区间长度由小到大排序
    sort(intervals.begin(), intervals.end(), [](vector<int> &a, vector<int> &b) {
        return a[1] - a[0] < b[1] - b[0];
    });

    int m = queries.size();
    vector<pair<int, int>> qs(m);
    for (int i = 0; i < m; i++) {
        qs[i] = {queries[i], i};
    }
    // 离线：按查询位置排序
    sort(qs.begin(), qs.end(), [](const pair<int, int> &a, const pair<int, int> &b) {
        return a.first < b.first;
    });

    // 初始化并查集
    vector<int> fa(m + 1);
    for (int i = 0; i <= m; i++) {
        fa[i] = i;
    }
    function<int(int)> find = [&](int x) {
        if (fa[x] != x) fa[x] = find(fa[x]);
        return fa[x];
    };

    vector<int> ans(m, -1);
    // 对每个区间，回答所有在 [l,r] 范围内的询问
    for (auto &p: intervals) {
        int l = p[0];
        int r = p[1];
        int length = r - l + 1;
        // 二分找大于等于区间左端点的最小询问
        int i = lower_bound(qs.begin(), qs.end(), l, [](const auto &a, int b) {
            return a.first < b;
        }) - qs.begin();
        // 回答所有询问位置在 [l,r] 范围内的还没有被回答过的询问
        for (i = find(i); i < m && qs[i].first <= r; i = find(i + 1)) {
            ans[qs[i].second] = length;
            fa[i] = i + 1;
        }
    }
    return ans;
}