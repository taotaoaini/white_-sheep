
#include <bits/stdc++.h>
using namespace std;
#define fi first
#define se second
#define mid ((l+r)/2)
using pii = pair<int,int>;
using i64 = long long;
const int N = 500010;

struct DSU {
    int tot = 0, fa[N], sz[N], s[N];

    void init(int n) {
        for (int i = 0; i <= n; i++) { // 初始化
            fa[i] = i;
            sz[i] = 1;
        }
    }

    int find(int x) { // 即find查找函数
        while (x != fa[x]) x = fa[x];
        return x;
    }

    void merge(int u, int v) { // 合并函数
        int x = find(u);
        int y = find(v);
        if (x == y) return;
        if (sz[x] > sz[y]) swap(x, y);
        s[++tot] = x, fa[x] = y, sz[y] += sz[x];
    }

    void dodel() {// 删除栈顶边
        if (!tot) return;
        int x = s[tot--];
        sz[fa[x]] -= sz[x], fa[x] = x;
    }

    void back(int t) { //删除到只剩t条边
        while (tot > t) dodel();
    }
}d;

int n;
vector<pii> e[N];
i64 res;
void query(int l,int r){
    int t=d.tot;
    if(l==r){
        for(auto edge:e[l]){
            int x=edge.fi;
            int y=edge.se;
            int fx=d.find(x);
            int fy=d.find(y);
            res+=d.sz[fx]+d.sz[fy];
        }
        return;
    }
    for(int i=mid+1;i<=r;i++){
        for(auto edge:e[i]){
            int x=edge.fi;
            int y=edge.se;
            d.merge(x,y);
        }
    }
    query(l,mid);
    d.back(t);
    for(int i=l;i<=mid;i++){
        for(auto edge:e[i]){
            int x=edge.fi;
            int y=edge.se;
            d.merge(x,y);
        }
    }
    query(mid+1,r);
}
int main(){
    cin>>n;
    for(int i=1;i<=n;i++){
        int a,b,c;
        cin>>a>>b>>c;
        e[c].push_back({a,b});
    }
    query(1,n);
    cout<<res<<endl;
}