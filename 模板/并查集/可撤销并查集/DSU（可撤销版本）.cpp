// 教程 https://www.cnblogs.com/zac2010/p/17985517
// 并查集只可以加边而不可以删边
// 可撤销并查集只可以按照加入的时间从后到前撤销加边操作。
// 常用于一些连通性问题，并且很多时候会调整处理的顺序，只要按照上述撤销顺序撤销边即可。
//路径压缩会改变树的形态，所以只用按秩合并（按子树大小合并，小的那颗合并到大的上面去），单次时间复杂度为O(logn)级别
// 因为对于每个节点来说，深度最多是logn级别的。

// todo 习题
// 可持久化并查集 https://www.luogu.com.cn/problem/P3402 离线做法

#include <bits/stdc++.h>
using namespace std;
const int N = 100010;

struct DSU {
    int tot = 0, fa[N], sz[N], s[N];

    void init(int n) {
        for (int i = 0; i <= n; i++) { // 初始化
            fa[i] = i;
            sz[i] = 1;
        }
    }

    int find(int x) { // 即find查找函数
        while (x != fa[x]) x = fa[x];
        return x;
    }

    void merge(int u, int v) { // 合并函数
        int x = find(u);
        int y = find(v);
        if (x == y) return;
        if (sz[x] > sz[y]) swap(x, y);
        s[++tot] = x, fa[x] = y, sz[y] += sz[x];
    }

    void dodel() {// 删除栈顶边
        if (!tot) return;
        int x = s[tot--];
        sz[fa[x]] -= sz[x], fa[x] = x;
    }

    void back(int t) { //删除到只剩t条边
        while (tot > t) dodel();
    }
};