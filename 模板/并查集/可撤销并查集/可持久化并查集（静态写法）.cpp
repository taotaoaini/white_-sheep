// 可持久化并查集 https://www.luogu.com.cn/problem/P3402 离线做法
// 我们用可撤销并查集离线玩。直接做很难，因为撤销操作可以撤销之前的撤销操作，这样很可能时嵌套的
// 可以将操作建成一棵树，如果是第i种操作，那么由i-1连向i，表示i-1执行完后执行i
// 如果是第2种操作，那么把k连向i，表示i在k之后，撤销i和k中间的，跳过中间的操作
// 这样建图之后我们发现与一个节点的操作相关的是祖先到他这条路径上的操作。
// 那么一遍深度优先搜索，到了每个节点操作一下了，然后继续递归，返回时去掉操作（即撤销）。
#include<bits/stdc++.h>

using namespace std;
const int N = 100010;
const int M = 200010;

struct DSU {
    int tot = 0, fa[N], sz[N], s[N];

    void init(int n) {
        for (int i = 0; i <= n; i++) { // 初始化
            fa[i] = i;
            sz[i] = 1;
        }
    }

    int find(int x) { // 即find查找函数
        while (x != fa[x]) x = fa[x];
        return x;
    }

    void merge(int u, int v) { // 合并函数
        int x = find(u);
        int y = find(v);
        if (x == y) return;
        if (sz[x] > sz[y]) swap(x, y);
        s[++tot] = x, fa[x] = y, sz[y] += sz[x];
    }

    void dodel() {// 删除栈顶边
        if (!tot) return;
        int x = s[tot--];
        sz[fa[x]] -= sz[x], fa[x] = x;
    }

    void back(int t) { // 删除到只剩t条边
        while (tot > t) dodel();
    }
}d;

int n, m;
vector<int> e[M];
struct Q {
    int op;
    int x, y;
} q[M];
int ans[M];
void add(int u, int v) {
    e[u].push_back(v);
}
void dfs(int u) {
    int t = d.tot;
    auto p = q[u];
    if (p.op == 1) d.merge(p.x, p.y);
    if (p.op == 3) ans[u] = (d.find(p.x) == d.find(p.y));
    for (auto v : e[u]) {
        dfs(v);
    }
    d.back(t);
}
void solve() {
    cin >> n >> m;
    d.init(n);
    for (int i = 1; i <= m; i++) {
        auto &p = q[i];
        cin >> p.op;
        if (p.op == 1 || p.op == 3) {
            cin >> p.x >> p.y;
            add(i - 1, i);
        } else {
            cin >> p.x;
            add(p.x, i);
        }
    }
    dfs(0);
    for (int i = 1; i <= m; i++) {
        if (q[i].op == 3) cout << ans[i] << endl;
    }
}
int main(){
    ios::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    solve();
    return 0;
}