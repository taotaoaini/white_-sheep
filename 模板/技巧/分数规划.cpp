#include <bits/stdc++.h>

using namespace std;
//  1.有n(n<=1000)个物品，每个物品有两个权值a(1<=1e9)和b(1<=1e9)，你可以放弃k(0<=k<n)个物品，选n-k个物品，使得sum(a)/sum(b)最大
// 输出答案乘100后四舍五入到整数的值
// 2. alent Show G https://www.luogu.com.cn/problem/P4377
// 至少型背包+分数规划

//1.分数规划+二分+排序 复杂度：nlogn*log(1e4)
const int N = 1010;
int n, k;
double a[N], b[N], c[N];

bool check(double x) {
    double s = 0;
    for (int i = 1; i <= n; ++i)c[i] = a[i] - x * b[i];
    sort(c + 1, c + n + 1);
    for (int i = k + 1; i <= n; ++i) s += c[i];
    return s >= 0;
}

double find() {
    double l = 0, r = 1;
    while (r - l > 1e-4) {
        double mid = (l + r) / 2;
        if (check(mid)) l = mid;//最大化
        else r = mid;
    }
    return l;
}

void solve() {
    while (scanf("%d%d", &n, &k), n) {
        for (int i = 1; i <= n; i++)scanf("%lf", &a[i]);
        for (int i = 1; i <= n; i++)scanf("%lf", &b[i]);
        printf("%.0lf\n", 100 * find());
    }
}


// 最少型背包
// 好题
struct Solution2 {
    using i64 = long long;
    vector<int> w,t;
    int n,m;
    bool check(double md){
        vector<double> a(n+1);
        for(int i=1;i<=n;i++) a[i]=t[i]-md*(i64)w[i];
        vector<double> f(m+10,-1e5); //不可达状态
        f[0]=0;
        for(int i=1;i<=n;i++){
            for(int j=m;j>=0;j--){
                int v=min(j+w[i],m);  //统一将大于等于m的，处理成m，不影响答案
                f[v]=max(f[v],f[j]+a[i]);
            }
        }
        return f[m]>=0;
    }
    void solve() {
        cin>>n>>m;
        w.assign(n+10,0);
        t.assign(n+10,0);
        for(int i=1;i<=n;i++) cin>>w[i]>>t[i];
        double l=0,r=3010;
        while(r-l>1e-5){
            double mid=(l+r)/2;
            if(check(mid)) l=mid;
            else r=mid;
        }
        int res=r*1000;
        cout<<res<<endl;
    }

};