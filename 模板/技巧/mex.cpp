// 数组中未出现的最小非负整数mex

// 内存只需要开到数组大小就行
// 时间复杂度：O(logn)
#include<bits/stdc++.h>
using namespace std;
const int N=100010;
int cnt[N];
set<int> st;
multiset<int> mst;

void init(int n){
    for(int i=0;i<=n;i++){
        cnt[i]=0;
        st.insert(i);
    }
}
void add(int x){
    // 第一次添加
    if(cnt[x]==0){
        st.erase(x);
    }
    ++cnt[x];
    mst.insert(x);
}
void del(int x){
    if(cnt[x]==1)  st.insert(x); // 只剩一个了
    --cnt[x];
    mst.erase(mst.find(x)); //不能写成mulst.erase(x) 这样是删除所有值为x的元素
}
int getmx(){
    return *st.begin();
}
void clear(){
    while(!mst.empty()){
        del(*mst.begin());
    }
}