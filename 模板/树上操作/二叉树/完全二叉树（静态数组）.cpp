// L2-035 完全二叉树的层序遍历  https://pintia.cn/problem-sets/994805046380707840/exam/problems/1336215880692482058?type=7&page=1
#include <bits/stdc++.h>
using namespace std;
const int N=310;
int n;
int a[N];
int idx=0;
int b[N];
void dfs(int u){ //镜头数组dfs
    if(u>n) return;
    if(u*2<=n) dfs(u*2);
    if(u*2+1<=n) dfs(u*2+1);
    b[u]=a[++idx];
}
int pos=0;
void bfs(int u){
    queue<int> q;
    q.push(u);
    while(!q.empty()){ //静态数组bfs
        int sz=q.size();
        for(int i=0;i<sz;i++){
            int u=q.front();
            q.pop();
            if (++pos == 1) { cout <<b[u];}
            else cout<<" "<<b[u];
            if(2*u<=n) q.push(2*u);
            if(2*u+1<=n) q.push(2*u+1);
        }
    }
}

void solve(){
    cin>>n;
    for(int i=1;i<=n;i++) cin>>a[i];
    dfs(1);
    bfs(1);
}
int main(){
    solve();
    return 0;
}