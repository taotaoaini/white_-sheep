#include <bits/stdc++.h>
using namespace std;
using i64 = long long;
const i64 dinf=0x3f3f3f3f3f3f3f3f;//4.62*10^18级别的
/*
从特殊到一般：先思考一条链的情况，然后逐渐增加分支来思考一般的情况


*/


// 1.最大节点价值之和 https://leetcode.cn/problems/find-the-maximum-sum-of-node-values/description/

// (1)选择任意次树上的边，使边的端点，使nums[u] = nums[u] XOR k,nums[v] = nums[v] XOR k
// 1)一个数异或两次（偶数次）k后保持不变
// 对于一条从 x 到 y 的简单路径，我们把路径上的所有边操作后，
// 路径上除了 x 和 y 的其它节点都恰好操作两次，所以只有 nums[x]和 nums[y]都异或了 k，其余元素不变。
// 所以题目中的操作可以作用在任意两个数上。我们不需要建树，edges是多余的。
// 2)无论操作多少次，总是有偶数个元素异或了k，其余元素不变
//如果我们操作的两个数之前都没有异或过，那么操作后，异或 k的元素增加了 2。
//如果我们操作的两个数之前都异或过，那么操作后，异或 k 的元素减少了 2。
//如果我们操作的两个数之前一个异或过，另一个没有异或过，那么操作后，异或 k的元素加一减一，不变。
//问题变成：选择 nums中的偶数个元素，把这些数都异或 k，数组的最大元素和是多少？
// 状态机 DP 解决。
//定义 f[i][0]表示选择 nums 的前 i 数中的偶数个元素异或 k，得到的最大元素和。
//定义 f[i][1]表示选择 nums 的前 i 数中的奇数个元素异或 k，得到的最大元素和。
// 0:偶数次， 1：奇数次
// f[i][0]=max(f[i-1][0],f[i-1][1]+(nums[i]^k));
// f[i][1]=max(f[i-1][1],f[i-1][0]+(nums[i]^k));
struct Solution1{
    long long solve1(vector<int>& nums, int k, vector<vector<int>>& edges) {
        int n=nums.size();
        vector<array<i64,2>> f(n+1);
        f[0][0]=0;
        f[0][1]=-dinf;
        for(int i=1;i<=n;i++){
            f[i][0]=max(f[i-1][0]+nums[i-1],f[i-1][1]+(nums[i-1]^k));
            f[i][1]=max(f[i-1][1]+nums[i-1],f[i-1][0]+(nums[i-1]^k));
        }
        return f[n][0];
    }
};
// 一般化解题（树形DP）
// 用「选或不选」思考。
// 对于以 x 为根的子树，考虑 x 和它的儿子 y 之间的边是否操作。
// 定义 f[x][0] 表示 x 操作偶数次时，子树 x 的除去 x 的最大价值和。
// 定义 f[x][1] 表示 x 操作奇数次时，子树 x 的除去 x 的最大价值和。
