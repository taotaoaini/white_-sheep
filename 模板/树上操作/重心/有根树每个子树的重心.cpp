// Kay and Snowflake  https://codeforces.com/problemset/problem/685/B
// 给你一棵有根数根为1，然后要你求每个子树的重心
// 如果一个点是一棵树的重心，那么以这个点为根，其所有的子树的大小都不超过整个树的一半
// 一棵树的重心一定是它重儿子重心到这个树的根的路径中。
// 那我们肯定是往点多的方向走，而且上面多了点，所以就是往上走，往下面走时不优的。

#include<bits/stdc++.h>

using namespace std;
const int N = 300010;
int n, q;
int fa[N];
vector<int> e[N];
int son[N], sz[N];
int ans[N];

void dfs(int u) {
    sz[u] = 1;
    son[u] = 0;
    for (auto v : e[u]) {
        dfs(v);
        sz[u] += sz[v];
        if (sz[son[u]] < sz[v]) son[u] = v;
    }
    if (sz[son[u]] * 2 > sz[u]) { // 重儿子都没有超过sz[u]的一半，则重心是当前根节点
        int mx = ans[son[u]]; // 重儿子的重心,每个点最多跳一次，保证了时间复杂度
        while ((sz[u] - sz[mx]) * 2 > sz[u]) mx = fa[mx]; // 保证以重心为中心，子树都小于sz[u]/2
        ans[u] = mx;
    } else {
        ans[u] = u;
    }
}

void solve() {
    cin >> n >> q;
    for (int i = 2; i <= n; i++) {
        int x;
        cin >> x;
        e[x].push_back(i);
        fa[i] = x;
    }
    dfs(1);
    for (int i = 1; i <= q; i++) {
        int x;
        cin >> x;
        cout << ans[x] << endl;
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    solve();
    return 0;
}