#include<bits/stdc++.h>

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)
#define uls u<<1
#define urs u<<1|1
#define vls v<<1
#define vrs v<<1|1

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

//int[]:6.7e7
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

//--要素--
//延迟标记
//相对标记：相对标记指的是可以共存的标记，且打标记的顺序与答案无关，即标记可以叠加。
//绝对标记：绝对标记是指不可以共存的标记，每一次都要先把标记下传，再给当前节点打上新的标记。

//二维线段树：维护二维表的信息，区间和，区间最值
//线段树套线段树，在外层线段树的每个节点上再开一颗内层线段树
//外层维护行信息，内层维护列信息
//空间N*4*M*4
//点修：1.外层经过的节点均入内，2.内层经过的节点均修改 Q(logN*logM)
//区查：1.外层覆盖即入内，2.内层覆盖即返回 Q*(logN*logM)
struct Solution1 {
    int n;
    int sum[N << 2][N << 2];

    Solution1(int n) {
        this->n = n;
        memset(sum, 0, sizeof(sum));
    }

    Solution1() {
        memset(sum, 0, sizeof(sum));
    }

    void init(int n) {
        this->n = n;
    }

    void changeY(int u, int v, int l, int r, int y, int a) { //内修
        sum[u][v] += a; //内层经过的节点均修改
        if (l == r) return;
        if (y <= mid) changeY(u, vls, l, mid, y, a);
        else changeY(u, vrs, mid + 1, r, y, a);
    }

    void changeX(int u, int l, int r, int x, int y, int a) { //外修
        changeY(u, 1, 1, n, y, a); //外层经过的节点均入内
        if (l == r) return;
        if (x <= mid) changeX(uls, l, mid, x, y, a);
        else changeX(urs, mid + 1, r, x, y, a);
    }

    int queryY(int u, int v, int l, int r, int y1, int y2) { //内查
        if (y1 <= l && r <= y2) return sum[u][v]; //内层覆盖即返回
        if (y2 <= mid) return queryY(u, vls, l, mid, y1, y2);
        else if (y1 > mid) return queryY(u, vrs, mid + 1, r, y1, y2);
        else
            return queryY(u, vls, l, mid, y1, mid)
                   + queryY(u, vrs, mid + 1, r, mid + 1, y2);
    }

    int queryX(int u, int l, int r, int x1, int x2, int y1, int y2) { //外查
        if (x1 <= l && r <= x2) return queryY(u, 1, 1, n, y1, y2); //外层覆盖即入内
        if (x2 <= mid) return queryX(uls, l, mid, x1, x2, y1, y2);
        else if (x1 > mid) return queryX(urs, mid + 1, r, x1, x2, y1, y2);
        else
            return queryX(uls, l, mid, x1, mid, y1, y2)
                   + queryX(urs, mid + 1, r, mid + 1, x2, y1, y2);
    }
};

//void solve1() {
//    int n;
//    int op, x, y, a, x1, x2, y1, y2;
//    Solution1 *st = new Solution1();
//    while (~scanf("%d", &op)) {
//        if (op == 0) {
//            scanf("%d", &n);
//            st->init(n);
//        }
//        if (op == 1)
//            scanf("%d%d%d", &x, &y, &a),
//                    st->changeX(1, 1, n, x + 1, y + 1, a);
//        if (op == 2)
//            scanf("%d%d%d%d", &x1, &y1, &x2, &y2),
//                    printf("%d\n", st->queryX(1, 1, n, x1 + 1, x2 + 1, y1 + 1, y2 + 1));
//        if (op == 3) break;
//    }
//}


//标签永久化 ：https://www.luogu.com.cn/problem/P3372
//修改时留下的懒标记并不下传，也不删除，而是留在打上标记的那个节点。
//当查询经过这个节点时，就加上这个节点的懒标记造成的影响，
int a[N];//原数组
struct Solution2 {
    i64 sum[N * 4], tag[N * 4];//区间和，永久标记
    void build(int u, int l, int r) {//建树
        sum[u] = a[l];
        if (l == r) return;
        build(ls, l, mid);
        build(rs, mid + 1, r);
        sum[u] = sum[ls] + sum[rs];
    }

    void change(int u, int l, int r, int ql, int qr, i64 x) {//区修
        sum[u] += (min(r, qr) - max(ql, l) + 1) * x;//经过节点更新sum
        if (ql <= l && qr >= r) {//覆盖节点更新tag
            tag[u] += x;
            return;
        }
        if (ql <= mid) change(ls, l, mid, ql, qr, x);
        if (qr > mid) change(rs, mid + 1, r, ql, qr, x);
    }

    i64 query(int u, int l, int r, int ql, int qr, i64 s) {//区查
        if (ql <= l && qr >= r) return sum[u] + (min(r, qr) - max(ql, l) + 1) * s;
        i64 res = 0;
        s += tag[u];//累计标记，下传给儿子
        if (ql <= mid) res += query(ls, l, mid, ql, qr, s);
        if (qr > mid) res += query(rs, mid + 1, r, ql, qr, s);
        return res;
    }
} st;

//void solve2() {
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    st.build(1, 1, n);
//    for (int i = 1, op, l, r, x; i <= m; i++) {
//        cin >> op;
//        // cout<<"op:::"<<op<<endl;
//        if (op == 1) {
//            cin >> l >> r >> x;
//            st.change(1, 1, n, l, r, x);
//        } else {
//            cin >> l >> r;
//            cout << st.query(1, 1, n, l, r, 0) << endl;
//        }
//    }
//}

//3.权值线段树
//离散化
struct Solution3 {
    vector<i64> sum;
    vector<int> cnt;

    void init(int n) {
        sum.clear();
        cnt.clear();
        sum.assign((n + 10) * 4, 0);
        cnt.assign((n + 10) * 4, 0);
    }

    void pushup(int u) {
        sum[u] = sum[ls] + sum[rs];
        cnt[u] = cnt[ls] + cnt[rs];
    }

    void change(int u, int l, int r, int x, int k, int kt) { //点修
        if (l == r) {
            sum[u] += k, cnt[u] += kt;
            return;
        }
        if (x <= mid) change(ls, l, mid, x, k, kt);
        else change(rs, mid + 1, r, x, k, kt);
        pushup(u);
    }

    int query(int u, int l, int r, int x, int y) {//区查
        if (x <= l && r <= y) return sum[u];
        int s = 0;
        if (x <= mid) s += query(ls, l, mid, x, y);
        if (y > mid) s += query(rs, mid + 1, r, x, y);
        return s;
    }

    i64 find(int u, int l, int r, int s) {//线段树二分
        if (s == 0) return 0;
        if (cnt[u] == s) return sum[u];
        if (cnt[ls] > s) return find(ls, l, mid, s);
        else return find(rs, mid + 1, r, s - cnt[ls]) + sum[ls];
    }
};

//void solve3() {//离散化 22333=12345
//    int n;
//    vector<int> a(n + 10);
//    vector<pii> b(n + 10);
//    for (int i = 1; i <= n; i++) b[i] = {a[i], i};
//    sort(b.begin(), b.end());
//    for (int i = 1; i <= n; i++) {
//        int id = lower_bound(b.begin(), b.end(), make_pair(a[i], i))-b.begin();
//    }
//}

// 4. 维护矩阵
struct Mat {
    i64 a[2][2];

    void init() { //单位矩阵
        a[0][0] = a[1][1] = 1;
        a[1][0] = a[0][1] = 0;
    }

    Mat operator*(const Mat &tmp) const {
        Mat res;
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++) res.a[i][j] = 0;

        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 2; j++) {
                    res.a[i][j] = (res.a[i][j] + a[i][k] * tmp.a[k][j]) % mod;
                }
            }
        }
        return res;
    }

    Mat operator+(const Mat &tmp) const {
        Mat res;
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++) res.a[i][j] = 0;

        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                res.a[i][j] = (a[i][j] + tmp.a[i][j]) % mod;
        return res;
    }
};

Mat qmi(i64 n) {
    Mat tmp, res;
    tmp.a[0][0] = tmp.a[0][1] = tmp.a[1][0] = 1;
    tmp.a[1][1] = 0;
    res.init();
    while (n) {
        if (n & 1) res = res * tmp;
        tmp = tmp * tmp;
        n >>= 1;
    }
    return res;
}

//int a[N];
struct Solution4 {
    vector<Mat> sum;
    vector<Mat> add;

    void init(int n) {
        sum.resize(4 * (n + 10));
        add.resize(4 * (n + 10));
    }

    void pushup(int u) {
        sum[u] = sum[ls] + sum[rs];
    }

    void pushdown(int u, int l, int r) {
        sum[ls] = add[u] * sum[ls];
        sum[rs] = add[u] * sum[rs];
        add[ls] = add[u] * add[ls];
        add[rs] = add[u] * add[rs];
        add[u].init();
    }

    void build(int u, int l, int r) {
        sum[u].init(), add[u].init();
        if (l == r) {
            sum[u] = qmi(a[l] - 1);
            return;
        }
        build(ls, l, mid);
        build(rs, mid + 1, r);
        pushup(u);
    }

    void update(int u, int l, int r, int ql, int qr, Mat val) {
        if (ql <= l && qr >= r) {
            sum[u] = sum[u] * val;
            add[u] = add[u] * val;
            return;
        }
        pushdown(u, l, r);
        if (ql <= mid) update(ls, l, mid, ql, qr, val);
        if (qr > mid) update(rs, mid + 1, r, ql, qr, val);
        pushup(u);
    }

    i64 query(int u, int l, int r, int ql, int qr) {
        if (ql <= l && qr >= r) {
            return sum[u].a[0][0];
        }
        pushdown(u, l, r);
        i64 res = 0;
        if (ql <= mid) res = (res + query(ls, l, mid, ql, qr)) % mod;
        if (qr > mid) res = (res + query(rs, mid + 1, r, ql, qr)) % mod;
        return res;
    }
};
//void solve() {
//    //freopen("a.in","r",stdin);
//    //freopen("a.out","w",stdout);
//    int n, m;
//    cin >> n >> m;
//    for (int i = 1; i <= n; i++) cin >> a[i];
//    tr.init(n);
//    tr.build(1, 1, n);
//    for (int i = 1, op, l, r, x; i <= m; i++) {
//        cin >> op;
//        if (op == 1) {
//            cin >> l >> r >> x;
//            Mat val = qmi(x);
//            tr.update(1, 1, n, l, r, val);
//        } else {
//            cin >> l >> r;
//            cout << tr.query(1, 1, n, l, r) << endl;
//        }
//    }
//}

// 5.动态开点
//https://codeforces.com/problemset/problem/915/E
// 空间复杂度: 修改子树次数m，分裂开点时，最坏的情况会走两个分支，到叶子节点。
//  单词修改的最坏开点为2logn，所以,空间复杂度为O(m*2logn)，logn的情况不需要考虑最坏
struct Solution5 {
    int root, tot;
    vector<int> lc, rc, sum, add;

    void init(int n) {
        lc.assign(n + 10, 0);
        rc.assign(n + 10, 0);
        sum.assign(n + 10, 0);
        add.assign(n + 10, -1);
    }

    void pushup(int u) { //上传
        sum[u] = sum[lc[u]] + sum[rc[u]];
    }
    void pushdown(int u, int l, int r) { //下传
        if (add[u] == -1) return;
        if (!lc[u]) lc[u] = ++tot; //动态开点
        if (!rc[u]) rc[u] = ++tot;
        sum[lc[u]] = add[u] * (mid - l + 1);
        sum[rc[u]] = add[u] * (r - mid);
        add[lc[u]] = add[rc[u]] = add[u];
        add[u] = -1;
    }
    void update(int &u, int l, int r, int x, int y, int k) { //区修
        if (!u) u = ++tot; //动态开点
        if (x <= l && r <= y) {sum[u] = k * (r - l + 1); add[u] = k; return;}
        pushdown(u, l, r);
        if (x <= mid) update(lc[u], l, mid, x, y, k);
        if (y > mid) update(rc[u], mid + 1, r, x, y, k);
        pushup(u);
    }
};
//void solve() {
//    //freopen("a.in","r",stdin);
//    //freopen("a.out","w",stdout);
//    // 1:工作，0:不工作了
//    int n;
//    cin>>n;
//    tr.init(1.5e7);
//    int q;
//    cin>>q;
//    for(int i=1,op,l,r;i<=q;i++){
//        cin>>l>>r>>op;
//        if(op==1){
//            tr.update(tr.root,1,n,l,r,1);
//        }else{
//            tr.update(tr.root,1,n,l,r,0);
//        }
//        cout<<n-tr.sum[tr.root]<<endl;
//    }
//}





