// DP解法
// 本方法可以求具体路径 ，但要求边权为非负数
#include <bits/stdc++.h>

using namespace std;
const int inf=0x3f3f3f3f;
// 链式前向星存图
const int N = 100010;
int h[N], ne[N * 2], w[N * 2], e[N * 2], idx = 0;

// h:头，ne:下一条边，e：指向的节点
void add(int u, int v, int c) { //加边
    e[++idx] = v;
    ne[idx] = h[u];
    w[idx] = c;
    h[u] = idx;
}
int d[N],f[N],b[N]; //f:存路径和，f：存父亲节点，b：存到达父亲节点边的编号
void dfs(int u,int fa){
    for(int i=h[u];i;i=ne[i]){
        int v=e[i];
        if(v==fa) continue;
        d[v]=d[u]+1;
        f[v]=u;b[v]=i;
        if(d[v]>d[0]){ //d[0]:存的是所需要的端点
            f[0]=v;
            d[0]=d[v];
        }
        dfs(v,u);
    }
}

void work(int& p,int & q){ //p,q为直径的两个端点
    d[0]=-inf; //取一个不可达值
    d[1]=0;dfs(1,0);p=f[0];
    d[p]=0;dfs(p,0);q=f[0]; //此时p作为根节点，找另一个端点
}


int n,k;
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> k;
    for (int i = 1; i <= n - 1; i++) {
        int u, v, c;
        cin >> u >> v;
        add(u, v, 1);
        add(v, u, 1);
    }
    int p=-1,q=-1;
    work(p, q);
    cout<<p<<" "<<q<<endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}

