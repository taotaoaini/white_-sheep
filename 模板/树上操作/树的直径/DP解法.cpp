// DP解法：可以解决边权为负的情况
#include <bits/stdc++.h>

using namespace std;
const int N = 100010;
const int inf = 0x3f3f3f3f;
int h[N], ne[N * 2], w[N * 2], e[N * 2], idx = 0;
int d1[N], d2[N]; //d1：最大,d2:次大
int f[N], b[N];

// 链式前向星解法
void add(int u, int v, int c) {
    e[++idx] = v;
    ne[idx] = h[u];
    h[u] = idx;
    w[idx] = c;
}

void dfs(int u, int fa, int &mx) {
    for (int i = h[u]; i; i = ne[i]) {
        int v = e[i];
        if (v == fa) continue;
        dfs(v, u, mx);
        int t = d1[v] + w[i];
        if (t > d1[u]) {
            d2[u] = d1[u], d1[u] = t;
        } else if (t > d2[u]) {
            d2[u] = t;
        }
    }
    d1[u] = max(d1[u], 0); //
    d2[u] = max(d2[u], 0); //此处可以处理有负边权的情况
    mx = max(mx, d1[u] + d2[u]);
}

int n, k;

void solve() {
    cin >> n >> k;
    for (int i = 1; i <= n - 1; i++) {
        int u, v, c;
        cin >> u >> v;
        add(u, v, 1);
        add(v, u, 1);
        memset(d1, -0x3f, sizeof(d1));
        memset(d2, -0x3f, sizeof(d2));
        int mx = 0;
        dfs(1, 0, mx);
        cout << mx << endl;
    }
}
