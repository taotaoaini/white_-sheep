#include <bits/stdc++.h>

using namespace std;

// L2-043 龙龙送外卖 https://pintia.cn/problem-sets/994805046380707840/exam/problems/1518582482059845632?type=7&page=1
// 已知每个节点的父节点，自底向上，处理路径上的问题,维护信息
// 从根节点到达每个需要的节点走过的路径，最后返回根节点，则是走过的路径标记数*2
// 如果最后不返回，则是走过的路径标记数*2 - 节点到根节点最长路径长度
#include<bits/stdc++.h>

using namespace std;
const int N = 1e5 + 10;

int n, m;
int d[N];  // 深度
int mx = 0; // 节点到根节点最长路径长度
int fa[N]; // 每个节点父节点

int dfs(int u, int depth) { //处理走过的路径
    if (fa[u] == -1 || d[u]) { //如果之前已经遍历过了，直接处理返回
        mx = max(mx, depth + d[u]);
        return depth * 2;
    }
    int res = dfs(fa[u], depth + 1);
    d[u] = d[fa[u]] + 1;
    return res;
}

void solve() {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) {
        cin >> fa[i];
    }
    int res = 0;
    for (int i = 1, x; i <= m; i++) {
        cin >> x;
        res += dfs(x, 0);
        cout << res - mx << endl;
    }
}

int main() {
    solve();
    return 0;
}