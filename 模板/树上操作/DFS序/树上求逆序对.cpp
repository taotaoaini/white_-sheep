// L3-032 L3-032 关于深度优先搜索和逆序对的题应该不会很难吧这件事 https://pintia.cn/problem-sets/994805046380707840/exam/problems/1518582895035215872?type=7&page=1

// 统计树上DFS序中总共出现的逆序对
// （1）计算DFS序个数：对于每棵以u为根的子树，u的子树个数为n个，则排列顺序为n！
// （2）根据乘法原理：当前节点的dfs序数=以其子节点为根的dfs序数*子节点的排序数
#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
const i64 mod=1e9+7;
const int N=3e5+10;
vector<int> e[N];
int n,rt;
i64 fact[N]; //阶乘
i64 f[N]; // 为以u为子树的dfs序方案数
i64 cnt1=0,cnt2=0;

// 树状数组求逆序对
i64 tr[N];
int lowbit(int x){
    return x&-x;
}
void add(int x,int val){
    for(int i=x;i<=n;i+=lowbit(i)) tr[i]+=val;
}
i64 query(int x){
    i64 res=0;
    for(int i=x;i>=1;i-= lowbit(i)) res+=tr[i];
    return res;
}
void init(){
    fact[0]=1;
    for(int i=1;i<N;i++){
        fact[i]=fact[i-1]*i%mod;
    }
}
i64 qmi(i64 a,i64 b){
    i64 res=1;
    while(b){
        if(b&1) res=res*a%mod;
        a=a*a%mod;
        b/=2;
    }
    return res;
}

void dfs(int u,int fa){
    f[u]=1;
    for(int v:e[u]){
        if(v==fa) continue;
        cnt1=(cnt1+query(n)-query(v)+mod)%mod;// 祖先里比当前大的数的个数
        cnt2=(cnt2+query(v))%mod;// 祖先里比当前小的数的个数
        add(v,1);
        dfs(v,u);
        add(v,-1);
        f[u]=f[u]*f[v]%mod;
    }
    //当前节点的dfs序数 = ∏以其子节点为根的子树的dfs序数×子节点的排列数
    i64 x=(u==rt?e[u].size():e[u].size()-1);
    f[u]=f[u]*fact[x]%mod;
}
void solve(){
    init();
    cin>>n>>rt;
    for(int i=1;i<=n-1;i++){
        int x,y;
        cin>>x>>y;
        e[x].push_back(y);
        e[y].push_back(x);
    }
    add(rt,1);
    dfs(rt,-1);
    i64 x=((1LL*n*(n-1)%mod*qmi(2,mod-2)%mod-cnt1-cnt2)%mod+mod)%mod;//没有父子关系的点对数量
    i64 res=f[rt]*(cnt1+x*qmi(2,mod-2)%mod)%mod; //cnt1就是有父子关系的逆序对数
    cout<<res<<endl;
}
int main(){
    solve();
    return 0;
}

