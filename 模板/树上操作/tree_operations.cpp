#include<bits/stdc++.h>

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls u<<1
#define rs u<<1|1
#define fi first
#define se second
#define mid ((l+r)/2)

using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using i128 = __int128_t;
using u128 = __uint128_t;
using f128 = long double;

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;
const int inf = 0x3f3f3f3f;//1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f;//4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 1100;

//树上修改与查询
//https://www.luogu.com.cn/problem/P3384
struct Solution1 {
    int n;
    int w[N];
    vector<int> e[N];
    int fa[N], dep[N], sz[N], son[N];
    int top[N], id[N], nw[N], cnt;

    void init() {
        this->n = n;
    }

    void dfs1(int u, int father) {//求fa,dep,sz,son
        fa[u] = father, dep[u] = dep[father] + 1, sz[u] = 1;
        for (int v: e[u]) {
            if (v == father) continue;
            dfs1(v, u);
            sz[u] += sz[v];
            if (sz[son[u]] < sz[v]) son[u] = v;
        }
    }

    void dfs2(int u, int t) {//求top,id,nw
        top[u] = t, id[u] = ++cnt, nw[cnt] = w[u];
        if (!son[u]) return;
        dfs2(son[u], t);
        for (int v: e[u]) {
            if (v == fa[u] || v == son[u]) continue;
            dfs2(v, u);
        }
    }

    struct tree {
        i64 add, sum;
    } tr[N * 4];

    void pushup(int u) {
        tr[u].sum = tr[ls].sum + tr[rs].sum;
    }

    void pushdown(int u) {

    }

    void build(int u, int l, int r) {
        tr[u] = {0, nw[r]};
        if (l == r) return;
        build(ls, l, mid);
        build(rs, mid + 1, r);
        pushup(u);
    }

    i64 query(int u, int l, int r, int ql, int qr) {
        if (ql <= l && qr >= r) return tr[u].sum;
        pushdown(u);
        i64 res = 0;
        if (l <= mid) res += query(ls, l, mid, ql, qr);
        if (r > mid) res += query(rs, mid + 1, r, ql, qr);
        return res;
    }

    i64 query_path(int u, int v) {
        i64 res = 0;
        while (top[u] != top[v]) {
            if (dep[top[u]] < dep[top[v]]) swap(u, v);//每次都操作最深的节点
            res += qeury(1, 1, n, id[top[u]], id[u]);
            u = fa[top[u]];
        }
        if(dep[u]<dep[v]) swap(u,v);
        res+= query(1,1,n,id[v],id[u]);
        return res;
    }
    void change(int u,int l,int r,int ql,int qr,int k){
        if(ql<=l&&qr>=r){
            tr[u].add+=k;
            tr[u].sum+=k;
            return;
        }
        pushdown(u);
        if(ql<=mid) change(u,l,mid,ql,qr,k);
        if(qr>mid) change(u,mid+1,r,ql,qr,k);
        return;
    }
};
