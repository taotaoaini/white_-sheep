// gcd区间 https://www.luogu.com.cn/problem/P1890
// n=1e3,m=1e6

#include<bits/stdc++.h>
using namespace std;
const int N=1010;
int n,m; // n:数组大小，m：查询次数
int f[N][N];
int gcd(int a,int b){
    return b==0?a:gcd(b,a%b);
}
void init(){ //O(n^2)
    for(int i=n;i>=1;i--){ //需要从后往前递推
     for(int j=i+1;j<=n;j++){
        f[i][j]=gcd(f[i][i],f[i+1][j]);
     }
    }
}
void solve(){
    cin>>n>>m;
    for(int i=1;i<=n;i++) cin>>f[i][i];
    init();
    for(int i=1;i<=m;i++){
        int x,y;
        cin>>x>>y;
        cout<<f[x][y]<<endl;
    }
}
int main(){
    solve();
    return 0;
}