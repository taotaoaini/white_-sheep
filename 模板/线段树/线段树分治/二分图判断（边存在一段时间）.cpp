// 二分图 /【模板】线段树分治 https://www.luogu.com.cn/problem/P5787
// 以时间为下标，建立线段树
//如第1,2,3个操作一样的，那么我们可以在[1,3]的时间上都做相同的修改，而不必分别在[1,1],[2,2],[3,3]修改
//
#include <bits/stdc++.h>
using namespace std;

using pii = pair<int, int>;
#define fi first
#define se second
#define mp make_pair
#define ls (p*2)
#define rs (p*2+1)
#define mid ((l+r)/2)
const int N = 200010;
const int M = 200010;
struct DSU {
    int tot = 0, fa[N], sz[N], s[N];

    void init(int n) {
        for (int i = 0; i <= n; i++) { // 初始化
            fa[i] = i;
            sz[i] = 1;
        }
    }

    int find(int x) { // 即find查找函数
        while (x != fa[x]) x = fa[x];
        return x;
    }

    void merge(int u, int v) { // 合并函数
        int x = find(u);
        int y = find(v);
        if (x == y) return;
        if (sz[x] > sz[y]) swap(x, y);
        s[++tot] = x, fa[x] = y, sz[y] += sz[x];
    }

    void dodel() {// 删除栈顶边
        if (!tot) return;
        int x = s[tot--];
        sz[fa[x]] -= sz[x], fa[x] = x;
    }

    void back(int t) { //删除到只剩t条边
        while (tot > t) dodel();
    }
} d;

int n, m, k;
pii e[M];
vector<pii> tr[N << 2]; // 线段树
int ans[N];
void add(int p, int l, int r, int ql, int qr, pii edge) {  // 将加入的边插入[ql, qr]时间段
    if (ql <= l && qr >= r) {
        tr[p].push_back(edge); // 标记永久化
        return;
    }
    if (ql <= mid) add(ls, l, mid, ql, qr, edge);
    if (qr > mid) add(rs, mid + 1, r, ql, qr, edge);
}
void query(int p, int l, int r) {  // O(nlogn)
    int now = d.tot; // now记录最开始栈中元素个数
    bool flag = false;
    for (auto edge : tr[p]) {
        int x = edge.fi, y = edge.se; // 我们要将x和y分成两种不同的类
        if (d.find(x) == d.find(y)) { // 如果x和y已经是同类了, 那么就有冲突了
            flag = true;
            break;
        }
        // 我们要将x和y分成两种不同的类
        d.merge(x, y + n);
        d.merge(y, x + n);
    }
    if (!flag) {
        if (l == r) ans[l] = true; // 如果到达子树都没有冲突，说明此时间符合条件可以
        else { // 继续查找子树
            query(ls, l, mid);
            query(rs, mid + 1, r);
        }
    }
    d.back(now); //每次结束都撤回操作
}

void solve() {
    cin >> n >> m >> k;
    for (int i = 1; i <= m; i++) {
        int x, y, l, r;
        cin >> x >> y >> l >> r;
        l += 1; // 坐标向右偏移一位
        if (l <= r) add(1, 1, k, l, r, mp(x, y));
    }
    d.init(2 * n); // 种类并查集，分成两类，空间开两倍
    query(1, 1, k);
    for (int i = 1; i <= k; i++) {
        if (ans[i]) cout << "Yes" << endl;
        else cout << "No" << endl;
    }
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0); cout.tie(0);
    solve();
    return 0;
}