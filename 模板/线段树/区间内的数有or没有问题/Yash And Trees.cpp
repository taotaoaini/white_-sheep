// cf633g cf687c cf1955g cf1941d cf1899g cf1855d  cf1771f cf1743d cf1695c cf1641d cf1516c abc348f abc292e abc291g abc289c abc287h

// Yash And Trees https://codeforces.com/problemset/problem/633/G
// 1）问题：当需要在数组查找(ai mod m = x) x有多少不同，而且m较小的情况下
// 解决： 用bitset维护，bitset[x]=1，表示这个数组内存在点权 mod m = x的点
// 2）问题：当查询变为区间，并可以支持区间加操作
// 解决：线段树维护bitset，合并按位或
//      求和， info[(i+x) mod m]=info[i],对于i<m-x只要左移，对于i>=m-x相当于移动到i+x-m位，右移m-x位
//      info=(info<<x)|(info>>m-x)
// 3）问题：当上面操作放到了有根树上，对u的子树进行操作
// 解决：构造树的DFS序，转化为连续区间操作，就可以用线段树维护了

// 空间复杂度分析：
// bitset的一位只占1bit,因此内存为1000/8字节 。O(4n*1000/8)
// 空间复杂度: bitset的各种操作都是O(1000/32)，因此O(q(logn * 1000/32+tot))

#include<bits/stdc++.h>
#define mid ((l+r)/2)
#define ls (p*2)
#define rs (p*2+1)
using namespace std;
const int N = 100010;
const int M = 1010;
// 树
int n, m, q;
int a[N];
vector<int> e[N];
//int in[N], out[N], id[N], tot = 0;
int dep[N], sz[N], dfn[N], id[N], tot;
// 质数表
bool vis[M];
vector<int> pr;
//线段树
bitset<M> tr[N << 2], ans; // ans:查询答案
int add[N << 2];

void init(int n) { //构造素数表
    vis[1] = true;
    for (int i = 2; i < n; i++) { // 这里不能等于n，防止把m本身算进去了
        if (!vis[i]) pr.push_back(i);
        for (int j = 0; j < pr.size() && 1LL * i * pr[j] < n; j++) {
            vis[i * pr[j]] = true;
            if (i % pr[j] == 0) break;
        }
    }
}

//void dfs_init(int u, int fa) {
//	in[u] = ++tot;
//	id[tot] = u;
//	for (auto v: e[u]) {
//		if (v == fa) continue;
//		dfs_init(v, u);
//	}
//	out[u] = tot;
//}
void dfs_init(int u, int fa) { // 构造DFS序
    dep[u] = dep[fa] + 1, sz[u] = 1, dfn[u] = ++tot, id[tot] = u;
    for (auto v: e[u]) {
        if (v == fa) continue;
        dfs_init(v, u);
        sz[u] += sz[v];
    }
}

void pull(int p) { //向上传
    tr[p] = tr[ls] | tr[rs];
}

void build(int p, int l, int r) { // 初建树
    if (l == r) {
        tr[p].reset();
        tr[p].set(a[id[l]]);
        return;
    }
    build(ls, l, mid);
    build(rs, mid + 1, r);
    pull(p);
}

void doadd(int p, int val) { // 更新
    tr[p] = (tr[p] << val) | (tr[p] >> (m - val));
    add[p] += val;
    add[p] %= m;
}

void push(int p) { // 向下传
    if (!add[p]) return;
    doadd(ls, add[p]);
    doadd(rs, add[p]);
    add[p] = 0;
}

void change(int p, int l, int r, int ql, int qr, int val) { // 修改
    if (ql <= l && qr >= r) {
        doadd(p, val);
        return;
    }
    push(p);
    if (ql <= mid) change(ls, l, mid, ql, qr, val);
    if (qr > mid) change(rs, mid + 1, r, ql, qr, val);
    pull(p);
}

void query(int p, int l, int r, int ql, int qr) { // 查询
    if (ql <= l && qr >= r) {
        ans |= tr[p];
        return;
    }
    push(p);
    if (ql <= mid) query(ls, l, mid, ql, qr);
    if (qr > mid) query(rs, mid + 1, r, ql, qr);
    return;
}

void solve() {
    cin >> n >> m;
    init(m); // 构建素数表
    for (int i = 1; i <= n; i++) cin >> a[i], a[i] %= m;
    for (int i = 1; i <= n - 1; i++) {
        int x, y;
        cin >> x >> y;
        e[x].push_back(y);
        e[y].push_back(x);
    }
    dfs_init(1, 0); // DFS序
    build(1, 1, n);
    cin >> q;
    while (q--) {
        int op, x, val;
        cin >> op >> x;
        if (op == 1) {
            cin >> val;
            change(1, 1, n, dfn[x], dfn[x] + sz[x] - 1, val % m);
        } else {
            ans.reset();
            query(1, 1, n, dfn[x], dfn[x] + sz[x] - 1);
            int cnt = 0;
            for (int i = 0; i < pr.size(); i++) {
                if (ans[pr[i]]) cnt++;
            }
            cout << cnt << endl;
        }
    }
}

int main() {
//	freopen("a.in","r",stdin);
    ios::sync_with_stdio(false);
    cin.tie(0);
    solve();
    return 0;
}