// 不包含相邻元素的子序列的最大和 https://leetcode.cn/problems/maximum-sum-of-subsequence-with-non-adjacent-elements/description/
// 可以修改数组元素值的打家劫舍
// 用分治的思想解决打家劫舍。
// 把 nums 从中间切开，分成两个子数组，分别记作 a 和 b。
// f(A)=f(a)+f(b) 但不对，同时选择了a的最后一个数和b的第一个数，就不满足题目要求了
// 添加约束
// 约束 a 的最后一个数一定不选，即 f(a′)+f(b)其中 a′是去掉最后一个数的 a
// 约束 b 的第一个数一定不选，即f(a) + f(b')其中 b'表示去掉第一个数的 b
// f00 表示在 A 第一个数一定不选，最后一个数也一定不选的情况下，打家劫舍的答案。
// f01 表示在 A 第一个数一定不选，最后一个数可选可不选的情况下，打家劫舍的答案。
// f10 表示在 A 第一个数可选可不选，最后一个数一定不选的情况下，打家劫舍的答案。
// f11(A)表示在 A 第一个数可选可不选，最后一个数也可选可不选的情况下，打家劫舍的答案
// 这等于上面定义的 f(A)。
#include<bits/stdc++.h>
using namespace std;
#define ls (p*2)
#define rs (p*2+1)
#define mid ((l+r)/2)
const long long mod= 1e9+7;
// 4 个数分别保存 f00, f01, f10, f11
vector<array<int, 4>> tr;
void pushup(int p) {
    auto& a = tr[ls], b = tr[rs];
    tr[p] = {
            max(a[0] + b[2], a[1] + b[0]),
            max(a[0] + b[3], a[1] + b[1]),
            max(a[2] + b[2], a[3] + b[0]),
            max(a[2] + b[3], a[3] + b[1]),
    };
}

// 用 nums 初始化线段树
void build(vector<int>& nums, int p, int l, int r) {
    if (l == r) {
        tr[p][3] = max(nums[l], 0);
        return;
    }
    build(nums, ls, l, mid);
    build(nums, rs, mid + 1, r);
    pushup(p);
};-


// 把 nums[i] 改成 val
void update(int p, int l, int r, int i, int val) {
    if (l == r) {
        tr[p][3] = max(val, 0);
        return;
    }
    if (i <= mid) {
        update(ls, l, mid, i, val);
    } else {
        update(rs, mid + 1, r, i, val);
    }
    pushup(p);
};
int maximumSumSubsequence(vector<int>& nums, vector<vector<int>>& queries) {
    int n = nums.size();
    tr.resize(4*n);
    build(nums, 1, 0, n - 1);
    long long ans = 0;
    for (auto& q : queries) {
        update(1, 0, n - 1, q[0], q[1]);
        ans += tr[1][3]; // 注意 f11 没有任何限制，也就是整个数组的打家劫舍
    }
    return ans % mod;
}