// 雨天的尾巴 /【模板】线段树合并 https://www.luogu.com.cn/problem/P4556
#include<bits/stdc++.h>
// #pragma GCC optimize(2)
// #pragma GCC optimize(3,"Ofast","inline")
using i8 = signed char;
using u8 = unsigned char;
using i16 = signed short int;
using u16 = unsigned short int;
using i32 = signed int;
using u32 = unsigned int;
using f32 = float;
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
//using i128 = __int128_t;
//using u128 = __uint128_t;
using f128 = long double;


// floor 小数向下取整
// ceil 小数向让取整
// round 四舍五入
#define count(x) (__builtin_popcount(x)) // 32位数中1的个数
#define countll(x) (__builtin_popcountll(x)) // 64位数中1的个数
#define endl '\n'
#define ls lc[p]
#define rs rc[p]
#define fi first
#define se second
#define mid ((l+r)/2)
#define mp make_pair

using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

int n, m, ans[N];
vector<int> e[N];
int fa[N][20], dep[N];
int root[N], tot;
int lc[N * 50], rc[N * 50]; // 其实这题最多只需要建2*n-1个节点，满二叉树
int sum[N * 50], type[N * 50]; //sum:某种救济粮的数量,type:救济粮的类型

void pushup(int p) {//上传
    if (sum[ls] >= sum[rs]) {
        sum[p] = sum[ls], type[p] = type[ls];
    } else {
        sum[p] = sum[rs], type[p] = type[rs];
    }
}

void change(int &p, int l, int r, int x, int k) {//点修
    if (!p) p = ++tot;
    if (l == r) {sum[p] += k; type[p] = x; return;}
    if (x <= mid) change(ls, l, mid, x, k);
    else change(rs, mid + 1, r, x, k);
    pushup(p);
}
void dfs(int u, int f) { // 倍增数组生成
    dep[u] = dep[f] + 1;
    fa[u][0] = f;
    for (int i = 1; i <= 18; i++) {
        fa[u][i] = fa[fa[u][i - 1]][i - 1];
    }
    for (auto v : e[u]) {
        if (v == f) continue;
        dfs(v, u);
    }
}
int lca(int u, int v) {//lca板子
    if (dep[u] < dep[v]) swap(u, v);
    for (int i = 18; i >= 0; i--) {
        if (dep[fa[u][i]] >= dep[v]) u = fa[u][i];
    }
    if (u == v) return v;
    for (int i = 18; i >= 0; i--) {
        if (fa[u][i] != fa[v][i]) {
            u = fa[u][i];
            v = fa[v][i];
        }
    }
    return fa[u][0];
}
int merge(int p, int x, int l, int r) {//合并
    if (!p || !x) return p + x;//一个为空，就返回另一个
    if (l == r) {sum[p] += sum[x]; return p;}
    ls = merge(ls, lc[x], l, mid);
    rs = merge(rs, rc[x], mid + 1, r);
    pushup(p);
    return p;
}
void calc(int u, int f) {//统计
    for (int v : e[u]) {
        if (v == f) continue;
        calc(v, u);
        root[u] = merge(root[u], root[v], 1, N);
    }
    ans[u] = sum[root[u]] ? type[root[u]] : 0;
}
void solve() {
    cin >> n >> m;
    for (int i = 1, x, y; i < n; i++) {
        cin >> x >> y;
        e[x].push_back(y);
        e[y].push_back(x);
    }
    dfs(1, 0);//树上倍增
    for (int i = 1, x, y, z; i <= m; i++) {// 点差分
        cin >> x >> y >> z;
        change(root[x], 1, N, z, 1);
        change(root[y], 1, N, z, 1);
        int t = lca(x, y);
        change(root[t], 1, N, z, -1);
        change(root[fa[t][0]], 1, N, z, -1);
    }
    calc(1, 0);//统计
    for (int i = 1; i <= n; i++) {
        cout << ans[i] << endl;
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
    }

    return 0;
}