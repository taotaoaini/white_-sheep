// 线段树分裂    https://www.luogu.com.cn/problem/P5494
// 支持删除，单点修改，区间查询，第k小值
// 权值线段树
// 1<=n<=1e5,1<=x,y,q<=2e5
#include<bits/stdc++.h>

using namespace std;
using i64 = long long;

const int N = 2e5 + 10;
int n, m;
int idx = 1;
i64 sum[N << 5];
int a[N];
int ls[N << 5], rs[N << 5], root[N << 5], rub[N << 5], cnt, tot;

int New() {// 内存分配与回收
    return cnt ? rub[cnt--] : ++tot;
}

void Del(int &p) { // 删除节点
    ls[p] = rs[p] = sum[p] = 0;
    rub[++cnt] = p;
    p = 0;
}

void pushup(int p) { // 上传
    sum[p] = sum[ls[p]] + sum[rs[p]];
}

void build(int &p, int l, int r) { //建树
    if (!p) p = New();
    if (l == r) {
        sum[p] = a[l];
        return;
    }
    int mid = (l + r) / 2;
    build(ls[p], l, mid);
    build(rs[p], mid + 1, r);
    pushup(p);
}

void update(int &p, int l, int r, int x, int val) { // 单点修改
    if (!p) p = New();
    if (l == r) {
        sum[p] += val;
        return;
    }
    int mid = (l + r) / 2;
    if (x <= mid)
        update(ls[p], l, mid, x, val);
    else
        update(rs[p], mid + 1, r, x, val);
    pushup(p);
}

int merge(int p, int q, int l, int r) { // 线段树合并
    if (!p || !q) return p + q;
    if (l == r) {
        sum[p] += sum[q];
        Del(q);
        return p;
    }
    int mid = (l + r) / 2;
    ls[p] = merge(ls[p], ls[q], l, mid);
    rs[p] = merge(rs[p], rs[q], mid + 1, r);
    pushup(p);
    Del(q);
    return p;
}

void split(int &p, int &q, int l, int r, int ql, int qr) { // 分裂
    if (qr < l || r < ql) return;
    if (!p) return;
    if (ql <= l && r <= qr) {
        q = p;
        p = 0;
        return;
    }
    if (!q) q = New();
    int mid = (l + r) / 2;
    if (ql <= mid) split(ls[p], ls[q], l, mid, ql, qr);
    if (qr>mid) split(rs[p], rs[q], mid + 1, r, ql, qr);
    pushup(p);
    pushup(q);
}

i64 query(int p, int l, int r, int ql, int qr) { // 区间查询
    if (!p) return 0;
    if (ql <= l && qr >= r) return sum[p];
    int mid = (l + r) / 2;
    i64 res = 0;
    if (ql <= mid) res += query(ls[p], l, mid, ql, qr);
    if (qr > mid) res += query(rs[p],mid+1, r, ql, qr);
    return res;
}

int kth(int p, int l, int r, int k) { // 权值线段树第k小
    if (l==r) return p;
    int mid =(l+r) / 2;
    i64 left = sum[ls[p]];
    if (k <= left)
        return kth(ls[p], l, mid, k);
    else
        return kth(rs[p],mid + 1, r, k - left);
}

int main() {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) cin >> a[i];
    build(root[1], 1, n);
    while (m--) {
        int op;
        cin >> op;
        if (!op) {
            int p, x, y;
            cin >> p >> x >> y;
            split(root[p], root[++idx], 1, n, x, y);
        } else if (op == 1) {
            int p, t;
            cin >> p >> t;
            root[p] = merge(root[p], root[t], 1, n);
        } else if (op == 2) {
            int p, x, q;
            cin >> p >> x >> q;
            update(root[p], 1, n, q, x);
        } else if (op == 3) {
            int p, x, y;
            cin >> p >> x >> y;
            cout << query(root[p],1,n,x,y) << endl;
        } else {
            int p, k;
            cin >> p >> k;
            if (sum[root[p]] < k)
                cout << -1 << endl;
            else
                cout << kth(root[p], 1, n,k) << endl;
        }
    }
}