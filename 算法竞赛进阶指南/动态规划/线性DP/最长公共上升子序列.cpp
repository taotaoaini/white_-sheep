// 最长公共上升子序列 https://www.acwing.com/problem/content/274/
#include<bits/stdc++.h>
using namespace std;

void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    int n;
    cin >> n;
    vector<int> a(n + 1);
    vector<int> b(n + 1);
    for (int i = 1; i <= n; i++) cin >> a[i];
    for (int i = 1; i <= n; i++) cin >> b[i];
    vector<vector<int>> f(n + 1, vector<int>(n + 1));
    for (int i = 1; i <= n; i++) { //a和b的所有位置匹配
        int tmp = 0;
        for (int j = 1; j <= n; j++) {
            // f[i][j] = max(f[i - 1][j], f[i][j - 1]); 错
            //分析为什么不能用像普通公共子序列的转移方程
            //因为需要确保f[i][j]的j是以b[j]为结尾的状态
            //f[i][j-1]破坏了结构，需要用到以b[j]为结尾的状态
            f[i][j]=f[i-1][j]; //b[j]不和a[i]匹配
            if (a[i] == b[j]) f[i][j] = max(f[i][j], tmp+1);
            if (b[j] < a[i]) tmp = max(tmp, f[i-1][j]); //线性DP常用的优化
            //a[i]固定，找到<b[j]的最长的f[i-1][j];
//            cout<<f[i][j]<<" ";
        }
//        cout<<endl;
    }
    int res=0;
    for(int i=1;i<=n;i++) res=max(res,f[n][i]); //答案放在了最后一层
    cout << res<< endl;
}
