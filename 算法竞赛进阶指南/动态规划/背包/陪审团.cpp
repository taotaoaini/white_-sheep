// 很好的题目，综合性很强的01背包问题，用到了负数下标偏移等技巧
//  陪审团 https://www.acwing.com/problem/content/282/
#include<bits/stdc++.h>
using namespace std;
using i64 = long long;
using pii = pair<int,int>;
#define fi first
#define se second
int f[210][810][30];
void solve() {
    int n, m;
    int idx = 0;
    cin >> n >> m;
    idx += 1;
    vector<pii> a(n + 1);
    vector<int> v(n + 1);
    vector<int> w(n + 1);
    for (int i = 1; i <= n; i++) cin >> a[i].fi >> a[i].se;
    for (int i = 1; i <= n; i++) v[i] = a[i].fi - a[i].se, w[i] = a[i].fi + a[i].se;
    const int base = 400; //偏量
    // vector<vector<vector<int>>> f(n + 10, vector<vector<int>>(N + 10, vector<int>(m + 10, -inf)));
    memset(f, -0x3f, sizeof(f));
    f[0][base][0] = 0; //向右偏移400
    for (int i = 1; i <= n; i++) { //01背包转移
        for (int j = 0; j <= 2 * base ; j++) {
            for (int k = 0; k <= m; k++) {
                f[i][j][k] = f[i - 1][j][k];
                if (k >= 1 && j + v[i] <= 2 * base && j + v[i] >= 0)
                    f[i][j][k] = max(f[i][j][k], f[i - 1][j + v[i]][k - 1] + w[i]);
            }
        }
    }
    int res = 0;
    int s = 0;
    for (int i = base, j = base;; i--, j++) {
        if (f[n][i][m] >= 0 || f[n][j][m] >= 0) {
            if (f[n][i][m] > f[n][j][m]) s = i;
            else s = j;
            res = max(f[n][i][m], f[n][j][m]);
            break;
        }
    }
//    cout<<res<<endl;
    vector<int> ans;
    int i = n, j = s, k = m;
    int sp = 0, sd = 0;
    while (k) { //解包,通过最优解逆推具体方案
        if (f[i][j][k] == f[i - 1][j][k]) i--;
        else {
            ans.push_back(i);
            sp += a[i].fi;
            sd += a[i].se;
            j += v[i];
            i--, k--;
        }
    }
    sort(ans.begin(), ans.end());
    cout << "Jury #" << idx << endl;
    cout << "Best jury has value " << sp << " for prosecution and value " << sd << " for defence:" << endl;
    for (int i = 0; i < (int)ans.size(); i++) cout << ans[i] << " ";
    cout << endl;
    cout << endl; //这题需要打两个空格
}