// 数字组合 https://acwing.com/problem/content/280/
#include <bits/stdc++.h>
using namespace std;
void solve() {
    int n,m;
    cin>>n>>m;
    vector<int> a(n+1);
    for(int i=1;i<=n;i++) cin>>a[i];
    vector<int> f(m+1,0);
    //恰好型背包求方案数，为什么不需要初始化为不可到达值捏
    // 如果转移途中f[i][j]=0,就表示没有到达过，对答案没有影响
    f[0]=1;
    for(int i=1;i<=n;i++){ //物品
        for(int j=m;j>=a[i];j--){  //体积
            f[j]+=f[j-a[i]];
        }
    }
    cout<<f[m]<<endl;
}
int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}