// 自然数拆分 https://www.acwing.com/problem/content/281/

#include <bits/stdc++.h>
using namespace std;
using  i64 =long long;
const i64 mod=2147483648;
void solve() {
    int n;
    cin >> n;
    vector<i64> f(n + 1, 0);
    f[0] = 1;
    for (int i = 1; i <= n-1; i++) { //n本身不算
        for (int j = i; j <= n; j++) {
            f[j] = (f[j] + f[j - i]) % mod;
        }
    }
    cout << f[n] << endl;
}