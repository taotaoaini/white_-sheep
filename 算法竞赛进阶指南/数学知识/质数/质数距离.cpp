// 01 质数距离 https://www.acwing.com/problem/content/198/
#include <bits/stdc++.h>
using namespace std;
using i64 = long long;
// 筛1-1e5里的质数
const int inf=0x3f3f3f3f;
vector<int> init(int n){
    vector<int> res;
    vector<int> vis(n+10);
    for(int i=2;i<=n;i++){
        if(!vis[i]){
            res.push_back(i);
        }
        for(int j=0;j<res.size()&&i*res[j]<=n;j++){
            vis[i*res[j]]=true;
            if(i%res[j]==0) break; //确保每个数被自己的最小质数筛掉
        }
    }
    return res;
}
void solve(){
    vector<int> pr=init(1e5);
    // [L,R] x>=L,p|x，且x最小
    i64 L,R;
    while(cin>>L>>R){
        vector<int> a(1e6+10);
        for(int i=0;i<(int)pr.size();i++){
            int p=pr[i];
            for(i64 j=max(1LL*(L+p-1)/p*p,1LL*2*p);j<=R;j+=p){
                a[j-L]=true;
            }
        }
        vector<int> b;
        for(int i=0;i<=R-L;i++){
            if(!a[i]&&i+L>1) b.push_back(i+L);
        }
        int mi[3]{inf,0,0},mx[3]{0};
        for(int i=1;i<(int)b.size();i++){
            if(b[i]-b[i-1]<mi[0]){ mi[0]=b[i]-b[i-1];mi[1]=b[i-1];mi[2]=b[i];}
            if(b[i]-b[i-1]>mx[0]){ mx[0]=b[i]-b[i-1];mx[1]=b[i-1];mx[2]=b[i];}
        }
        if((int)b.size()<2) {cout<<"There are no adjacent primes."<<endl;}
        else cout<<mi[1]<<","<<mi[2]<<" are closest, "<<mx[1]<<","<<mx[2]<<" are most distant."<<endl;
    }
}
int main(){
    ios::sync_with_stdio(false);cin.tie(0);
    int t=1;
    while(t--){
        solve();
    }
}