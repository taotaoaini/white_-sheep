//  阶乘分解 https://www.acwing.com/problem/content/199/
#include <bits/stdc++.h>
using namespace std;

void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    int n;
    cin >> n;
    auto get = [&](int n)->vector<int> { //筛质数
        vector<int> vis(n + 1);
        vector<int> pr;
        for (int i = 2; i <= n; i++) {
            if (!vis[i]) {
                pr.push_back(i);
            }
            for (int j = 0; j < (int)pr.size() && 1LL * i * pr[j] <= n; j++) {
                vis[i*pr[j]]=true;
                if(i%pr[j]==0) break;
            }
        }
        return pr;
    };
    auto re=[&](int n,int p)->int{ //分解质因子，阶乘分解
        int res=0;
        while(n){
            res+=n/p;
            n/=p;
        }
        return res;
    };
    vector<int> pr=get(n);
    // for(auto & x: pr) cout<<x<<" ";
    for(int i=0;i<(int)pr.size();i++){
        int x=re(n,pr[i]);
        if(x!=0) cout<<pr[i]<<" "<<x<<endl;
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}