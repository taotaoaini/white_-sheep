// 01 超市 https://www.acwing.com/problem/content/147/
#include <bits/stdc++.h>
using namespace std;
using i64 = long long;
using pii=pair<int,int>;
#define fi first
#define se second
void solve() {
    int n;
    while ((cin >> n)) {
        vector<pii> a(n + 1);
        for (int i = 1; i <= n; i++) cin >> a[i].fi >> a[i].se;
        sort(a.begin() + 1, a.end(), [&](pii x, pii y) {return x.se < y.se;});
        priority_queue<int, vector<int>, greater<int>> q;
        int res = 0;
        int d=1;
        for (int i = 1; i <= n; i++) {
            if(a[i].se<d){
                int x=q.top();
                if(x<a[i].fi){
                    res-=x;
                    res+=a[i].fi;
                    q.pop();
                    q.push(a[i].fi);
                }
            }else{
                res+=a[i].fi;
                d+=1;
                q.push(a[i].fi);
            }
        }
        cout<<res<<endl;
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}