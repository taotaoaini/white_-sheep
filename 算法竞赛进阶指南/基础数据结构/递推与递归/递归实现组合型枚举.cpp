#include <bits/stdc++.h>

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;
int n,m;
void dfs(int x, int k, vector<int> a) {
    if (k >= m) {
        for(int i=0;i<(int)a.size();i++){
            cout<<a[i]<<" ";
        }
        cout<<endl;
        return;
    }
    if (x > n ||n-x+1+k<m) {
        return;
    }
    a.push_back(x);
    dfs(x + 1,k+1,a);
    a.pop_back();
    dfs(x + 1, k,a);
}
void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n>>m;
    dfs(1, 0,vector<int>());
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}