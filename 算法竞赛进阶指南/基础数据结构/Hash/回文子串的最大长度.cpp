// 回文子串的最大长度 https://acwing.com/problem/content/141/
//  Zbox优化，构造成奇串,abcbabcbabcba,$#a#b#c#b#a#b#c#b#a#b#c#b#a#
#include <bits/stdc++.h>
using namespace std;
void solve() {
    string s;
    int idx=0;
    while (cin >> s) {
        idx+=1;
        if (s == "END") break;
        auto get = [&](string s)->string{
            vector<char> vec;
            vec.push_back('$');
            vec.push_back('#');
            for (int i = 0; i < (int)s.length(); i++) {
                vec.push_back(s[i]);
                vec.push_back('#');
            }
            return string(vec.begin(), vec.end());
        };
        auto getd = [&](string s)->vector<int>{
            int n=s.length()-1;
            vector<int> d(n+10);
            d[1]=1;
            for(int i=2,L=0,R=1;i<=n;i++){
                if(i<=R) d[i]=min(d[R-i+L],R-i+1); //R-i+L:对称的点，R-i+1：盒子限制的长度
                while(s[i-d[i]]==s[i+d[i]]) d[i]+=1; //暴力查找
                if(i+d[i]-1>R) L=i-d[i]+1,R=i+d[i]-1;  //更新盒子长度
            }
            return d;
        };
        s = get(s);
        vector<int> tmp=getd(s);
        int res=0;
        for(int i=0;i<(int)tmp.size();i++){
            res=max(res,tmp[i]);
        }
        cout<<"Case "<<idx<<":"<<" "<<res-1<<endl;
        //res-1:为什么减1，
        // #b#c# a #c#b# 6-1,多了一个#
        // #a#b # b#a# :多了一个中心的#
    }
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}