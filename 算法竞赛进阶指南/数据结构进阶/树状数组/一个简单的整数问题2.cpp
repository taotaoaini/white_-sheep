// https://www.acwing.com/activity/content/problem/content/478/
#include<bits/stdc++.h>
using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
// #define lc(x) tr[x].l
// #define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;

#define lc p*2
#define rc p*2+1
int n, m;
i64 sum[N * 4], add[N * 4];
int a[N];

void pull(int p) {
    sum[p] = sum[lc] + sum[rc];
}

void push(int p, int l, int r) {
    int mid = (l + r) >> 1;
    if (add[p]) {
        sum[lc] += 1LL * (mid - l + 1) * add[p];
        sum[rc] += 1LL * (r - mid) * add[p];
        add[lc] += add[p];
        add[rc] += add[p];
        add[p] = 0;
    }
}

void build(int p, int l, int r) {
    sum[p] = a[l];
    if (l == r) return;
    int mid = (l + r) >> 1;
    build(lc, l, mid);
    build(rc, mid + 1, r);
    pull(p);
}

void change(int p, int l, int r, int ql, int qr, int val) {
    if (ql <= l && qr >= r) {
        sum[p] += 1LL * val * (r - l + 1);
        add[p] += val;
        return;
    }
    push(p, l, r);
    int mid = (l + r) >> 1;
    if (ql <= mid) { change(lc, l, mid, ql, qr, val); }
    if (qr > mid) { change(rc, mid + 1, r, ql, qr, val); }
    pull(p);
}

i64 query(int p, int l, int r, int ql, int qr) {
    if (ql <= l && qr >= r) {
        return sum[p];
    }
    push(p, l, r);
    int mid = (l + r) >> 1;
    i64 res = 0;
    if (ql <= mid) { res += query(lc, l, mid, ql, qr); }
    if (qr > mid) { res += query(rc, mid + 1, r, ql, qr); }
    return res;
}

void solve() {
    cin >> n >> m;
    for (int i = 1; i <= n; i++) cin >> a[i];
    build(1, 1, n);
    for (int i = 1; i <= m; i++) {
        char op;
        int l, r, val;
        cin >> op;
        if (op == 'Q') {
            cin >> l >> r;
            cout << query(1, 1, n, l, r) << endl;
        } else {
            cin >> l >> r >> val;
            change(1, 1, n, l, r, val);
        }
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}