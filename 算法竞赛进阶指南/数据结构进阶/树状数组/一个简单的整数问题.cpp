#include <bits/stdc++.h>

using namespace std;
const int N = 100010;
using i64 =long long;
#define lc p*2
#define rc p*2+1
int n, m;
i64 sum[N * 4], add[N * 4];
int a[N];

void pull(int p) {
    sum[p] = sum[lc] + sum[rc];
}

void push(int p, int l, int r) {
    int mid = (l + r) >> 1;
    if (add[p]) {
        sum[lc] += 1LL * (mid - l + 1) * add[p];
        sum[rc] += 1LL * (r - mid) * add[p];
        add[lc] += add[p];
        add[rc] += add[p];
        add[p] = 0;
    }
}

void build(int p, int l, int r) {
    sum[p] = a[l];
    if (l == r) return;
    int mid = (l + r) >> 1;
    build(lc, l, mid);
    build(rc, mid + 1, r);
    pull(p);
}

void change(int p, int l, int r, int ql, int qr, int val) {
    if (ql <= l && qr >= r) {
        sum[p] += 1LL * val * (r - l + 1);
        add[p] += val;
        return;
    }
    push(p, l, r);
    int mid = (l + r) >> 1;
    if (ql <= mid) { change(lc, l, mid, ql, qr, val); }
    if (qr > mid) { change(rc, mid + 1, r, ql, qr, val); }
    pull(p);
}

i64 query(int p, int l, int r, int ql, int qr) {
    if (ql <= l && qr >= r) {
        return sum[p];
    }
    push(p, l, r);
    int mid = (l + r) >> 1;
    i64 res = 0;
    if (ql <= mid) { res += query(lc, l, mid, ql, qr); }
    if (qr > mid) { res += query(rc, mid + 1, r, ql, qr); }
    return res;
}

void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> m;
    for (int i = 1; i <= n; i++) cin >> a[i];
    build(1, 1, n);
    // cout << query(1, 1, n, 1, n) << endl;
    for (int i = 1; i <= m; i++) {
        char op;
        int l, r, x, val;
        cin >> op;
        if (op == 'Q') {
            cin >> x;
            cout << query(1, 1, n, x, x) << endl;
        } else {
            cin >> l >> r >> val;
            change(1, 1, n, l, r, val);
        }
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}