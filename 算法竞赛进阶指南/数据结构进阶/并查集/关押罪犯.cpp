#include <bits/stdc++.h>

using i64 = signed long long;
using u64 = unsigned long long;
using f64 = double;
using f128 = long double;
using pii = std::pair<int, int>;
using pll = std::pair<long long, long long>;
using pci = std::pair<char, int>;
using namespace std;

#define bitcnt(x) (__builtin_popcountll(x))//64位数中1的个数
#define endl '\n'
#define ls p<<1
#define rs p<<1|1
#define fi first
#define se second
#define lc(x) tr[x].l
#define rc(x) tr[x].r

priority_queue<int, vector<int>, less<int> > mx_hp; //大顶堆
priority_queue<int, vector<int>, greater<int> > mi_hp; //小顶堆

const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const i64 dinf = 0x3f3f3f3f3f3f3f3f; //4.62*10^18级别的
const int mod = 1e9 + 7;
const int M = 100010;
const int N = 100010;


void solve() {
    int n, m;
    cin >> n >> m;
    vector<array<int, 3>> a(m + 1);
    for (int i = 1; i <= m; i++) cin >> a[i][0] >> a[i][1] >> a[i][2];
    sort(a.begin() + 1, a.end(), [&](array<int, 3> x, array<int, 3> y) {
        return x[2] > y[2];
    });
    //种类并查集
    vector<int> fa(n*2+10);
    vector<int> e(n+10);
    for (int i = 1; i <= n*2; i++) fa[i] = i;
    // 敌人的敌人就是朋友
    auto find = [&](int x) {while (x != fa[x]) x = fa[x] = fa[fa[x]]; return x;};
    for (int i = 1; i <= m; i++) {
        auto& p = a[i];
        int fax = find(p[0]);
        int fay = find(p[1]);
        //前面放在一起了，朋友内讧了
        if (fax == fay) {cout << p[2] << endl; return;}
        else {
            /*fa[fax]=find(p[1]+n);
            fa[fay]=find(p[0]+n);*/
            if(e[p[0]]==0) e[p[0]]=p[1];
            fa[fay]=find(e[p[0]]);
            if(e[p[1]]==0) e[p[1]]=p[0];
            fa[fax]=find(e[p[1]]);
        }
    }
    cout << 0 << endl;
}

int main() {
    ios::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    //cin>>t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}