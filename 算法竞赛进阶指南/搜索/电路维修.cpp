//电路维修  https://www.acwing.com/problem/content/177/

#include<iostream>
#include<vector>
#include<queue>
#include<array>
#include<iomanip>//设置浮点数精度问题

using i64 = signed long long;

using namespace std;
const int inf = 0x3f3f3f3f; //1061109567,是10^9级别的
const int N = 510;
using pii = std::pair<int, int>;
int n, m;
string s[N];

int bfs() {
    deque<array<int, 3>> q; //双端队列模拟有限队列，因为是01最短路
    vector<vector<int>> dis(n + 2, vector<int>(m + 2, inf));
    int dx[4]{-1, 1, 1, -1}, dy[4]{-1, -1, 1, 1}; //从左上角，逆时针方向一圈
    int ix[4]{-1, 0, 0, -1}, iy[4]{-1, -1, 0, 0};
    string tmp = "\\/\\/";
    dis[0][0] = 0;
    q.push_back({0, 0, 0});
    while (q.size()) {
        auto p = q.front(); //提出来，立马pop
        q.pop_front();
        int w = p[0], a = p[1], b = p[2];
        if (a == n && b == m) return dis[n][m]; //枚举的是点，会到[n,m]
        if (dis[a][b] != w) continue;
        for (int i = 0; i < 4; i++) {
            int x = dx[i] + a;
            int y = dy[i] + b;
            if (x < 0 || x > n || y < 0 || y > m) continue;
            int ga = a + ix[i];
            int gb = b + iy[i];
            if (ga < 0 || ga >= n || gb < 0 || gb >= m) continue;
            //这个可以没有判断，因为这个格子始终会在这个点的左上方的区域，不会越界
            int c = (s[ga][gb] != tmp[i]); //如果相等则c=0
            int d = c + dis[a][b];
            if (dis[x][y] > d) {
                dis[x][y] = d;
                if (c) q.push_back({d, x, y});
                else q.push_front({d, x, y});
            }
        }
    }
    return -1;
}

void solve() {
    //freopen("a.in","r",stdin);
    //freopen("a.out","w",stdout);
    cin >> n >> m;
    for (int i = 0; i < n; i++) cin >> s[i];
    int res = bfs();
    if (res == -1) cout << "NO SOLUTION" << endl;
    else cout << res << endl;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);

    //init();//全局初始化

    int t = 1;
    cin >> t;

    while (t--) {
        solve();
        //init();
    }

    return 0;
}