//统一命名规范 https://ac.nowcoder.com/acm/contest/80027/F

#include <bits/stdc++.h>
#define endl '\n'
using namespace std;

vector<string> getWords(string s) {
    vector<string> ans;
    string tmp = "";
    for (char c: s) {
        if (isupper(c) || c == '_') {
            if (!tmp.empty()) {
                ans.push_back(tmp);
                tmp.clear();
            }
        }
        if (c != '_') tmp += tolower(c);
    }
    if (!tmp.empty()) ans.push_back(tmp);
    return ans;
}

string Camel(vector<string> words) {
    string res = "";
    res += words.front();
    for (int i = 1; i < words.size(); i++) {
        string s = words[i];
        s[0] = toupper(s[0]);
        res += s;
    }
    return res;
}

string Pascal(vector<string> words) {
    string res = "";
    for (string s: words) {
        s[0] = toupper(s[0]);
        res += s;
    }
    return res;
}

string Snake(vector<string> words) {
    string res = "";
    res += words.front();
    for (int i = 1; i < words.size(); i++) {
        res += "_" + words[i];
    }
    return res;
}

void solve() {
    int n;
    string op;
    cin >> n >> op;
    for (int i = 1; i <= n; i++) {
        string s;
        cin >> s;
        vector<string> words = getWords(s);
        if (op == "Camel") {
            cout << Camel(words) << '\n';
        } else if (op == "Pascal") {
            cout << Pascal(words) << '\n';
        } else {
            cout << Snake(words) << '\n';
        }
    }
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);
    solve();
}