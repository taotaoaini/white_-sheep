// 解压文本 https://ac.nowcoder.com/acm/contest/80027/J
// 是一道dfs暴力搜索的题， 答案有解必须是有向无环图，判断环直接返回
// 回溯标记.进入这一层vis就标记为true，退出这一层就标记为false
#include<bits/stdc++.h>

using namespace std;
int n;
string a[20];
bool vis[20];
string res = "";
bool flag = false;

void dfs(int x) {
    if (flag) return;
    if (res.length() > 1e6) {
        cout << "#" << endl;
        flag = true;
        return;
    }
    for (int i = 0; i < a[x].length(); i++) {
        if (a[x][i] == '*') {
            i++;
            int id = a[x][i] - '0';
            if (id > n) {
                res += " ";
                continue;
            } else {
// 				cout<<x<<" "<<id<<" "<<vis[id]<<endl;
                if (vis[id]) {
                    cout << "#" << endl;
                    flag = true;
                    return;
                }
                vis[id] = true;
                dfs(id);
                vis[id] = false;
            }
        } else {
            res += a[x][i];
        }
    }
}

void solve() {
    cin >> n;
    cin.ignore(1, '\n');
    for (int i = 1; i <= n; i++) {
        getline(cin, a[i], '#');  // 空格，换行符读入
        cin.ignore(1, '\n');
    }
    vis[1] = true;
    dfs(1);
    vis[1] = false;
    if (!flag)
        cout << res << endl;
}

int main() {
    solve();
    return 0;
}