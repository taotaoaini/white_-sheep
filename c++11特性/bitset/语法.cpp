#include<bits/stdc++.h>
using namespace std;

void solve(){
    // 1.
    bitset<10> a; // 默认以0填充
    string s="100101";
    bitset<10> b(s); // 长度为10，前面用０补充 0000100101
    cout<<b<<endl;
    // 2.超出了bitset定义的范围,整数取后部分，字符串取前部分
    bitset<2> c(12);//12的二进制为1100（长度为４），但c的size=2，只取后面部分，即00
    string s1="100101";
    bitset<4> d(s);//s的size=6，而bitset的size=4，只取前面部分，即1001
    // 3.运算操作，对二进制的操作都可以直接使用到bitset上
    a|=b;
    // 4.单一元素访问和修改
    // 访问，从低到高位访问
    cout<<a[0]<<endl;
    a[0]=1;
    // 5.函数操作
    bitset<8>foo(string("10011011"));

    cout<<foo.count()<<endl;//5　　（count函数用来求bitset中1的位数，foo中共有５个１
    cout<<foo.size()<<endl;//8　　（size函数用来求bitset的大小，一共有８位

    cout<<foo.test(0)<< endl;//true　　（test函数用来查下标处的元素是０还是１，并返回false或true，此处foo[0]为１，返回true
    cout<<foo.test(2)<<endl;//false　　（同理，foo[2]为０，返回false

    cout<<foo.any()<<endl;//true　　（any函数检查bitset中是否有１
    cout<<foo.none()<<endl;//false　　（none函数检查bitset中是否没有１
    cout<<foo.all()<<endl;//false　　（all函数检查bitset中是全部为１
    foo.reset(); //  全都变成0
    foo.set(); // 全都变成1
    cout<<foo.to_string()<<endl; //返回它转换为string的结果
}
int main(){
    solve();
    return 0;
}